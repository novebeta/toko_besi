jun.PelunasanPiutangDetilWin = Ext.extend(Ext.Window, {
    title: 'Ganti Nilai Kas Diterima',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PelunasanPiutangDetil',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'kas_diterima',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kas_diterima',
                        id: 'kas_diterimaid',
                        ref: '../kas_diterima',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                }
            ]
        };
        jun.PelunasanPiutangDetilWin.superclass.initComponent.call(this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-PelunasanPiutangDetil').getForm().reset();
        this.btnDisabled(false);
        this.close();
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(true);
    }

});