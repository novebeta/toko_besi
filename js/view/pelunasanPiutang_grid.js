jun.PelunasanPiutangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pelunasan Piutang",
    id: 'docs-jun.PelunasanPiutangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Dokumen',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Konsumen',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        jun.rztPelunasanPiutang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.mode = 'grid';
                    b.params.konsumen_id = Ext.getCmp('konsumenpelunasanpiutanggridid').getValue();
                }
            }
        });
        this.store = jun.rztPelunasanPiutang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Pelunasan Piutang',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Pelunasan Piutang',
                    ref: '../btnEdit'
                }                ,
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Konsumen :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    minChars:3,
                    store: jun.rztKonsumenCmp,
                    forceSelection: true,
                    hiddenName: 'konsumen_id',
                    valueField: 'orang_id',
                    displayField: 'nama',
                    matchFieldWidth: !1,
                    itemSelector: "div.search-item-table",
                    tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                        '<div class="cell4" style="width: 100px;text-align: right;">{kode}</div>',
                        '<div class="cell4" style="width: 250px;">{nama}</div>',
                        "</div></tpl>", '</div>'),
                    listWidth: 350,
                    hideTrigger: true,
                    ref: '../konsumen',
                    id: 'konsumenpelunasanpiutanggridid',
                    allowBlank: false,
                    width: 175
                }
            ]
        };
        jun.PelunasanPiutangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.konsumen.on('select', function () {this.store.reload();}, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PelunasanPiutangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pelunasan_piutang_id;
        var form = new jun.PelunasanPiutangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPelunasanPiutangDetil.removeAll();
        jun.rztPelunasanPiutangDetil.baseParams = {
            pelunasan_piutang_id: idz
            //tgl: this.tgl.getValue()
        };
        jun.rztPelunasanPiutangDetil.load();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PelunasanPiutang/delete/id/' + record.json.pelunasan_piutang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPelunasanPiutang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
