jun.DetilPenjualanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DetilPenjualanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DetilPenjualanStoreId',
            autoLoad: !1,
            autoSave: !1,
            url: 'DetilPenjualan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'disc2', type: 'float'},
                {name: 'pot', type: 'float'},
                {name: 'totalpot', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'nominal', type: 'float'},
                {name: 'pcs', type: 'float'},
                {name: 'hpp', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var total = this.sum("nominal");
        var disc = this.sum("totalpot");
        var bruto = this.sum("bruto");
        Ext.getCmp("discid").setValue(disc);
        Ext.getCmp("brutoid").setValue(bruto);
        Ext.getCmp("sub_totalid").setValue(total);
        total += parseFloat(Ext.getCmp("ongkos_angkutid").getValue());
        Ext.getCmp("totalid").setValue(total);
        var dp = parseFloat(Ext.getCmp("uang_mukaid").getValue());
        if (total - dp < 0) {
            Ext.getCmp("uang_mukaid").setValue(0);
        } else {
            Ext.getCmp("sisa_tagihanid").setValue(total - dp);
        }
    }
});
jun.rztDetilPenjualan = new jun.DetilPenjualanstore();
