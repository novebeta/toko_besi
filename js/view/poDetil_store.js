jun.PoDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PoDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PoDetilStoreId',
            url: 'PoDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_detil_id'},
                {name: 'barang_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'pcs', type: 'float'},
                {name: 'po_id'},
            ]
        }, cfg));
    }
});
jun.rztPoDetil = new jun.PoDetilstore();
//jun.rztPoDetil.load();
