jun.TerimaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penerimaan Barang",
    id: 'docs-jun.TerimaGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No LPB',
            sortable: true,
            resizable: true,
            dataIndex: 'no_lpb',
            width: 100
        },
        {
            header: 'Tgl Terima',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_terima',
            width: 100
        },
        {
            header: 'No PO',
            sortable: true,
            resizable: true,
            dataIndex: 'no_po',
            width: 100
        },
        {
            header: 'Nama Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztTerima.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.mode = 'grid';
                    b.params.done = Ext.getCmp('statuspenerimaangridid').getValue();
                }
            }
        });
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        this.store = jun.rztTerima;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Lihat Penerimaan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Faktur Pembelian',
                    ref: '../btnFakturBeli',
                    id: "btnFakturBeliid"
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Status :'
                },
                new jun.comboOpenClose({
                    id: 'statuspenerimaangridid',
                    ref: '../status'
                })
            ]
        };
        jun.TerimaGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.status.on('select', function () {this.store.reload();}, this);
        this.btnFakturBeli.on('Click', this.fakturForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        //Ext.getCmp('btnFakturBeliid').setVisible(SYSTEM_PEMBELIAN_3);
    },
    fakturForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.json.done === 1) {
            Ext.MessageBox.alert("Warning", "Pembelian tidak bisa dibuat karena sudah dibuat.");
            return;
        }
        var idz = selectedz.json.terima_id;
        var form = new jun.PembelianWin({modez: 0});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
        jun.rztPembelianDetil.baseParams = {
            terima_id: idz
        };
        jun.rztPembelianDetil.reload();
        jun.rztPembelianDetil.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TerimaWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.terima_id;
        var form = new jun.TerimaWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaDetil.baseParams = {
            terima_id: idz
        };
        jun.rztTerimaDetil.reload();
        jun.rztTerimaDetil.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Terima/delete/id/' + record.json.terima_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTerima.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
