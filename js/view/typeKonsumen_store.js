jun.TypeKonsumenstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TypeKonsumenstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TypeKonsumenStoreId',
            url: 'TypeKonsumen',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'type_konsumen_id'},
{name:'type_konsumen_code'},
{name:'type_konsumen_name'},
{name:'status'}
            ]
        }, cfg));
    }, FilterData: function() {
        this.filter([{property: "status", value: "1"}])
    }
});
jun.rztTypeKonsumen = new jun.TypeKonsumenstore();
//jun.rztTypeKonsumen.load();
