jun.KasWin = Ext.extend(Ext.Window, {
    title: 'Kas Masuk',
    modez: 1,
    width: 890,
    height: 535,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "hidden",
                        name: "arus",
                        ref: "../arus"
                    },
                    {
                        xtype: "label",
                        text: "Nomor:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Kelompok:",
                        x: 5,
                        y: 35
                    },
                    new jun.cmbKelompok({
                        hiddenName: "tipe_kas",
                        name: 'tipe_kas',
                        id: 'tipe_kasid',
                        width: 175,
                        value: 0,
                        ref: '../kelompok',
                        x: 85,
                        y: 32
                    }),
                    {
                        xtype: "label",
                        text: "Terima Dari:",
                        ref: '../label_who_id',
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        minChars: 3,
                        store: jun.rztKonsumenCmp,
                        hiddenName: 'who_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="float:left;width:120;font-weight: bold">{kode}</span>' +
                                '<span style="font-weight: bold">{nama}</span>', "</div></tpl>"),
                        listWidth: 350,
                        forceSelection: true,
                        hideTrigger: true,
                        allowBlank: false,
                        id: 'who_idid',
                        x: 85,
                        y: 62,
                        width: 175
                    },
//                    {
//                        xtype: "container",
//                        ref:'../who_id',
//                        id:'who_idid',
//                        items: [
//
//                        ],
//
//                    },
                    {
                        xtype: "label",
                        text: "Rekening:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        forceSelection: true,
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        width: 175,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Cara Bayar:",
                        x: 5,
                        y: 125
                    },
                    new jun.comboPayment({
                        hiddenName: "cara_bayar",
                        name: 'cara_bayar',
                        id: 'cara_bayarid',
                        width: 175,
                        x: 85,
                        y: 122
                    }),
                    {
                        xtype: "label",
                        text: "Keterangan:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        maxLength: 600,
                        height: 110,
                        width: 300,
                        x: 375,
                        y: 32
                    },
                    new jun.KasDtlGrid({
                        height: 275,
                        frameHeader: !1,
                        header: !1,
                        x: 5,
                        y: 155
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 595,
                        y: 438
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        id: 'totalkas',
                        name: 'total',
                        ref: '../total',
                        maxLength: 30,
                        x: 675,
                        y: 435
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasWin.superclass.initComponent.call(this);
        this.on('show', this.onActivate, this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.kelompok.on('select', this.onKelompokChange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztKasDtl.removeAll();
    },
    onActivate: function () {
        var a = this.arus.getRawValue();
        if (a == 0) {
            this.setTitle('Kas Masuk');
            this.label_who_id.setText('Terima Dari');
        } else {
            this.setTitle('Kas Keluar');
            this.label_who_id.setText('Bayar Ke');
        }
        if(this.tipe == "kecil" ){
            this.id_bank.setReadOnly(true);
            this.id_bank.setValue(SYSTEM_KAS_KECIL);
        }
    },
    onKelompokChange: function () {
        Ext.getCmp('who_idid').destroy();
        var kelompok = Ext.getCmp('tipe_kasid').getValue();
        var comp;
        switch (parseInt(kelompok)) {
            case 0:
                comp = new Ext.form.ComboBox({
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    minChars: 3,
                    store: jun.rztKonsumenCmp,
                    hiddenName: 'who_id',
                    valueField: 'orang_id',
                    displayField: 'nama',
                    matchFieldWidth: !1,
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="float:left;width:120;font-weight: bold">{kode}</span>' +
                            '<span style="font-weight: bold">{nama}</span>', "</div></tpl>"),
                    listWidth: 350,
                    hideTrigger: true,
                    allowBlank: false,
                    id: 'who_idid',
                    x: 85,
                    y: 62,
                    width: 175
                });
                break;
            case 1:
                comp = new Ext.form.ComboBox({
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.rztKaryawan,
                    hiddenName: 'who_id',
                    valueField: 'orang_id',
                    displayField: 'nama',
                    allowBlank: false,
                    id: 'who_idid',
                    x: 85,
                    y: 62,
                    width: 175
                });
                break;
            case 2:
                comp = new Ext.form.ComboBox({
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.rztSupplierCmp,
                    hiddenName: 'who_id',
                    valueField: 'orang_id',
                    displayField: 'nama',
                    allowBlank: false,
                    id: 'who_idid',
                    x: 85,
                    y: 62,
                    width: 175
                });
                break;
            case 3:
                comp = new Ext.ux.form.UCTextField({
                    allowBlank: false,
                    id: 'who_idid',
                    name: 'who',
                    maxLength: 25,
                    x: 85,
                    y: 62,
                    width: 175
                });
                break;
            default:
        }
        this.formz.add(comp);
        this.formz.doLayout(false, false);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Kas/create';
        Ext.getCmp('form-Kas').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztKasDtl.data.items, "data"))
            },
            success: function (f, a) {
                var arus = this.arus.getRawValue();
                if(arus == 1){
                    jun.rztKas.reload();
                }else{
                    jun.rztKasKeluar.reload();
                }
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (!this.closeForm) {
                    Ext.getCmp('form-Kas').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});