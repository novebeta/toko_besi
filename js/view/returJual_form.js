jun.ReturJualWin = Ext.extend(Ext.Window, {
    title: 'Retur Penjualan',
    modez: 1,
    width: 900,
    height: 520,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturJual',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "hidden",
                        name: "parent",
                        ref: "../parent"
                    },
                    {
                        xtype: "label",
                        text: "No Retur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        x: 85,
                        y: 2,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        maxLength: 15,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztKonsumenCmp,
                        minChars:3,
                        hiddenName: 'konsumen_id',
                        valueField: 'konsumen_id',
                        displayField: 'konsumen_name',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="float:left;width:120;font-weight: bold">{konsumen_code}</span>' +
                                '<span style="font-weight: bold">{konsumen_name}</span>', "</div></tpl>"),
                        listWidth: 350,
                        hideTrigger: true,
                        ref: '../konsumen_retur',
                        allowBlank: false,
                        readOnly: true,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Area:",
                        x: 625,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "",
                        ref: '../area',
                        x: 705,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        height: "50",
                        width: "215",
                        readOnly: true,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Pasar:",
                        x: 625,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "",
                        ref: '../pasar',
                        x: 705,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Sales:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'salesman_id',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        width: 175,
                        readOnly: true,
                        ref: '../sales',
                        x: 85,
                        y: 62
                    },
                    new jun.ReturJualDetilGrid({
                        x: 5,
                        y: 95,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalreturid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutoreturid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 392
                    },
                    {
                        xtype: 'hidden',
                        name: 'disc',
                        id: 'discreturid',
                        ref: '../disc',
                        value: 0
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalreturid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Kas Keluar:",
                        x: 590,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        name: 'uang_muka',
                        id: 'kaskeluarid',
                        ref: '../uang_muka',
                        maxLength: 30,
                        value: 0,
                        x: 690,
                        y: 392
                    },
//                    {
//                        xtype: "label",
//                        text: "BG/Cek:",
//                        x: 625,
//                        y: 455
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'no_bg_cek',
//                        id: 'no_bg_cekid',
//                        ref: '../no_bg_cek',
//                        maxLength: 30,
//                        allowBlank: false,
//                        value: '-',
//                        x: 705,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Kredit Piutang:",
                        x: 590,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'sisa_tagihan',
                        id: 'kreditpiutangid',
                        ref: '../sisa_tagihan',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 690,
                        y: 422
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturJualWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.uang_muka.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
    },
    updateTotal: function () {
        jun.rztReturJualDetil.refreshData();
    },
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztReturJualDetil.removeAll();
        Ext.getCmp('form-ReturJual').getForm().reset();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    onKonsumenChange: function () {
        var konsumen_id = this.konsumen_retur.getValue();
        var konsumen = jun.getKonsumen(konsumen_id);
        var area = jun.getArea(konsumen.data.area_id);
        this.alamat.setValue(konsumen.data.address);
        this.area.setText(area.data.area_name);
        if (konsumen.data.pasar_id != undefined) {
            var pasar = jun.getPasar(konsumen.data.pasar_id);
            this.pasar.setText(pasar.data.pasar_name);
        }
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztReturJualDetil.data.length == 0) {
            Ext.MessageBox.show({
                title: 'Warning',
                msg: "Retur jual minimal harus memiliki satu item.",
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
            this.btnDisabled(false);
            return;
        }
//        if (Ext.getCmp('kas_keluarid').getGroupValue() == '0') {
//            jun.rztFakturPiutangReturJual.baseParams = {
//                sales_id: this.sales.getValue(),
//                tgl: this.tgl.getValue(),
//                konsumen_id: this.konsumen.getValue()
//            };
//            jun.rztFakturPiutangReturJual.reload();
//            jun.rztFakturPiutangReturJual.baseParams = {};
//            var form = new jun.FakturPiutangReturJualWin();
//            form.show();
//            var total = parseInt(this.total.getValue());
//            form.piutang.setValue(total);
//            form.alokasi.setValue(0 - total);
//            form.onHide = function () {
//                var b = this.btn;
//                if (b == 1) {
//                    var c = this.sm;
//                    var urlz = 'ReturJual/create/';
//                    Ext.getCmp('form-ReturJual').getForm().submit({
//                        url: urlz,
//                        timeOut: 1000,
//                        scope: this,
//                        params: {
//                            kredit_piutang: Ext.encode(Ext.pluck(
//                                c, "data")),
//                            detil: Ext.encode(Ext.pluck(
//                                jun.rztReturJualDetil.data.items, "data")),
//                            id: this.id,
//                            mode: this.modez
//                        },
//                        success: function (f, a) {
//                            jun.rztReturJual.reload();
//                            var response = Ext.decode(a.response.responseText);
//                            Ext.MessageBox.show({
//                                title: 'Info',
//                                msg: response.msg,
//                                buttons: Ext.MessageBox.OK,
//                                icon: Ext.MessageBox.INFO
//                            });
//                            Ext.getCmp('form-ReturJual').getForm().reset();
//                            this.close();
//                        },
//                        failure: function (f, a) {
//                            Ext.MessageBox.show({
//                                title: 'Warning',
//                                msg: 'server-side failure with status code '
//                                    + f.status,
//                                buttons: Ext.MessageBox.OK,
//                                icon: Ext.MessageBox.WARNING
//                            });
//                        }
//
//                    });
//                }
//            }
//        } else {
            var urlz = 'ReturJual/create/';
            Ext.getCmp('form-ReturJual').getForm().submit({
                url: urlz,
                timeOut: 1000,
                scope: this,
                params: {
                    detil: Ext.encode(Ext.pluck(
                        jun.rztReturJualDetil.data.items, "data")),
                    id: this.id,
                    mode: this.modez
                },
                success: function (f, a) {
                    jun.rztReturJual.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    if (this.modez == 0) {
                        Ext.getCmp('form-ReturJual').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if (this.closeForm) {
                        this.close();
                    }
                },
                failure: function (f, a) {
                    jun.rztReturJual.reload();
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert(a.response.statusText, a.response.responseText);
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.close();
                }

            });
//        }
//        this.btnDisabled(false);
    },
    onbtnSaveCloseClick: function () {
        this.btnDisabled(true);
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});