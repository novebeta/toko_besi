jun.calculateDisc = function (jml, price, d1, d2, pot) {
//(2 x Rp100.000 x 98%) x 95% -- (2 x Rp2000)
    return Math.round((jml * price * ((100 - d1) / 100)) * ((100 - d2) / 100)) - Math.round(jml * pot);
};
jun.calculateDiscPurchase = function (jml, price, d) {
//(2 x Rp100.000 x 98%) x 95% -- (2 x Rp2000)
    return Math.round(jml * price * ((100 - d) / 100));
};
jun.getKonsumen = function (a) {
    var b = jun.rztKonsumenLib, c = b.findExact("orang_id", a);
    return b.getAt(c)
};
jun.getSupplier = function (a) {
    var b = jun.rztSupplierLib, c = b.findExact("orang_id", a);
    return b.getAt(c)
};
jun.renderSupplier = function (a, b, c) {
    var jb = jun.getSupplier(a);
    return jb == undefined ? '' : jb.data.nama;
};
jun.getBarang = function (a) {
    var b = jun.rztBarangLib, c = b.findExact("barang_id", a);
    return b.getAt(c)
};
jun.renderBarcode = function (a, b, c) {
    var c = jun.getBarang(a);
    return c.data.barcode
};
jun.renderNamaBarang = function (a, b, c) {
    var c = jun.getBarang(a);
    return c.data.barang_name
};
jun.renderSatBarang = function (a, b, c) {
    var c = jun.getBarang(a);
    return c.data.satuan;
};
jun.renderSatBarang = function (a, b, c) {
    var c = jun.getBarang(a);
    return c.data.satuan
};
jun.renderNamaKonsumen = function (a, b, c) {
    var c = jun.getKonsumen(a);
    return c.data.nama
};
jun.renderKodeKonsumen = function (a, b, c) {
    var c = jun.getKonsumen(a);
    return c.data.kode
};
jun.getPenjualan = function (a) {
    var b = jun.rztPenjualan, c = b.findExact("penjualan_id", a);
    return b.getAt(c)
};
jun.getChartMaster = function (a) {
    var b = jun.rztChartMasterLib, c = b.findExact("account_code", a);
    return b.getAt(c)
};
jun.renderNamaChart = function (a, b, c) {
    var c = jun.getChartMaster(a);
    return c.data.account_name
};
jun.convertToPcs = function (id, sat, jml) {
    var barang = jun.getBarang(id);
    var pcs;
    switch (sat) {
        case "PCS":
            pcs = jml;
            break;
        case "LSN":
            pcs = Math.round(jml * 12);
            break;
        case "PAK":
            pcs = Math.round(jml * parseFloat(barang.data.pak_2_pcs));
            break;
        default:
            pcs = Math.round(jml * parseFloat(barang.data.max_2_pcs));
    }
    return pcs;
};
jun.Calculator = Ext.extend(Ext.Window, {
    title: "Kalkulator",
    iconCls: "silk13-calculator",
    modez: 1,
    width: 365,
    height: 195,
    layout: "form",
//    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                defaults: {
                    // applied to each contained panel
                    bodyStyle: "background-color: #E4E4E4; padding: 10px"
                },
                id: "form-Calculator",
                labelWidth: 100,
                labelAlign: "left",
                layout: "table",
                layoutConfig: {columns: 6},
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Barang :'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Barang',
                        colspan: 5,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                            '<span style="float:left;width: 15%;">{barcode}</span><span style="float:left;width: 75%;">{barang_name}</span><span>{sisa}</span>',
                            "</div></tpl>"),
                        listWidth: 650,
                        store: jun.rztBarang,
                        valueField: 'barang_id',
                        displayField: 'barang_name',
                        forceSelection: true,
                        hideTrigger: true,
                        ref: '../barang',
                        width: 258
                    },
                    {
                        xtype: "label",
                        text: 'PCS : '
                    },
                    {
                        fieldLabel: 'PCS',
                        xtype: 'numericfield',
                        ref: '../pcs',
                        value: 0,
                        width: 75
                    },
                    {
                        xtype: "label",
                        text: ''
                    },
                    {
                        xtype: "label",
                        text: 'PCS : '
                    },
                    {
                        fieldLabel: 'PCS',
                        xtype: 'numericfield',
                        ref: '../pcs1',
                        value: 0,
                        colspan: 2,
                        width: 103
                    },
                    {
                        xtype: "label",
                        html: 'Tanda cek untuk <br>perhitungkan satuan saat konversi PCS ke satuan LSN, PAK dan MAKS.',
                        colspan: 2,
                        rowspan: 3
                    },
                    {
                        xtype: "button",
                        text: "==>>",
                        ref: "../btnSave"
                    },
                    {
                        xtype: "label",
                        text: 'LSN'
                    },
                    {
                        xtype: 'checkbox',
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        checked: true,
                        name: 'cek_lsn',
                        ref: '../cek_lsn'
                    },
                    {
                        fieldLabel: 'LSN',
                        xtype: 'numericfield',
                        ref: '../lsn',
                        value: 0,
                        width: 75
                    },
//                    {
//                        xtype: "label",
//                        text: '',
//                        colspan: 2
//                    },
                    {
                        xtype: "button",
                        text: "<<==",
                        ref: "../btnPdf"
                    },
                    {
                        xtype: "label",
                        text: 'PAK'
                    },
                    {
                        xtype: 'checkbox',
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        checked: true,
                        name: 'cek_pak',
                        ref: '../cek_pak'
                    },
                    {
                        fieldLabel: 'PAK',
                        xtype: 'numericfield',
                        ref: '../pak',
                        value: 0,
                        width: 75
                    },
                    {
                        xtype: "label",
                        text: '',
                        colspan: 1
                    },
                    {
                        xtype: "label",
                        text: 'MAKS'
                    },
                    {
                        xtype: 'checkbox',
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        checked: true,
                        name: 'cek_maks',
                        ref: '../cek_maks'
                    },
                    {
                        fieldLabel: 'MAKS',
                        xtype: 'numericfield',
                        ref: '../maks',
                        value: 0,
                        width: 75
                    }
                ]
            }
        ];
//        this.fbar = {
//            xtype: "toolbar",
//            items: [
//                {
//                    xtype: "button",
//                    text: "PCS ==>> LSN, PAK, MAKS",
//                    ref: "../btnSave"
//                },
//                {
//                    xtype: "button",
//                    text: "LSN, PAK, MAKS ==>> PCS",
//                    ref: "../btnPdf"
//                }
//            ]
//        };
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        jun.Calculator.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var pcs = parseFloat(this.pcs1.getValue());
        var lsn = parseFloat(this.lsn.getValue());
        var pak = parseFloat(this.pak.getValue());
        var maks = parseFloat(this.maks.getValue());
        pcs += lsn === 0 ? 0 : jun.convertToPcs(barang_id, 'LSN', lsn);
        pcs += pak === 0 ? 0 : jun.convertToPcs(barang_id, 'PAK', pak);
        pcs += maks === 0 ? 0 : jun.convertToPcs(barang_id, 'MAKS', maks);
        this.pcs.setValue(pcs);
    },
    onbtnSaveclick: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var is_lsn = this.cek_lsn.getValue();
        var is_pak = this.cek_pak.getValue();
        var is_maks = this.cek_maks.getValue();
        var barang = jun.getBarang(barang_id);
        var pcs = parseFloat(this.pcs.getValue());
        var pcs1 = pcs;
        var lsn = 0;
        var pak = 0;
        var maks = 0;
        var max_2_pcs = parseFloat(barang.data.max_2_pcs);
        var pak_2_pcs = parseFloat(barang.data.pak_2_pcs);
        max_2_pcs = isNaN(max_2_pcs) ? 0 : max_2_pcs;
        pak_2_pcs = isNaN(pak_2_pcs) ? 0 : pak_2_pcs;
        if (pcs1 !== 0 && max_2_pcs !== 0 && is_maks) {
            maks = ~~(pcs1 / max_2_pcs);
            pcs1 = pcs1 % max_2_pcs;
        }
        if (pak_2_pcs > 12) {
            if (pcs1 !== 0 && pak_2_pcs !== 0 && is_pak) {
                pak = ~~(pcs1 / pak_2_pcs);
                pcs1 = pcs1 % pak_2_pcs;
            }
            if (pcs1 !== 0 && is_lsn) {
                lsn = ~~(pcs1 / 12);
                pcs1 = pcs1 % 12;
            }
        } else {
            if (pcs1 !== 0 && is_lsn) {
                lsn = ~~(pcs1 / 12);
                pcs1 = pcs1 % 12;
            }
            if (pcs1 !== 0 && pak_2_pcs !== 0 && is_pak) {
                pak = ~~(pcs1 / pak_2_pcs);
                pcs1 = pcs1 % pak_2_pcs;
            }
        }
        this.pcs1.setValue(pcs1);
        this.lsn.setValue(lsn);
        this.pak.setValue(pak);
        this.maks.setValue(maks);
    }
});
jun.showCalculator = function () {
    var calc = Ext.getCmp('form-Calculator');
    if (calc == undefined) {
        var r = new jun.Calculator();
        r.show();
    }else{
        var win = calc.ownerCt;
        win.toFront();
    }
};
jun.BackupRestoreWin = new Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pos.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});