jun.ChartMasterGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kode Rekening",
    id: 'docs-jun.ChartMasterGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode Rekening',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Nama Rekening',
            sortable: true,
            resizable: true,
            dataIndex: 'account_name',
            width: 100
        },
        {
            header: 'Deskripsi',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100,
            renderer: jun.renderActive
        }
    ],
    initComponent: function () {
        this.store = jun.rztChartMaster;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kode Rekening',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Kode Rekening',
                    ref: '../btnEdit'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.rztChartMasterHeader.reload();
        jun.ChartMasterGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ChartMasterWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.account_code;
        var form = new jun.ChartMasterWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ChartMaster/delete/id/' + record.json.account_code,
            method: 'POST',
            success: function (f, a) {
                jun.rztChartMaster.reload();
                jun.rztChartMasterLib.reload();
                jun.rztChartMasterComp.reload();
                jun.rztChartMasterNonKasBank.reload();
                jun.jun.rztChartMasterHeader.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
