jun.Terimastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Terimastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaStoreId',
            url: 'Terima',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_id'},
                {name: 'no_lpb'},
                {name: 'tgl_terima'},
                {name: 'po_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'done'},
                {name: 'no_po'},
                {name: 'tgl'},
                {name: 'nama'},
                {name: 'supplier_id'},
                {name: 'alamat'},
                {name: 'pembelian_id'}
            ]
        }, cfg));
    }
});
jun.rztTerima = new jun.Terimastore();
//jun.rztTerima.load();
