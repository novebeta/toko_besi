jun.KasDtlWin = Ext.extend(Ext.Window, {
    title: 'Detil Kas',
    modez: 1,
    width: 400,
    height: 200,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-KasDtl',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'No Faktur',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_faktur',
                        id: 'no_fakturid',
                        ref: '../no_faktur',
                        maxLength: 20,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Nilai Faktur',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nilai_faktur',
                        id: 'nilai_fakturid',
                        ref: '../nilai_faktur',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kode Rekening',
                        store: jun.rztChartMasterNonKasBank,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">', '<h3><span">{account_code} - {account_name}</span></h3><br />{description}', "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_name',
                        forceSelection: true,
                        ref: '../akun',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Jumlah Kas',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
//                {
//                    xtype: 'button',
//                    text: 'Simpan',
//                    hidden: false,
//                    ref: '../btnSave'
//                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasDtlWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    btnDisabled: function (status) {
//        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var no_faktur = this.no_faktur.getValue();
        if (no_faktur == "") {
            Ext.MessageBox.alert("Error", "Nomer Faktur harus diisi.");
            this.btnDisabled(false);
            return
        }
        var akun = this.akun.getValue();
        if (akun == "") {
            Ext.MessageBox.alert("Error", "Kode rekening harus dipilih.");
            this.btnDisabled(false);
            return
        }
        var a = jun.rztKasDtl.findExact("account_code", akun);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Kode rekening sudah dimansukkan.");
            this.btnDisabled(false);
            return
        }
        var nilai_faktur = this.nilai_faktur.getValue();
        var total = this.total.getValue();
        var c = jun.rztKasDtl.recordType,
            d = new c({
                no_faktur: no_faktur,
                nilai_faktur: nilai_faktur,
                account_code: akun,
                total: total

            });
        jun.rztKasDtl.add(d);
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});