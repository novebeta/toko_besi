jun.Pembelianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pembelianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembelianStoreId',
            url: 'Pembelian',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembelian_id'},
                {name: 'no_faktur_beli'},
                {name: 'tgl'},
                {name: 'tempo'},
                {name: 'sub_total'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'no_bg_cek'},
                {name: 'sisa_tagihan'},
                {name: 'lunas'},
                {name: 'final'},
                {name: 'id_user'},
                {name: 'disc'},
                {name: 'no_faktur_supp'},
                {name: 'tgl_tempo'},
                {name: 'terima_id'},
                {name: 'id_bank'},
                {name: 'biaya_bongkar'},
                {name: 'biaya_angkut'},
                {name: 'allowance'},
                {name: 'tipe'},
                {name: 'terima_id'},
                {name: 'alamat'},
                {name: 'nama'},
                {name: 'supplier_id'},
                {name: 'no_po'},
                {name: 'po_id'},
                {name: 'no_lpb'}
            ]
        }, cfg));
    }
});
jun.rztPembelian = new jun.Pembelianstore();
//jun.rztPembelian.load();
