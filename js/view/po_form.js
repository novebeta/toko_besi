jun.PoWin = Ext.extend(Ext.Window, {
    title: 'Purchase Order',
    modez: 1,
    width: 815,
    height: 470,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Po',
                labelWidth: 100,
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No PO:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_po',
                        ref: '../no_po',
                        maxLength: 15,
                        width: 170,
                        readOnly: true,
                        x: 95,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Pemasok:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        ref: '../supplier',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        width: 170,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 170,
                        value: new Date(),
                        x: 95,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        height: 52,
                        width: 170,
                        readOnly: true,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tgl Dibutuhkan:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_butuh',
                        fieldLabel: 'tgl_butuh',
                        name: 'tgl_butuh',
                        width: 170,
                        format: 'd M Y',
                        x: 95,
                        y: 62
                    },
                    new jun.PoDetilGrid({
                        x: 5,
                        y: 95,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PoWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.supplier.on('select', this.onSuppChange, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else if(this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }else{
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztPoDetil.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onSuppChange: function () {
        var supplier_id = this.supplier.getValue();
        var supplier = jun.getSupplier(supplier_id);
        this.alamat.setValue(supplier.data.alamat);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Po/create/';
        Ext.getCmp('form-Po').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPoDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPo.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Po').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});