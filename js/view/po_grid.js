jun.PoGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Order",
    id: 'docs-jun.PoGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'no_po',
            width: 100
        },
        {
            header: 'Tgl PO',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Dibutuhkan',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_butuh',
            width: 100
        },
        {
            header: 'Pemasok',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderSupplier
        }
    ],
    initComponent: function () {
        jun.rztPo.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.done = Ext.getCmp('statuspogridid').getValue();
                }
            }
        });
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        this.store = jun.rztPo;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah PO',
                    ref: '../btnAdd',
                    id: "btnAddid"
                },
                {
                    xtype: 'tbseparator',
                    id: "separatorbtnEdit"
                },
                {
                    xtype: 'button',
                    text: 'Edit PO',
                    ref: '../btnEdit',
                    id: "btnEditid"
                },
                {
                    xtype: 'tbseparator',
                    id: "separatorbtnTerimaBarang"
                },
                {
                    xtype: 'button',
                    text: 'Penerimaan Barang',
                    ref: '../btnTerimaBarang',
                    id: "btnTerimaBarangid"
                },
                {
                    xtype: 'tbseparator',
                    id: "separatorbtnFakturBeli"
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Status :'
                },
                new jun.comboOpenClose({
                    id: 'statuspogridid',
                    ref: '../status'
                })
            ]
        };
//        this.store.baseParams = {mode: "grid"};
//        this.store.reload();
//        this.store.baseParams = {};
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        jun.PoGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnTerimaBarang.on('Click', this.terimaForm, this);
        //this.btnFakturBeli.on('Click', this.fakturForm, this);
        this.status.on('select', function(){this.store.reload();}, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        //Ext.getCmp('separatorbtnFakturBeli').setVisible(SYSTEM_PEMBELIAN_1 && SYSTEM_PEMBELIAN_3);
        //Ext.getCmp('btnAddid').setVisible(SYSTEM_PEMBELIAN_1);
        //Ext.getCmp('separatorbtnEdit').setVisible(SYSTEM_PEMBELIAN_1);
        //Ext.getCmp('btnEditid').setVisible(SYSTEM_PEMBELIAN_1);
        //Ext.getCmp('separatorbtnTerimaBarang').setVisible(SYSTEM_PEMBELIAN_1 && SYSTEM_PEMBELIAN_5);
        //Ext.getCmp('btnTerimaBarangid').setVisible(SYSTEM_PEMBELIAN_5);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    terimaForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih purchase order");
            return;
        }
        if(selectedz.json.done == '1'){
            Ext.MessageBox.alert("Warning", "Penerimaan barang tidak bisa dibuat karena barang sudah diterima.");
            return;
        }
        var idz = selectedz.json.po_id;
        var form = new jun.TerimaWin({modez: selectedz.json.done});
        form.formz.getForm().loadRecord(this.record);
        //form.supplier.setValue(selectedz.json.supplier_id);
        //form.no_po.setValue(selectedz.json.no_po);
        form.onSuppChange();
        form.show();
        jun.rztTerimaDetil.baseParams = {
            po_id: idz
        };
        jun.rztTerimaDetil.reload();
        jun.rztTerimaDetil.baseParams = {};
        //Ext.Ajax.request({
        //    url: 'Terima/getDataByPO',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        po_id: idz
        //    },
        //    success: function (f, a) {
        //        var form = new jun.TerimaWin({modez: 0});
        //        form.formz.getForm().loadRecord(this.record);
        //        form.show(this);
        //        form.onSuppChange();
        //        var response = Ext.decode(f.responseText);
        //        if (response.total == '0')
        //            return;
        //        var result = response.results[0];
        //        form.no_lpb.setValue(result.no_lpb);
        //        form.tgl_terima.setValue(result.tgl_terima);
        //        form.btnSave.hide();
        //        form.btnSaveClose.hide();
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert(a.response.statusText, a.response.responseText);
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //        this.btnDisabled(false);
        //    }
        //});

    },
    loadForm: function () {
        var form = new jun.PoWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.po_id;
        var form = new jun.PoWin({modez: selectedz.json.done == '1' ? 2 : 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPoDetil.baseParams = {
            po_id: idz
        };
        jun.rztPoDetil.load();
        jun.rztPoDetil.baseParams = {};
        form.onSuppChange();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Po/delete/id/' + record.json.po_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPo.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
