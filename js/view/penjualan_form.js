jun.PenjualanWin = Ext.extend(Ext.Window, {
    title: 'Penjualan Tunai',
    modez: 1,
    width: 900,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Penjualan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. SO:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'so',
                        id: 'soid',
                        ref: '../so',
                        width: 175,
//                        readOnly: true,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'nama',
                        id: 'namaid',
                        ref: '../nama',
                        maxLength: 100,
                        allowBlank: false,
                        x: 85,
                        y: 92,
                        height: 20,
                        width: 175,
                        value: 'TUNAI'
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "50",
                        width: "175",
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        id: 'noteid',
                        height: "50",
                        width: "175",
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Kirim:",
                        x: 625,
                        y: 5
                    },
                    {
                        xtype: 'checkbox',
                        columnWidth: .5,
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'kirim',
                        id: 'kirimid',
                        ref: '../kirim',
                        x: 705,
                        y: 2,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Kas/Bank:",
                        x: 625,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        value: DEFKASPENJ,
                        width: 155,
                        x: 705,
                        y: 32
                    },
                    new jun.DetilPenjualanGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 425
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 422
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutoid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Biaya Angkut:",
                        x: 625,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'ongkos_angkut',
                        id: 'ongkos_angkutid',
                        ref: '../ongkos_angkut',
                        maxLength: 30,
                        value: 0,
                        x: 705,
                        y: 422
                    },
//                    {
//                        xtype: "label",
//                        text: "BG/Cek:",
//                        x: 625,
//                        y: 455
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'no_bg_cek',
//                        id: 'no_bg_cekid',
//                        ref: '../no_bg_cek',
//                        maxLength: 30,
//                        allowBlank: false,
//                        value: '-',
//                        x: 705,
//                        y: 452
//                    },
                    {
                        xtype: 'hidden',
                        id: 'sisa_tagihanid',
                        name: 'sisa_tagihan',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        id: 'sub_totalid',
                        name: 'sub_total'
                    },
                    {
                        xtype: 'hidden',
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        value: 0
                    },
                    {
                        xtype: "label",
                        text: "Total Bayar:",
                        x: 625,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 705,
                        y: 452
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PenjualanWin.superclass.initComponent.call(this);
        //this.btnRetur.on('click', this.onbtnReturclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.ongkos_angkut.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            //this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            //this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztDetilPenjualan.removeAll();
    },
//    onbtnReturclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztReturJual.findExact('parent', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.ReturJualWin();
//            if (jun.rztSetorKasSales.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            var tgl = this.tgl.getValue();
//            form.tgl.setValue(tgl);
//            form.show();
//            form.parent.setValue(this.id);
//        } else {
//            var retur = jun.rztReturJual.getAt(c);
//            form = new jun.ReturJualWin();
//            form.modez = 1;
//            form.formz.getForm().loadRecord(retur);
//            form.show();
////            Ext.getCmp('kas_keluarid').setValue(retur.data.kas_keluar);
//            jun.rztReturJualDetil.baseParams = {
//                penjualan_id: retur.data.penjualan_id
//            };
//            jun.rztReturJualDetil.load();
//            jun.rztReturJualDetil.baseParams = {};
//        }
//        var konsumen = this.konsumen.getValue();
//        var sales = this.sales.getValue();
//        var faktur = this.doc_ref.getValue();
//        form.konsumen_retur.setValue(konsumen);
//        form.sales.setValue(sales);
//        form.onSupplierChange();
//    },
//    onbtnBotolclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztBotol.findExact('penjualan_id', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.BotolWin();
//            if (jun.rztReturBotol.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            form.show();
//            var tgl = this.tgl.getValue();
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.penjualan_id.setValue(this.id);
//            form.tgl.setValue(tgl);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//        } else {
//            var botol = jun.rztBotol.getAt(c);
//            form = new jun.BotolWin({
//                modez: 1,
//                id: botol.data.botol_id
//            });
//            form.show();
//            form.formz.getForm().loadRecord(botol);
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.penjualan_id.setValue(this.id);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//            jun.rztBotolDetil.baseParams = {
//                botol_id: botol.data.botol_id
//            };
//            jun.rztBotolDetil.load();
//            jun.rztBotolDetil.baseParams = {};
//        }
//    },
    updateTotal: function () {
        jun.rztDetilPenjualan.refreshData();
    },
    //onKonsumenChange: function () {
    //    var konsumen_id = this.konsumen.getValue();
    //    var konsumen = jun.getKonsumen(konsumen_id);
    //    var area = jun.getArea(konsumen.data.area_id);
    //    this.alamat.setValue(konsumen.data.address);
    //    this.area.setText(area.data.area_name);
    //    var tgl = this.tgl.getValue();
    //    if (this.modez == 0) {
    //        var tempo = tgl.add(Date.DAY, parseInt(konsumen.data.tempo));
    //        this.tempo.setValue(tempo);
    //    }
    //    if (konsumen.data.pasar_id != null) {
    //        var pasar = jun.getPasar(konsumen.data.pasar_id);
    //        this.pasar.setText(pasar.data.pasar_name);
    //    }
    //},
    btnDisabled: function (status) {
        //this.btnRetur.setDisabled(status);
        this.btnSave.setDisabled(status);
        //this.btnBotol.setDisabled(status);
        //this.btnFinal.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    //onbtnFinalclick: function () {
    //    this.modez = 2;
    //    this.saveForm(true);
    //},
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Penjualan/create/';
        Ext.getCmp('form-Penjualan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztDetilPenjualan.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                //jun.rztPenjualan.baseParams = {
                //    tgl: this.tgl.getValue()
                //};
                jun.rztPenjualan.reload();
                //jun.rztPenjualan.baseParams = {};
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-Penjualan').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PenjualanKreditWin = Ext.extend(Ext.Window, {
    title: 'Penjualan Kredit',
    modez: 1,
    width: 900,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PenjualanKredit',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: "numericfield",
                        name: "tempo_day",
                        ref: "../tempo_day",
                        minValue: 0,
                        width: 175,
                        value: 0,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl Tempo:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tempo',
                        name: 'tempo',
                        id: 'tempoid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "No. SO:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'so',
                        id: 'soid',
                        ref: '../so',
                        width: 175,
//                        readOnly: true,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztKonsumenCmp,
                        hiddenName: 'konsumen_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        ref: '../konsumen_id',
                        allowBlank: false,
                        x: 375,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: 50,
                        width: 175,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Kirim:",
                        x: 625,
                        y: 5
                    },
                    {
                        xtype: 'checkbox',
                        columnWidth: .5,
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'kirim',
                        id: 'kirimid',
                        ref: '../kirim',
                        x: 705,
                        y: 2,
                        height: 20,
                        width: 165
                    },
                    {
                        xtype: "label",
                        text: "Kas/Bank:",
                        x: 625,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        width: 155,
                        value: DEFKASPENJ,
                        x: 705,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 625,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        id: 'noteid',
                        height: 50,
                        width: 155,
                        x: 705,
                        y: 62
                    },
                    new jun.DetilPenjualanGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 425
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 422
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: 'hidden',
                        id: 'sub_totalid',
                        name: 'sub_total'
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        value: 2
                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 5,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutoid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 85,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 5,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 85,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Biaya Angkut:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'ongkos_angkut',
                        id: 'ongkos_angkutid',
                        ref: '../ongkos_angkut',
                        maxLength: 30,
                        value: 0,
                        x: 375,
                        y: 422
                    },
//                    {
//                        xtype: "label",
//                        text: "BG/Cek:",
//                        x: 625,
//                        y: 455
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'no_bg_cek',
//                        id: 'no_bg_cekid',
//                        ref: '../no_bg_cek',
//                        maxLength: 30,
//                        allowBlank: false,
//                        value: '-',
//                        x: 705,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Total Bayar:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Uang Muka:",
                        x: 625,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        ref: '../uang_muka',
                        maxLength: 30,
                        value: 0,
                        x: 705,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Sisa Tagihan:",
                        x: 625,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'sisa_tagihan',
                        id: 'sisa_tagihanid',
                        ref: '../sisa_tagihan',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 705,
                        y: 452
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PenjualanKreditWin.superclass.initComponent.call(this);
        //this.btnRetur.on('click', this.onbtnReturclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.konsumen_id.on('select', this.onKonsumenSelect, this);
        this.uang_muka.on('change', this.updateTotal, this);
        this.ongkos_angkut.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
        this.tempo_day.on("change", this.onChangeTempoDay, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            //this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            //this.btnSaveClose.setVisible(true);
        }
    },
    onChangeTempoDay: function (field, b, l) {
        var tgl = this.tgl.getValue();
        var tempo = tgl.add(Date.DAY, parseInt(b));
        this.tempo.setValue(tempo);
    },
    onKonsumenSelect: function (combo, record, index) {
        this.alamat.setValue(record.data.alamat);
    },
    onWinClose: function () {
        jun.rztDetilPenjualan.removeAll();
    },
//    onbtnReturclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztReturJual.findExact('parent', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.ReturJualWin();
//            if (jun.rztSetorKasSales.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            var tgl = this.tgl.getValue();
//            form.tgl.setValue(tgl);
//            form.show();
//            form.parent.setValue(this.id);
//        } else {
//            var retur = jun.rztReturJual.getAt(c);
//            form = new jun.ReturJualWin();
//            form.modez = 1;
//            form.formz.getForm().loadRecord(retur);
//            form.show();
////            Ext.getCmp('kas_keluarid').setValue(retur.data.kas_keluar);
//            jun.rztReturJualDetil.baseParams = {
//                PenjualanKredit_id: retur.data.PenjualanKredit_id
//            };
//            jun.rztReturJualDetil.load();
//            jun.rztReturJualDetil.baseParams = {};
//        }
//        var konsumen = this.konsumen.getValue();
//        var sales = this.sales.getValue();
//        var faktur = this.doc_ref.getValue();
//        form.konsumen_retur.setValue(konsumen);
//        form.sales.setValue(sales);
//        form.onSupplierChange();
//    },
//    onbtnBotolclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztBotol.findExact('penjualan_id', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.BotolWin();
//            if (jun.rztReturBotol.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            form.show();
//            var tgl = this.tgl.getValue();
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.penjualan_id.setValue(this.id);
//            form.tgl.setValue(tgl);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//        } else {
//            var botol = jun.rztBotol.getAt(c);
//            form = new jun.BotolWin({
//                modez: 1,
//                id: botol.data.botol_id
//            });
//            form.show();
//            form.formz.getForm().loadRecord(botol);
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.penjualan_id.setValue(this.id);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//            jun.rztBotolDetil.baseParams = {
//                botol_id: botol.data.botol_id
//            };
//            jun.rztBotolDetil.load();
//            jun.rztBotolDetil.baseParams = {};
//        }
//    },
    updateTotal: function () {
        jun.rztDetilPenjualan.refreshData();
    },
    //onKonsumenChange: function () {
    //    var konsumen_id = this.konsumen.getValue();
    //    var konsumen = jun.getKonsumen(konsumen_id);
    //    var area = jun.getArea(konsumen.data.area_id);
    //    this.alamat.setValue(konsumen.data.address);
    //    this.area.setText(area.data.area_name);
    //    var tgl = this.tgl.getValue();
    //    if (this.modez == 0) {
    //        var tempo = tgl.add(Date.DAY, parseInt(konsumen.data.tempo));
    //        this.tempo.setValue(tempo);
    //    }
    //    if (konsumen.data.pasar_id != null) {
    //        var pasar = jun.getPasar(konsumen.data.pasar_id);
    //        this.pasar.setText(pasar.data.pasar_name);
    //    }
    //},
    btnDisabled: function (status) {
        //this.btnRetur.setDisabled(status);
        this.btnSave.setDisabled(status);
        //this.btnBotol.setDisabled(status);
        //this.btnFinal.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    //onbtnFinalclick: function () {
    //    this.modez = 2;
    //    this.saveForm(true);
    //},
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Penjualan/create/';
        Ext.getCmp('form-PenjualanKredit').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztDetilPenjualan.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPenjualan.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-PenjualanKredit').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.BayarWin = Ext.extend(Ext.Window, {
    title: 'Bayar',
    modez: 1,
    width: 670,
    height: 270,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 250,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'numericfield',
                        labelStyle: 'font: bold 40px arial;',
                        style: 'font: bold 40px arial;height:auto;text-align:right',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        fieldLabel: "TOTAL",
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        labelStyle: 'font: bold 40px arial;',
                        style: 'font: bold 40px arial;height:auto;text-align:right',
                        fieldLabel: "BAYAR",
                        ref: '../bayar',
                        maxLength: 30,
                        enableKeyEvents: true,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        labelStyle: 'font: bold 40px arial;',
                        style: 'font: bold 40px arial;height:auto;text-align:right',
                        fieldLabel: "KEMBALI",
                        ref: '../kembali',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ]
        ;
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Bayar',
                    ref: '../btnBayar'
                },
                {
                    xtype: 'button',
                    text: 'Tutup',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BayarWin.superclass.initComponent.call(this);
        this.on('show', this.onActivate, this);
        this.on('close', this.onWinClose, this);
        this.btnBayar.on('click', this.onBayarClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bayar.on('Keyup', this.onbayarPress, this);
    },
    onbayarPress: function (t, e) {
        var tot = parseFloat(this.total.getValue());
        var b = parseFloat(t.getValue());
        this.kembali.setValue(b - tot);
    },
    onBayarClick: function () {
        jun.rztPenjualan.rejectChanges();
        this.close();
    },
    onWinClose: function () {
    },
    onActivate: function () {
        var data = jun.rztPenjualan.query('bayar', true);
        var total = 0;
        data.each(function (entry) {
            total += parseFloat(entry.data.total);
        });
        this.total.setValue(total);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ReturPenjualanWin = Ext.extend(Ext.Window, {
    title: 'Retur Penjualan Tunai',
    modez: 1,
    width: 900,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturPenjualan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'nama',
                        id: 'namaid',
                        ref: '../nama',
                        maxLength: 100,
                        allowBlank: false,
                        x: 85,
                        y: 62,
                        height: 20,
                        width: 175,
                        value: 'TUNAI'
                    },
                    {
                        xtype: "label",
                        text: "Kas/Bank:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        value: DEFKASPENJ,
                        width: 175,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "50",
                        width: "175",
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        id: 'noteid',
                        height: "50",
                        width: "175",
                        x: 375,
                        y: 62
                    },
                    new jun.DetilReturPenjualanGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutoid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 625,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 705,
                        y: 422
                    },
                    {
                        xtype: 'hidden',
                        id: 'ongkos_angkutid',
                        name: 'ongkos_angkut',
                        value: 0
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Biaya Angkut:",
                    //    x: 625,
                    //    y: 425
                    //},
                    //{
                    //    xtype: 'numericfield',
                    //    name: 'ongkos_angkut',
                    //    id: 'ongkos_angkutid',
                    //    ref: '../ongkos_angkut',
                    //    maxLength: 30,
                    //    value: 0,
                    //    x: 705,
                    //    y: 422
                    //},
                    {
                        xtype: 'hidden',
                        name: 'sisa_tagihan',
                        id: 'sisa_tagihanid',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        value: -1
                    },
                    {
                        xtype: 'hidden',
                        id: 'sub_totalid',
                        name: 'sub_total'
                    },
                    {
                        xtype: 'hidden',
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        value: 0
                    },
                    {
                        xtype: "label",
                        text: "Kas Keluar:",
                        x: 625,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 705,
                        y: 452
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturPenjualanWin.superclass.initComponent.call(this);
        //this.btnRetur.on('click', this.onbtnReturclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //this.ongkos_angkut.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            //this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            //this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztDetilPenjualan.removeAll();
    },
//    onbtnReturclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztReturJual.findExact('parent', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.ReturJualWin();
//            if (jun.rztSetorKasSales.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            var tgl = this.tgl.getValue();
//            form.tgl.setValue(tgl);
//            form.show();
//            form.parent.setValue(this.id);
//        } else {
//            var retur = jun.rztReturJual.getAt(c);
//            form = new jun.ReturJualWin();
//            form.modez = 1;
//            form.formz.getForm().loadRecord(retur);
//            form.show();
////            Ext.getCmp('kas_keluarid').setValue(retur.data.kas_keluar);
//            jun.rztReturJualDetil.baseParams = {
//                ReturPenjualan_id: retur.data.ReturPenjualan_id
//            };
//            jun.rztReturJualDetil.load();
//            jun.rztReturJualDetil.baseParams = {};
//        }
//        var konsumen = this.konsumen.getValue();
//        var sales = this.sales.getValue();
//        var faktur = this.doc_ref.getValue();
//        form.konsumen_retur.setValue(konsumen);
//        form.sales.setValue(sales);
//        form.onSupplierChange();
//    },
//    onbtnBotolclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztBotol.findExact('ReturPenjualan_id', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.BotolWin();
//            if (jun.rztReturBotol.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            form.show();
//            var tgl = this.tgl.getValue();
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.ReturPenjualan_id.setValue(this.id);
//            form.tgl.setValue(tgl);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//        } else {
//            var botol = jun.rztBotol.getAt(c);
//            form = new jun.BotolWin({
//                modez: 1,
//                id: botol.data.botol_id
//            });
//            form.show();
//            form.formz.getForm().loadRecord(botol);
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.ReturPenjualan_id.setValue(this.id);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//            jun.rztBotolDetil.baseParams = {
//                botol_id: botol.data.botol_id
//            };
//            jun.rztBotolDetil.load();
//            jun.rztBotolDetil.baseParams = {};
//        }
//    },
    updateTotal: function () {
        jun.rztDetilPenjualan.refreshData();
    },
    //onKonsumenChange: function () {
    //    var konsumen_id = this.konsumen.getValue();
    //    var konsumen = jun.getKonsumen(konsumen_id);
    //    var area = jun.getArea(konsumen.data.area_id);
    //    this.alamat.setValue(konsumen.data.address);
    //    this.area.setText(area.data.area_name);
    //    var tgl = this.tgl.getValue();
    //    if (this.modez == 0) {
    //        var tempo = tgl.add(Date.DAY, parseInt(konsumen.data.tempo));
    //        this.tempo.setValue(tempo);
    //    }
    //    if (konsumen.data.pasar_id != null) {
    //        var pasar = jun.getPasar(konsumen.data.pasar_id);
    //        this.pasar.setText(pasar.data.pasar_name);
    //    }
    //},
    btnDisabled: function (status) {
        //this.btnRetur.setDisabled(status);
        this.btnSave.setDisabled(status);
        //this.btnBotol.setDisabled(status);
        //this.btnFinal.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    //onbtnFinalclick: function () {
    //    this.modez = 2;
    //    this.saveForm(true);
    //},
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturJual/create/';
        Ext.getCmp('form-ReturPenjualan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztDetilPenjualan.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                //jun.rztReturPenjualan.baseParams = {
                //    tgl: this.tgl.getValue()
                //};
                jun.rztReturPenjualan.reload();
                //jun.rztReturPenjualan.baseParams = {};
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-ReturPenjualan').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ReturPenjualanKreditWin = Ext.extend(Ext.Window, {
    title: 'Retur Penjualan Kredit',
    modez: 1,
    width: 900,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturPenjualanKredit',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: "numericfield",
                        name: "tempo_day",
                        ref: "../tempo_day",
                        minValue: 0,
                        width: 175,
                        value: 0,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl Tempo:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tempo',
                        name: 'tempo',
                        id: 'tempoid',
                        format: 'd M Y',
                        width: 175,
                        value: new Date(),
                        //readOnly: true,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Kas/Bank:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        width: 175,
                        value: DEFKASPENJ,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztKonsumenCmp,
                        hiddenName: 'konsumen_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        ref: '../konsumen_id',
                        allowBlank: false,
                        x: 375,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: 50,
                        width: 175,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 590,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        id: 'noteid',
                        height: 50,
                        width: 175,
                        x: 690,
                        y: 2
                    },
                    new jun.DetilReturPenjualanGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 425
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 422
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: 'hidden',
                        id: 'sub_totalid',
                        name: 'sub_total'
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        value: -2
                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 5,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutoid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 85,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 422
                    },
                    {
                        xtype: 'hidden',
                        id: 'ongkos_angkutid',
                        name: 'ongkos_angkut',
                        value: 0
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Biaya Angkut:",
                    //    x: 295,
                    //    y: 425
                    //},
                    //{
                    //    xtype: 'numericfield',
                    //    name: 'ongkos_angkut',
                    //    id: 'ongkos_angkutid',
                    //    ref: '../ongkos_angkut',
                    //    maxLength: 30,
                    //    value: 0,
                    //    x: 375,
                    //    y: 422
                    //},
//                    {
//                        xtype: "label",
//                        text: "BG/Cek:",
//                        x: 625,
//                        y: 455
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'no_bg_cek',
//                        id: 'no_bg_cekid',
//                        ref: '../no_bg_cek',
//                        maxLength: 30,
//                        allowBlank: false,
//                        value: '-',
//                        x: 705,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 375,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Kas Keluar:",
                        x: 590,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        ref: '../uang_muka',
                        maxLength: 30,
                        value: 0,
                        x: 690,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Kredit Piutang:",
                        x: 590,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'sisa_tagihan',
                        id: 'sisa_tagihanid',
                        ref: '../sisa_tagihan',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 690,
                        y: 452
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturPenjualanKreditWin.superclass.initComponent.call(this);
        //this.btnRetur.on('click', this.onbtnReturclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.konsumen_id.on('select', this.onKonsumenSelect, this);
        this.uang_muka.on('change', this.updateTotal, this);
        //this.ongkos_angkut.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
        this.tempo_day.on("change", this.onChangeTempoDay, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            //this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            //this.btnSaveClose.setVisible(true);
        }
    },
    onChangeTempoDay: function (field, b, l) {
        var tgl = this.tgl.getValue();
        var tempo = tgl.add(Date.DAY, parseInt(b));
        this.tempo.setValue(tempo);
    },
    onKonsumenSelect: function (combo, record, index) {
        this.alamat.setValue(record.data.alamat);
    },
    onWinClose: function () {
        jun.rztDetilPenjualan.removeAll();
    },
//    onbtnReturclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztReturJual.findExact('parent', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.ReturJualWin();
//            if (jun.rztSetorKasSales.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            var tgl = this.tgl.getValue();
//            form.tgl.setValue(tgl);
//            form.show();
//            form.parent.setValue(this.id);
//        } else {
//            var retur = jun.rztReturJual.getAt(c);
//            form = new jun.ReturJualWin();
//            form.modez = 1;
//            form.formz.getForm().loadRecord(retur);
//            form.show();
////            Ext.getCmp('kas_keluarid').setValue(retur.data.kas_keluar);
//            jun.rztReturJualDetil.baseParams = {
//                ReturPenjualanKredit_id: retur.data.ReturPenjualanKredit_id
//            };
//            jun.rztReturJualDetil.load();
//            jun.rztReturJualDetil.baseParams = {};
//        }
//        var konsumen = this.konsumen.getValue();
//        var sales = this.sales.getValue();
//        var faktur = this.doc_ref.getValue();
//        form.konsumen_retur.setValue(konsumen);
//        form.sales.setValue(sales);
//        form.onSupplierChange();
//    },
//    onbtnBotolclick: function () {
//        if (this.modez == 0) {
//            Ext.MessageBox.show({
//                title: 'Warning',
//                msg: "Faktur harus disimpan dulu.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.WARNING
//            });
//            return;
//        }
//        var c = jun.rztBotol.findExact('ReturPenjualan_id', this.id);
//        var form;
//        if (c == -1) {
//            form = new jun.BotolWin();
//            if (jun.rztReturBotol.totalLength == 0) {
//                form.modez = 0
//            } else {
//                form.modez = 1;
//            }
//            form.show();
//            var tgl = this.tgl.getValue();
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.ReturPenjualan_id.setValue(this.id);
//            form.tgl.setValue(tgl);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//        } else {
//            var botol = jun.rztBotol.getAt(c);
//            form = new jun.BotolWin({
//                modez: 1,
//                id: botol.data.botol_id
//            });
//            form.show();
//            form.formz.getForm().loadRecord(botol);
//            var sales = this.sales.getValue();
//            var faktur = this.doc_ref.getValue();
//            form.ReturPenjualan_id.setValue(this.id);
//            form.sales.setValue(sales);
//            form.no_faktur.setValue(faktur);
//            jun.rztBotolDetil.baseParams = {
//                botol_id: botol.data.botol_id
//            };
//            jun.rztBotolDetil.load();
//            jun.rztBotolDetil.baseParams = {};
//        }
//    },
    updateTotal: function () {
        jun.rztDetilPenjualan.refreshData();
    },
    //onKonsumenChange: function () {
    //    var konsumen_id = this.konsumen.getValue();
    //    var konsumen = jun.getKonsumen(konsumen_id);
    //    var area = jun.getArea(konsumen.data.area_id);
    //    this.alamat.setValue(konsumen.data.address);
    //    this.area.setText(area.data.area_name);
    //    var tgl = this.tgl.getValue();
    //    if (this.modez == 0) {
    //        var tempo = tgl.add(Date.DAY, parseInt(konsumen.data.tempo));
    //        this.tempo.setValue(tempo);
    //    }
    //    if (konsumen.data.pasar_id != null) {
    //        var pasar = jun.getPasar(konsumen.data.pasar_id);
    //        this.pasar.setText(pasar.data.pasar_name);
    //    }
    //},
    btnDisabled: function (status) {
        //this.btnRetur.setDisabled(status);
        this.btnSave.setDisabled(status);
        //this.btnBotol.setDisabled(status);
        //this.btnFinal.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    //onbtnFinalclick: function () {
    //    this.modez = 2;
    //    this.saveForm(true);
    //},
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturJual/create/';
        Ext.getCmp('form-ReturPenjualanKredit').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztDetilPenjualan.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztReturPenjualan.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-ReturPenjualanKredit').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});