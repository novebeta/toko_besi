jun.PelunasanPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanPiutangStoreId',
            url: 'PelunasanPiutang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_piutang_id'},
                {name: 'total'},
                {name: 'id_bank'},
                {name: 'no_bg_cek'},
                {name: 'konsumen_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'alamat'}
            ]
        }, cfg));
    }
});
jun.rztPelunasanPiutang = new jun.PelunasanPiutangstore();
//jun.rztPelunasanPiutang.load();
