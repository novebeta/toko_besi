jun.StockMovesWin = Ext.extend(Ext.Window, {
    title: 'StockMoves',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-StockMoves',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'trans_no',
                        hideLabel: false,
                        //hidden:true,
                        name: 'trans_no',
                        id: 'trans_noid',
                        ref: '../trans_no',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'type',
                        hideLabel: false,
                        //hidden:true,
                        name: 'type',
                        id: 'typeid',
                        ref: '../type',
                        maxLength: 6,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tran_date',
                        fieldLabel: 'tran_date',
                        name: 'tran_date',
                        id: 'tran_dateid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'price',
                        hideLabel: false,
                        //hidden:true,
                        name: 'price',
                        id: 'priceid',
                        ref: '../price',
                        maxLength: 22,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'reference',
                        hideLabel: false,
                        //hidden:true,
                        name: 'reference',
                        id: 'referenceid',
                        ref: '../reference',
                        maxLength: 40,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'qty',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty',
                        id: 'qtyid',
                        ref: '../qty',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'discount_percent',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discount_percent',
                        id: 'discount_percentid',
                        ref: '../discount_percent',
                        maxLength: 22,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'standard_cost',
                        hideLabel: false,
                        //hidden:true,
                        name: 'standard_cost',
                        id: 'standard_costid',
                        ref: '../standard_cost',
                        maxLength: 22,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'visible',
                        hideLabel: false,
                        //hidden:true,
                        name: 'visible',
                        id: 'visibleid',
                        ref: '../visible',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'gudang_id',
                        store: jun.rztGudang,
                        hiddenName: 'gudang_id',
                        valueField: 'gudang_id',
                        displayField: 'gudang_code',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'barang_id',
                        store: jun.rztBarang,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'barcode',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.StockMovesWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'StockMoves/update/id/' + this.id;
        } else {
            urlz = 'StockMoves/create/';
        }
        Ext.getCmp('form-StockMoves').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztStockMoves.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-StockMoves').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KelolaStokWin = Ext.extend(Ext.Window, {
    title: 'Pengelolaan Persediaan',
    modez: 1,
    width: 640,
    height: 400,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-KelolaStok',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'hidden',
                        name: 'trans_no'
                    },
                    {
                        xtype: 'hidden',
                        name: 'type'
                    },
                    {
                        xtype: 'hidden',
                        name: 'gudang_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'price',
                        value: 0
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tran_date',
                        fieldLabel: 'Tanggal',
                        name: 'tran_date',
                        format: 'd M Y'
//                        width: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Ref. Dok',
                        hideLabel: false,
                        //hidden:true,
                        name: 'reference',
                        ref: '../reference',
                        maxLength: 40
                        //allowBlank: ,
//                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'note',
                        fieldLabel: 'Keterangan',
                        height: "50",
                        anchor: '100%'
                    },
                    new jun.KelolaStokDetilGrid({
                        height: 200,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KelolaStokWin.superclass.initComponent.call(this);
        this.on("close", this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztKelolaStokDetil.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var jml_item = jun.rztKelolaStokDetil.data.length;
        if (jml_item < 2) {
            Ext.MessageBox.alert("Error", "Item detil tidak boleh kurang dari dua.");
            this.btnDisabled(false);
            return;
        }
        var min = false;
        var plus = false;
        jun.rztKelolaStokDetil.data.items.forEach(function (value, key) {
            if (value.data.qty < 0) {
                min = true;
            }
            if (value.data.qty > 0) {
                plus = true;
            }
        });
        if (!min || !plus) {
            Ext.MessageBox.alert("Error", "Item detil minimal terdiri dari satu barang berjumlah " +
                "positif<br>dan satu barang berjumlah negatif.");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'StockMoves/CreateKelolaStok/';
        Ext.getCmp('form-KelolaStok').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztKelolaStokDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztKelolaStok.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-KelolaStok').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KelolaStokDetilWin = Ext.extend(Ext.Window, {
    title: 'Detil Pengelolaan Persediaan',
    modez: 1,
    width: 450,
    height: 145,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-KelolaStokDetil',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        colspan: 5,
                        typeAhead: true,
                        fieldLabel: 'Nama Barang',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                            '<span style="float:left;width: 20%;">{barcode}</span><span >{barang_name}</span>',
                            "</div></tpl>"),
                        listWidth: 550,
                        store: jun.rztBarang,
                        valueField: 'barang_id',
                        displayField: 'barang_name',
                        forceSelection: true,
                        hideTrigger: true,
                        ref: '../barang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        name: 'Jumlah',
                        fieldLabel: 'Jumlah (PCS)',
                        ref: '../jml',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        jun.KelolaStokDetilWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var barang_id = this.barang.getValue();
        var jml = this.jml.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            this.btnDisabled(false);
            return
        }
        if (jml == 0) {
            Ext.MessageBox.alert("Error", "Jumlah tidak boleh nol.");
            this.btnDisabled(false);
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        var a = jun.rztKelolaStokDetil.findExact("barang_id", barang_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
            return
        }
        var c = jun.rztKelolaStokDetil.recordType,
            d = new c({
                barang_id: barang_id,
                qty: jml
            });
        jun.rztKelolaStokDetil.add(d);
        this.btnDisabled(false);
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SelisihStokWin = Ext.extend(Ext.Window, {
    title: 'Selisih Persediaan',
    modez: 1,
    width: 640,
    height: 400,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SelisihStok',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'hidden',
                        name: 'trans_no'
                    },
                    {
                        xtype: 'hidden',
                        name: 'type'
                    },
                    {
                        xtype: 'hidden',
                        name: 'gudang_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'price',
                        value: 0
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tran_date',
                        fieldLabel: 'Tanggal',
                        name: 'tran_date',
                        format: 'd M Y',
                        allowBlank: false
//                        width: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Ref. Dok',
                        hideLabel: false,
                        //hidden:true,
                        name: 'reference',
                        ref: '../reference',
                        maxLength: 40
                        //allowBlank: ,
//                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'note',
                        fieldLabel: 'Keterangan',
                        height: "50",
                        anchor: '100%'
                    },
                    new jun.SelisihStokDetilGrid({
                        height: 200,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SelisihStokWin.superclass.initComponent.call(this);
        this.on("close", this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztSelisihStokDetil.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'StockMoves/CreateSelisihStok/';
        Ext.getCmp('form-SelisihStok').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztSelisihStokDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztSelisihStok.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SelisihStok').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
                this.btnDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SelisihStokDetilWin = Ext.extend(Ext.Window, {
    title: 'Detil Selisih Persediaan',
    modez: 1,
    width: 450,
    height: 145,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SelisihStokDetil',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        colspan: 5,
                        typeAhead: true,
                        fieldLabel: 'Nama Barang',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                            '<span style="float:left;width: 20%;">{barcode}</span><span >{barang_name}</span>',
                            "</div></tpl>"),
                        listWidth: 550,
                        store: jun.rztBarang,
                        valueField: 'barang_id',
                        displayField: 'barang_name',
                        forceSelection: true,
                        hideTrigger: true,
                        ref: '../barang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        name: 'Jumlah',
                        fieldLabel: 'Jumlah (PCS)',
                        ref: '../jml',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        jun.SelisihStokDetilWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var barang_id = this.barang.getValue();
        var jml = this.jml.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        var a = jun.rztSelisihStokDetil.findExact("barang_id", barang_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
            return
        }
        var c = jun.rztSelisihStokDetil.recordType,
            d = new c({
                barang_id: barang_id,
                qty: jml
            });
        jun.rztSelisihStokDetil.add(d);
        this.btnDisabled(false);
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
