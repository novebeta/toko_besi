jun.GlTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "GlTrans",
    id: "docs-jun.GlTransGrid",
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "counter",
            sortable: !1,
            resizable: !0,
            dataIndex: "counter",
            hidden: !0,
            width: 20
        },
        {
            header: "Kode Rekening",
            sortable: !1,
            resizable: !0,
            dataIndex: "account",
            width: 100
        },
        {
            header: "Nama Rekening",
            sortable: !1,
            resizable: !0,
            dataIndex: "account",
            width: 250,
            renderer: jun.renderNamaChart
        },
        {
            header: "Debit",
            sortable: !1,
            resizable: !0,
            dataIndex: "debit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            width: 100
        },
        {
            header: "Kredit",
            sortable: !1,
            resizable: !0,
            dataIndex: "kredit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztGlTrans;
        this.tbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Tambah Debit",
                    ref: "../btnAdd"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    text: "Tambah Kredit",
                    ref: "../btnEdit"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    text: "Hapus",
                    ref: "../btnDelete"
                }
            ]
        };
        jun.GlTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.loadEditForm, this);
        this.btnDelete.on("Click", this.deleteRec, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    loadForm: function () {
        var a = new jun.GlTransDetilWin({
            modez: 0,
            debit: !0,
            title: "Tambah Debit"
        });
        a.show();
    },
    loadEditForm: function () {
        var a = new jun.GlTransDetilWin({
            modez: 0,
            debit: !1,
            title: "Tambah Kredit"
        });
        a.show();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        this.store.remove(b);
        this.store.refreshData();
    }
});
jun.JurnalUmum = Ext.extend(Ext.grid.GridPanel, {
    title: "General Journal",
    id: 'docs-jun.JurnalUmum',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tran_date',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'reference',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'tot_debit',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        //jun.rztJurnalUmum.on({
        //    scope: this,
        //    beforeload: {
        //        fn: function (a, b) {
        //            b.params = {
        //                tgl: Ext.getCmp('tgljurnalumumgridid').getValue()
        //            }
        //        }
        //    }
        //});
        this.store = jun.rztJurnalUmum;

        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add General Journal',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat General Journal',
                    ref: '../btnEdit'
                }
                //,
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Delete General Journal',
                //    ref: '../btnDel'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'label',
                //    style: 'margin:5px',
                //    text: 'Date :'
                //},
                //{
                //    xtype: 'xdatefield',
                //    ref: '../tgl',
                //    id : 'tgljurnalumumgridid'
                //}
            ]
        };
        jun.JurnalUmum.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDel.on('Click', this.deleteRec, this);
        //this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.reload();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.GlTransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a doctor");
            return;
        }
        var idz = selectedz.json.type_no;
        var form = new jun.GlTransWin({modez: 1, idju: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztGlTrans.baseParams = {
            type_no: idz
        };
        jun.rztGlTrans.load();
        jun.rztGlTrans.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'GlTrans/delete',
            method: 'POST',
            scope: this,
            params: {
                type_no: record.json.type_no
            },
            success: function (f, a) {
                this.refreshTgl();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});