jun.SetorKasSalesGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"SetorKasSales",
        id:'docs-jun.SetorKasSalesGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                        {
			header:'setor_kas_sales_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'setor_kas_sales_id',
			width:100
		},
                                {
			header:'doc_ref',
			sortable:true,
			resizable:true,                        
            dataIndex:'doc_ref',
			width:100
		},
                                {
			header:'tgl',
			sortable:true,
			resizable:true,                        
            dataIndex:'tgl',
			width:100
		},
                                {
			header:'faktur',
			sortable:true,
			resizable:true,                        
            dataIndex:'faktur',
			width:100
		},
                                {
			header:'pelunasan_piutang',
			sortable:true,
			resizable:true,                        
            dataIndex:'pelunasan_piutang',
			width:100
		},
                                {
			header:'retur_botol',
			sortable:true,
			resizable:true,                        
            dataIndex:'retur_botol',
			width:100
		},
                		/*
                {
			header:'biaya_sales',
			sortable:true,
			resizable:true,                        
            dataIndex:'biaya_sales',
			width:100
		},
                                {
			header:'retur_penjualan_tunai',
			sortable:true,
			resizable:true,                        
            dataIndex:'retur_penjualan_tunai',
			width:100
		},
                                {
			header:'kas_disetor',
			sortable:true,
			resizable:true,                        
            dataIndex:'kas_disetor',
			width:100
		},
                                {
			header:'salesman_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'salesman_id',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztSetorKasSales;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.SetorKasSalesGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.SetorKasSalesWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == ""){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.setor_kas_sales_id;
            var form = new jun.SetorKasSalesWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == ""){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'SetorKasSales/delete/id/' + record.json.setor_kas_sales_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztSetorKasSales.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert(a.response.statusText, a.response.responseText);
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
             });
        
        }
})
