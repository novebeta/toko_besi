jun.ReturPembelianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturPembelianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturPembelianStoreId',
            url: 'ReturPembelian',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'Pembelian_id'},
                {name: 'no_faktur_beli'},
                {name: 'tgl'},
                {name: 'tempo'},
                {name: 'sub_total'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'no_bg_cek'},
                {name: 'sisa_tagihan'},
                {name: 'lunas'},
                {name: 'final'},
                {name: 'id_user'},
                {name: 'disc'},
                {name: 'no_faktur_supp'},
                {name: 'tgl_tempo'},
                //{name: 'ref_id'},
                {name: 'id_bank'},
                {name: 'biaya_bongkar'},
                {name: 'biaya_angkut'},
                {name: 'allowance'},
                {name: 'supplier_id'}
            ]
        }, cfg));
    }
});
jun.rztReturPembelian = new jun.ReturPembelianstore({baseParams: {mode: "grid"}});
//jun.rztPembelian.load();
