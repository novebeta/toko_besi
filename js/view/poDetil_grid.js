jun.PoDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PoDetil",
    id: 'docs-jun.PoDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'Sat Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'sat'
        },
        {
            header: 'Jml Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPoDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Barang :'
                },
                {
                    xtype: 'combo',
                    colspan: 3,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    matchFieldWidth: !1,
                    itemSelector: "div.search-item-table",
                    tpl: new Ext.XTemplate('<div class="container">','<tpl for="."><div class="search-item-table">',
                        '<div class="cell4" style="width: 250px;">{barcode}</div>',
                        '<div class="cell4" style="width: 400px;">{barang_name}</div>',
                        '<div class="cell4" style="width: 100px;text-align: right;">{sisa:number("0,0")}</div>',
                        "</div></tpl>",'</div>'),
                    listWidth: 750,
                    store: jun.rztBarang,
                    valueField: 'barang_id',
                    displayField: 'barang_name',
                    forceSelection: true,
                    hideTrigger: true,
                    ref: '../barang',
                    width: 348
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: '\xA0Sat Beli :'
                },
                {
                    xtype: 'uctextfield',
                    hideLabel: false,
                    ref: '../sat',
                    maxLength: 15,
                    width: 20
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: '\xA0Jml Beli :'
                },
                {
                    xtype: 'numericfield',
                    id: 'jmlid',
                    ref: '../jml',
                    width: 50,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Tambah',
//                    height: 44,
                    ref: '../btnAdd'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
//                    height: 44,
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
//                    height: 44,
                    ref: '../btnDelete'
                }
            ]
        };
        jun.PoDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        //this.barang.on('select', this.onChangeBarang, this);
//        this.sat.on('change', this.onChangeSat, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztDetilPenjualan.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
//                return
//            }
//        }
        var sat = this.sat.getValue();
        if (sat == "") {
            Ext.MessageBox.alert("Error", "Satuan harus dipilih.");
            return;
        }
        //var barang = jun.getBarang(barang_id);
        //var pcs;
//        var price = parseFloat(this.hrg.getValue());
        var jml = parseFloat(this.jml.getValue());
        if (jml <= 0) {
            Ext.MessageBox.alert("Error", "Jumlah harus lebih dari 0.");
            return;
        }
//        var disc = parseFloat(this.disc.getValue());
//        var bruto = price * jml;
//        var totdisc1 = bruto * (disc1 / 100);
//        var totdisc2 = bruto * (disc2 / 100);
//        var totalpot = (totdisc1 + totdisc2) + pot;
//        var nominal = jun.calculateDiscPurchase(jml, price, disc);
//        var totalpot = bruto - nominal;
//        switch (sat) {
//            case "PCS":
//                pcs = jml;
//                break;
//            case "LSN":
//                pcs = Math.round(jml * 12);
//                break;
//            case "PAK":
//                pcs = Math.round(jml * parseFloat(barang.data.pak_2_pcs));
//                break;
//            default:
//                pcs = Math.round(jml * parseFloat(barang.data.max_2_pcs));
//        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('sat', sat);
            record.set('jml', jml);
//            record.set('price', price);
//            record.set('disc', disc);
//            record.set('totalpot', totalpot);
//            record.set('nominal', nominal);
            record.set('pcs', 0);
//            record.set('bruto', bruto);
            record.commit();
        } else {
            var c = jun.rztPoDetil.recordType,
                d = new c({
                    barang_id: barang_id,
                    sat: sat,
                    jml: jml,
//                    price: price,
//                    disc: disc,
//                    totalpot: totalpot,
//                    nominal: nominal,
                    pcs: 0
//                    bruto: bruto
                });
            jun.rztPoDetil.add(d);
        }
        this.barang.reset();
        this.sat.reset();
        this.jml.reset();
//        this.hrg.reset();
//        this.disc.reset();
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Ubah') {
            this.barang.setValue(record.data.barang_id);
            this.onChangeBarang();
            this.sat.setValue(record.data.sat);
            //this.onChangeSat();
            this.jml.setValue(record.data.jml);
//            this.disc.setValue(record.data.disc);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Ubah");
            this.btnDisable(false);
        }
    },
    onChangeBarang: function () {
        //var barang_id = this.barang.getValue();
        //if (barang_id == "") {
        //    Ext.MessageBox.alert("Error", "Barang harus dipilih.");
        //    return
        //}
        //var barang = jun.getBarang(barang_id);
        //this.sat.setValue(barang.data.satuan);
        //if (sat == "") {
        //    return
        //}
//        switch (sat) {
//            case "PCS":
//                this.hrg.setValue(barang.data.price_pcs);
//                break;
//            case "LSN":
//                this.hrg.setValue(barang.data.price_lsn);
//                break;
//            case "PAK":
//                this.hrg.setValue(barang.data.price_pak);
//                break;
//            default:
//                this.hrg.setValue(barang.data.price_max);
//        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
