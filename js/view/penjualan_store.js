jun.Penjualanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Penjualanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PenjualanStoreId',
            url: 'Penjualan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_id'},
                {name: 'doc_ref'},
                {name: 'tempo'},
                {name: 'sub_total', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'uang_muka', type: 'float'},
                {name: 'no_bg_cek'},
                {name: 'sisa_tagihan', type: 'float'},
                {name: 'lunas'},
                {name: 'final'},
                {name: 'tgl'},
                {name: 'id_user'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'tdate'},
                {name: 'arus'},
                {name: 'konsumen_id'},
                {name: 'type_'},
                {name: 'so'},
                {name: 'kirim'},
                {name: 'alamat'},
                {name: 'nama'},
                {name: 'ongkos_angkut'},
                {name: 'note'},
                {name: 'tempo_day'},
                {name: 'id_bank'},
                {name: 'bayar'},
                {name: 'status_kirim'}
            ]
        }, cfg));
    }
});
jun.rztPenjualan = new jun.Penjualanstore({baseParams: {arus: 1}});
jun.rztPenjualanKredit = new jun.Penjualanstore({baseParams: {arus: 2}});
jun.rztReturPenjualan = new jun.Penjualanstore({baseParams: {arus: -1}});
jun.rztReturPenjualanKredit = new jun.Penjualanstore({baseParams: {arus: -2}});
