jun.ReturPembelianDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturPembelianDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturPembelianDetilStoreId',
            url: 'ReturPembelian/DetilRetur',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_detil_id'},
                {name: 'barang_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'nominal', type: 'float'},
                {name: 'pcs', type: 'float'},
                {name: 'totalpot', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'Pembelian_id'},
                {name: 'disc2', type: 'float'},
                {name: 'pot', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        //var disc = this.sum("totalpot");
        var disc = parseFloat(Ext.getCmp("discid").getValue());
        //var bruto = this.sum("bruto");
        var biaya_angkut = parseFloat(Ext.getCmp("biaya_angkutid").getValue());
        var nominal = this.sum("nominal");
        //Ext.getCmp("discid").setValue(disc);
        Ext.getCmp("sub_totalid").setValue(nominal);
        var total = nominal - disc + biaya_angkut;
        Ext.getCmp("totalid").setValue(total);
        var dp = parseFloat(Ext.getCmp("uang_mukaid").getValue());
        if (total - dp < 0) {
            Ext.getCmp("uang_mukaid").setValue(0);
            Ext.getCmp("sisa_tagihanid").setValue(total);
        } else {
            Ext.getCmp("sisa_tagihanid").setValue(total - dp);
        }
    }
});
jun.rztReturPembelianDetil = new jun.ReturPembelianDetilstore();
//jun.rztPembelianDetil.load();
