jun.SetorKasSalesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.SetorKasSalesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SetorKasSalesStoreId',
            url: 'SetorKasSales',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'setor_kas_sales_id'},
{name:'doc_ref'},
{name:'tgl'},
{name:'faktur'},
{name:'pelunasan_piutang'},
{name:'retur_botol'},
{name:'biaya_sales'},
{name:'retur_penjualan_tunai'},
{name:'kas_disetor'},
{name:'salesman_id'},
                
            ]
        }, cfg));
    }
});
jun.rztSetorKasSales = new jun.SetorKasSalesstore();
//jun.rztSetorKasSales.load();
