// TODO : report penjualan kurang lebar
jun.LapKasMasuk = Ext.extend(Ext.Window, {
    title: "Laporan Kas Masuk",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapKasMasuk",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        readOnly: true
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%',
                        readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapKasMasuk.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapKasMasuk").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapKasMasuk").getForm().url = "Report/LapKasMasuk";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapKasMasuk').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapKasMasuk").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapKasMasuk").getForm().url = "Report/LapKasMasuk";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapKasMasuk').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapPengiriman = Ext.extend(Ext.Window, {
    title: "Laporan Pengiriman",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapPengiriman",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        readOnly: true
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%',
                        readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapPengiriman.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapPengiriman").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPengiriman").getForm().url = "Report/LapPengiriman";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapPengiriman').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapPengiriman").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPengiriman").getForm().url = "Report/LapPengiriman";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapPengiriman').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapMasterKonsumen = Ext.extend(Ext.Window, {
    title: "Cetak Laporan Konsumen (Filter)",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 275,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapMasterKonsumen",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Kode Konsumen',
                        xtype: 'uctextfield',
                        name: "konsumen_code",
                        ref: '../konsumen_code',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'Nama Konsumen',
                        xtype: 'uctextfield',
                        name: "konsumen_name",
                        ref: '../konsumen_name',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'Phone',
                        xtype: 'uctextfield',
                        name: "phone",
                        ref: '../phone',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'HP',
                        xtype: 'uctextfield',
                        name: "hp",
                        ref: '../hp',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'HP 2',
                        xtype: 'uctextfield',
                        name: "hp2",
                        ref: '../hp2',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'Tempo',
                        xtype: 'uctextfield',
                        name: "tempo",
                        ref: '../tempo',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        fieldLabel: 'Status',
                        xtype: 'textfield',
                        name: "status",
                        ref: '../status',
                        anchor: '100%',
                        //readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapMasterKonsumen.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapMasterKonsumen").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapMasterKonsumen").getForm().url = "Report/LapMasterKonsumen";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapMasterKonsumen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapMasterKonsumen").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapMasterKonsumen").getForm().url = "Report/LapMasterKonsumen";
        this.format.setValue("phpexcel");
        var form = Ext.getCmp('form-LapMasterKonsumen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportDOS = Ext.extend(Ext.Window, {
    title: "DO Sementara",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportDOS",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        readOnly: true
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%',
                        readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        }, jun.ReportDOS.superclass.initComponent.call(this), this.btnSave.on("click", this.onbtnSaveclick,
            this), this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDOS").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDOS").getForm().url = "Report/DosSementara";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportDOS').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportDOS").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDOS").getForm().url = "Report/DosSementara";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportDOS').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSetorKas = Ext.extend(Ext.Window, {
    title: "Setoran Kas",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportSetorKas",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        readOnly: true
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%',
                        readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        }, jun.ReportSetorKas.superclass.initComponent.call(this), this.btnSave.on("click", this.onbtnSaveclick,
            this), this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSetorKas").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSetorKas").getForm().url = "Report/SetorKasSales";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportSetorKas').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSetorKas").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSetorKas").getForm().url = "Report/SetorKasSales";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSetorKas').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSetorKasPeriode = Ext.extend(Ext.Window, {
    title: "Setoran Kas",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportSetorKasPeriode",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        ref: '../from_date',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        ref: '../to_date',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                //{
                //    xtype: "button",
                //    iconCls: "silk13-page_white_acrobat",
                //    text: "Save to PDF",
                //    ref: "../btnPdf"
                //}
            ]
        }, jun.ReportSetorKasPeriode.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSetorKasPeriode").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSetorKasPeriode").getForm().url = "Report/SetoranKas";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportSetorKasPeriode').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSetorKasPeriode").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSetorKasPeriode").getForm().url = "Report/SetoranKas";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSetorKasPeriode').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.PrintFakturJual = Ext.extend(Ext.Window, {
    title: "Cetak Faktur Penjualan",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-PrintFakturJual",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        fieldLabel: 'No. Faktur',
                        ref: '../doc_ref',
                        height: 20,
                        readOnly: true
                    },
                    {
                        xtype: "hidden",
                        name: "penjualan_id",
                        ref: "../penjualan_id"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-printer",
                    text: "Print",
                    ref: "../btnPrint"
                }
            ]
        };
        jun.PrintFakturJual.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnPrint.on("click", this.onbtnPrintclick, this);
    },
    onbtnPrintclick: function (el) {
        this.format.setValue("html");
        Ext.getCmp("form-PrintFakturJual").getForm().standardSubmit = false;
        var win = window.open('', '_blank', 'width=1024,height=768,menubar=1');
        if (win == null) {
            alert("Pop-up is blocked!");
            return;
        }
        Ext.getCmp('form-PrintFakturJual').getForm().submit({
            url: 'Report/PrintFakturJual',
            method: 'POST',
            scope: this,
            success: function (f, a) {
//                var response = Ext.decode(a.responseText);
//                var response = f.responseText;
                win.document.write(a.result.msg);
                win.print();
            },
            failure: function (f, a) {
                win.close();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
//        var a = this.trans_date_mulai.hiddenField.dom.value;
//        b = this.trans_date_sampai.hiddenField.dom.value;
//        c = window.open("", "form", "width=800,height=600,location=no,menubar=0,status=0,resizeable,scrollbars");
//        c.document.write("<html><title>Mutasi Kas di Tangan</title><body><form id='form' method='POST' action='PondokHarapan/PahReport/MutasiKasDiTangan'><input type='hidden' name='trans_date_mulai' value='" + a + "'>" + "<input type='hidden' name='trans_date_sampai' value='" + b + "'>" + "<input type='hidden' name='format' value='html'>" + "</form></body></html>");
//        c.document.close(), c.document.getElementById("form").submit();
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-PrintFakturJual").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintFakturJual").getForm().url = "Report/PrintFakturJual";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-PrintFakturJual').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-PrintFakturJual").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintFakturJual").getForm().url = "Report/PrintFakturJual";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-PrintFakturJual').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapPenjualanTunai = Ext.extend(Ext.Window, {
    title: "Laporan Penjualan Tunai ",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 305,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztUsersCmp.getTotalCount() === 0) {
            jun.rztUsersCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                defaults: {
                    // applied to each contained panel
                    bodyStyle: "background-color: #E4E4E4; padding: 10px"
                },
                id: "form-LapPenjualanTunai",
                labelWidth: 100,
                labelAlign: "left",
                layout: "table",
                layoutConfig: {columns: 3},
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Dari Tanggal :'
                    },
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        colspan: 2,
                        format: 'd M Y',
                        name: "from_date",
                        ref: '../from_date',
                        allowBlank: false,
                        value: new Date(),
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Sampai Tanggal :'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        colspan: 2,
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        ref: '../to_date',
                        value: new Date(),
                        allowBlank: false,
                        anchor: '100%'
                    },
//                    {
//                        xtype: "label",
//                        text: 'Satuan :'
//                    },
//                    {
//                        xtype: 'combo',
//                        colspan:2,
//                        typeAhead: true,
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        fieldLabel: 'Satuan',
//                        store: new Ext.data.ArrayStore({
//                            fields: ["id", "text"],
//                            data: [
//                                ["PCS", "Pieces"],
//                                ["MAX", "Satuan Terbesar"]
//                            ]
//                        }),
//                        hiddenName: 'satuan',
//                        value: 'PCS',
//                        valueField: 'id',
//                        forceSelection: true,
//                        displayField: 'text',
//                        ref: '../satuan',
//                        allowBlank: false,
//                        anchor: '100%'
//                    },
                    {
                        xtype: "label",
                        text: 'Jenis Transaksi :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["P", "Penjualan"],
                                ["RP", "Retur Penjualan"]
                            ]
                        }),
                        hiddenName: 'jnstrans',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnstrans',
                        emptyText: "SEMUA",
                        hiddenValue: 'P',
                        value: 'P',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Kasir :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kasir',
                        store: jun.rztUsersCmp,
                        hiddenName: 'id_user',
                        valueField: 'id_user',
                        forceSelection: true,
                        displayField: 'username',
                        ref: '../usercmp',
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Laporan :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["D", "Detil"],
                                ["S", "Summary"]
                            ]
                        }),
                        hiddenName: 'jnslap',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnslap',
                        emptyText: "",
                        hiddenValue: 'S',
                        value: 'S',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    //{
                    //    xtype: "label",
                    //    text: 'Laporan :'
                    //},
                    //{
                    //    xtype: 'combo',
                    //    colspan: 2,
                    //    typeAhead: true,
                    //    triggerAction: 'all',
                    //    lazyRender: true,
                    //    mode: 'local',
                    //    fieldLabel: 'Laporan',
                    //    store: new Ext.data.ArrayStore({
                    //        fields: ["id", "text"],
                    //        data: [
                    //            ["LPP", "Laporan Penjualan Principle"],
                    //            ["LPPR", "Laporan Penjualan Principle Rekap"],
                    //            ["LP", "Laporan Penjualan"],
                    //            ["LPR", "Laporan Penjualan Rekap"],
                    //            ["LDN", "Laporan Detail Nota"]
                    //        ]
                    //    }),
                    //    hiddenName: 'jnslap',
                    //    value: 'LPP',
                    //    valueField: 'id',
                    //    forceSelection: true,
                    //    displayField: 'text',
                    //    listWidth: 350,
                    //    matchFieldWidth: !1,
                    //    ref: '../lap',
                    //    allowBlank: false,
                    //    anchor: '100%'
                    //},
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapPenjualanTunai.superclass.initComponent.call(this);
        //this.cekSales.on("check", this.onCekSales, this);
        //this.supplier.on("check", this.onsupplier, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        //this.cekpasar.on("check", this.oncekpasar, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        //this.konsumen_cmb.on("change", this.onKonsumenChange, this);
        //this.pasar.on("change", this.onPasarChange, this);
    },
    onsupplier: function (c, s) {
        //this.supplier_code.setDisabled(!s);
    },
    oncekpasar: function (c, s) {
        //this.pasar.setDisabled(!s);
    },
    onkonsumen: function (c, s) {
        //this.konsumen_cmb.setDisabled(!s);
    },
    onCekSales: function (c, s) {
        this.sales.setDisabled(!s);
    },
    onPasarChange: function () {
        //var pasar_id = this.pasar.getValue();
        //if (pasar_id == '') {
        //    this.konsumen_cmb.setReadOnly(false);
        //    this.konsumen_cmb.reset();
        //} else {
        //    this.konsumen_cmb.reset();
        //    this.konsumen_cmb.setReadOnly(true);
        //}
    },
    onKonsumenChange: function () {
        var konsumen_id = this.konsumen_cmb.getValue();
        if (konsumen_id == '') {
            this.pasar.setReadOnly(false);
            this.pasar.reset();
        } else {
            var konsumen = jun.getKonsumen(konsumen_id);
            if (konsumen.data.pasar_id != "") {
                this.pasar.setValue(konsumen.data.pasar_id);
            } else {
                this.pasar.reset();
            }
            this.pasar.setReadOnly(true);
        }
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapPenjualanTunai").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualanTunai").getForm().url = "Report/LapPenjualanTunai";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapPenjualanTunai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapPenjualanTunai").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualanTunai").getForm().url = "Report/LapPenjualanTunai";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapPenjualanTunai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapPenjualanKredit = Ext.extend(Ext.Window, {
    title: "Laporan Penjualan Kredit ",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 305,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztUsersCmp.getTotalCount() === 0) {
            jun.rztUsersCmp.load();
        }
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                defaults: {
                    // applied to each contained panel
                    bodyStyle: "background-color: #E4E4E4; padding: 10px"
                },
                id: "form-LapPenjualanKredit",
                labelWidth: 100,
                labelAlign: "left",
                layout: "table",
                layoutConfig: {columns: 3},
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Dari Tanggal :'
                    },
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        colspan: 2,
                        format: 'd M Y',
                        name: "from_date",
                        ref: '../from_date',
                        allowBlank: false,
                        value: new Date(),
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Sampai Tanggal :'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        colspan: 2,
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        ref: '../to_date',
                        value: new Date(),
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Transaksi :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["P", "Penjualan"],
                                ["RP", "Retur Penjualan"]
                            ]
                        }),
                        hiddenName: 'jnstrans',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnstrans',
                        emptyText: "SEMUA",
                        hiddenValue: 'P',
                        value: 'P',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Kasir :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kasir',
                        store: jun.rztUsersCmp,
                        hiddenName: 'id_user',
                        valueField: 'id_user',
                        forceSelection: true,
                        displayField: 'username',
                        ref: '../usercmp',
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Konsumen :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Konsumen',
                        store: jun.rztKonsumenCmp,
                        hiddenName: 'orang_id',
                        valueField: 'orang_id',
                        forceSelection: true,
                        displayField: 'nama',
                        ref: '../orangcmp',
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Laporan :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["D", "Detil"],
                                ["S", "Summary"]
                            ]
                        }),
                        hiddenName: 'jnslap',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnslap',
                        emptyText: "",
                        hiddenValue: 'S',
                        value: 'S',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapPenjualanKredit.superclass.initComponent.call(this);
        //this.cekSales.on("check", this.onCekSales, this);
        //this.supplier.on("check", this.onsupplier, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        //this.cekpasar.on("check", this.oncekpasar, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        //this.konsumen_cmb.on("change", this.onKonsumenChange, this);
        //this.pasar.on("change", this.onPasarChange, this);
    },
    onsupplier: function (c, s) {
        //this.supplier_code.setDisabled(!s);
    },
    oncekpasar: function (c, s) {
        //this.pasar.setDisabled(!s);
    },
    onkonsumen: function (c, s) {
        //this.konsumen_cmb.setDisabled(!s);
    },
    onCekSales: function (c, s) {
        this.sales.setDisabled(!s);
    },
    onPasarChange: function () {
        //var pasar_id = this.pasar.getValue();
        //if (pasar_id == '') {
        //    this.konsumen_cmb.setReadOnly(false);
        //    this.konsumen_cmb.reset();
        //} else {
        //    this.konsumen_cmb.reset();
        //    this.konsumen_cmb.setReadOnly(true);
        //}
    },
    onKonsumenChange: function () {
        var konsumen_id = this.konsumen_cmb.getValue();
        if (konsumen_id == '') {
            this.pasar.setReadOnly(false);
            this.pasar.reset();
        } else {
            var konsumen = jun.getKonsumen(konsumen_id);
            if (konsumen.data.pasar_id != "") {
                this.pasar.setValue(konsumen.data.pasar_id);
            } else {
                this.pasar.reset();
            }
            this.pasar.setReadOnly(true);
        }
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapPenjualanKredit").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualanKredit").getForm().url = "Report/LapPenjualanKredit";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapPenjualanKredit').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapPenjualanKredit").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualanKredit").getForm().url = "Report/LapPenjualanKredit";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapPenjualanKredit').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapPenjualan = Ext.extend(Ext.Window, {
    title: "Laporan Penjualan ",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 305,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztUsersCmp.getTotalCount() === 0) {
            jun.rztUsersCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                defaults: {
                    // applied to each contained panel
                    bodyStyle: "background-color: #E4E4E4; padding: 10px"
                },
                id: "form-LapPenjualan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "table",
                layoutConfig: {columns: 3},
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Dari Tanggal :'
                    },
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        colspan: 2,
                        format: 'd M Y',
                        name: "from_date",
                        ref: '../from_date',
                        allowBlank: false,
                        value: new Date(),
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Sampai Tanggal :'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        colspan: 2,
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        ref: '../to_date',
                        value: new Date(),
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Transaksi :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["P", "Penjualan"],
                                ["RP", "Retur Penjualan"]
                            ]
                        }),
                        hiddenName: 'jnstrans',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnstrans',
                        emptyText: "SEMUA",
                        hiddenValue: 'P',
                        value: 'P',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Kasir :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kasir',
                        store: jun.rztUsersCmp,
                        hiddenName: 'id_user',
                        valueField: 'id_user',
                        forceSelection: true,
                        displayField: 'username',
                        ref: '../usercmp',
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Laporan :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["D", "Detil"],
                                ["S", "Summary"]
                            ]
                        }),
                        hiddenName: 'jnslap',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnslap',
                        emptyText: "",
                        hiddenValue: 'S',
                        value: 'S',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.LapPenjualan.superclass.initComponent.call(this);
        //this.cekSales.on("check", this.onCekSales, this);
        //this.supplier.on("check", this.onsupplier, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        //this.cekpasar.on("check", this.oncekpasar, this);
        //this.konsumen.on("check", this.onkonsumen, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        //this.konsumen_cmb.on("change", this.onKonsumenChange, this);
        //this.pasar.on("change", this.onPasarChange, this);
    },
    onsupplier: function (c, s) {
        //this.supplier_code.setDisabled(!s);
    },
    oncekpasar: function (c, s) {
        //this.pasar.setDisabled(!s);
    },
    onkonsumen: function (c, s) {
        //this.konsumen_cmb.setDisabled(!s);
    },
    onCekSales: function (c, s) {
        this.sales.setDisabled(!s);
    },
    onPasarChange: function () {
        //var pasar_id = this.pasar.getValue();
        //if (pasar_id == '') {
        //    this.konsumen_cmb.setReadOnly(false);
        //    this.konsumen_cmb.reset();
        //} else {
        //    this.konsumen_cmb.reset();
        //    this.konsumen_cmb.setReadOnly(true);
        //}
    },
    onKonsumenChange: function () {
        var konsumen_id = this.konsumen_cmb.getValue();
        if (konsumen_id == '') {
            this.pasar.setReadOnly(false);
            this.pasar.reset();
        } else {
            var konsumen = jun.getKonsumen(konsumen_id);
            if (konsumen.data.pasar_id != "") {
                this.pasar.setValue(konsumen.data.pasar_id);
            } else {
                this.pasar.reset();
            }
            this.pasar.setReadOnly(true);
        }
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapPenjualan").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualan").getForm().url = "Report/LapPenjualan";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapPenjualan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapPenjualan").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPenjualan").getForm().url = "Report/LapPenjualan";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapPenjualan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapPiutangSales = Ext.extend(Ext.Window, {
    title: "Laporan Piutang Per Salesman",
    iconCls: "silk13-report",
    modez: 1,
    width: 725,
    height: 430,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapPiutangSales",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Tanggal',
                        ref: '../tgl',
                        name: 'tgl'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false
                    },
                    new jun.PiutangPerSalesGrid({
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "hidden",
                        name: "detil",
                        ref: "../detil"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        jun.LapPiutangSales.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.sales.on("change", this.onSalesChange, this);
        this.tgl.on("change", this.onSalesChange, this);
        this.on("close", this.onWinClose, this);
    },
    onWinClose: function () {
        jun.rztPiutangPerSales.removeAll();
    },
    onSalesChange: function () {
        var tgl = this.tgl.getValue();
        var sales = this.sales.getValue();
        if (tgl == "" || sales == "") {
            return;
        }
        jun.rztPiutangPerSales.baseParams = {
            sales: sales,
            tgl: tgl
        };
        jun.rztPiutangPerSales.load();
        jun.rztPiutangPerSales.baseParams = {};
    },
    onbtnPdfclick: function () {
        this.detil.setValue(Ext.encode(Ext.pluck(
            jun.rztPiutangPerSales.data.items, "data")));
        Ext.getCmp("form-LapPiutangSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPiutangSales").getForm().url = "Report/LapPiutangSales";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapPiutangSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        this.detil.setValue(Ext.encode(Ext.pluck(
            jun.rztPiutangPerSales.data.items, "data")));
        Ext.getCmp("form-LapPiutangSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapPiutangSales").getForm().url = "Report/LapPiutangSales";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapPiutangSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapBiayaSales = Ext.extend(Ext.Window, {
    title: "Laporan Biaya Per Salesman",
    iconCls: "silk13-report",
    modez: 1,
    width: 370,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapBiayaSales",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Dari Tanggal',
                        ref: '../drtgl',
                        name: 'drtgl'
                    },
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Sampai Tanggal',
                        ref: '../ketgl',
                        name: 'ketgl'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "hidden",
                        name: "detil",
                        ref: "../detil"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        jun.LapBiayaSales.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapBiayaSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapBiayaSales").getForm().url = "Report/LapBiayaSales";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapBiayaSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapBiayaSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapBiayaSales").getForm().url = "Report/LapBiayaSales";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapBiayaSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapBotolSales = Ext.extend(Ext.Window, {
    title: "Laporan Botol Per Salesman",
    iconCls: "silk13-report",
    modez: 1,
    width: 370,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapBotolSales",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Dari Tanggal',
                        ref: '../drtgl',
                        name: 'drtgl'
                    },
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Sampai Tanggal',
                        ref: '../ketgl',
                        name: 'ketgl'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "hidden",
                        name: "detil",
                        ref: "../detil"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        jun.LapBotolSales.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-LapBotolSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapBotolSales").getForm().url = "Report/LapBotolSales";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapBotolSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-LapBotolSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapBotolSales").getForm().url = "Report/LapBotolSales";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-LapBotolSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LapDaftarPiutang = Ext.extend(Ext.Window, {
    title: "Laporan Daftar Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 285,
    height: 125,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-LapDaftarPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        fieldLabel: 'Tanggal',
                        ref: '../tgl',
                        name: 'tgl'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }//,
                //{
                //    xtype: "button",
                //    iconCls: "silk13-page_white_acrobat",
                //    text: "Save to PDF",
                //    ref: "../btnPdf"
                //}
            ]
        };
        jun.LapDaftarPiutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.on("close", this.onWinClose, this);
    },
    onWinClose: function () {
        jun.rztPiutangPerSales.removeAll();
    },
    onbtnPdfclick: function () {
//        this.detil.setValue(Ext.encode(Ext.pluck(
//            jun.rztPiutangPerSales.data.items, "data")));
        Ext.getCmp("form-LapDaftarPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapDaftarPiutang").getForm().url = "Report/LapDaftarPiutang";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-LapDaftarPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
//        this.detil.setValue(Ext.encode(Ext.pluck(
//            jun.rztPiutangPerSales.data.items, "data")));
        Ext.getCmp("form-LapDaftarPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-LapDaftarPiutang").getForm().url = "Report/LapDaftarPiutang";
        this.format.setValue("phpexcel");
        var form = Ext.getCmp('form-LapDaftarPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLabaKotorSales = Ext.extend(Ext.Window, {
    title: "Laba Kotor Salesman",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportLabaKotorSales",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
//                        hiddenValue: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_acrobat",
                    text: "Save to PDF",
                    ref: "../btnPdf"
                }
            ]
        };
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        jun.ReportLabaKotorSales.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaKotorSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaKotorSales").getForm().url = "Report/LabaKotorSales";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaKotorSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLabaKotorSales").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaKotorSales").getForm().url = "Report/LabaKotorSales";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportLabaKotorSales').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportNotaDebet = Ext.extend(Ext.Window, {
    title: "Nota Debet",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportNotaDebet",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Pemasok',
                        store: jun.rztSupplier,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        forceSelection: true,
                        displayField: 'supplier_name',
                        ref: '../supplier_code',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        allowBlank: true,
                        emptyText: "SEMUA",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../sales',
                        fieldLabel: 'Salesman',
                        store: jun.rztSalesman,
                        forceSelection: true,
                        hiddenName: 'salesman_id',
                        valueField: 'salesman_id',
                        displayField: 'salesman_name',
                        emptyText: "SEMUA",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        if (jun.rztSalesman.getTotalCount() === 0) {
            jun.rztSalesman.load();
        }
        if (jun.rztSupplier.getTotalCount() === 0) {
            jun.rztSupplier.load();
        }
        jun.ReportNotaDebet.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNotaDebet").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNotaDebet").getForm().url = "Report/NotaDebet";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportNotaDebet').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportNotaDebet").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNotaDebet").getForm().url = "Report/NotaDebet";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportNotaDebet').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBarangSedangPesan = Ext.extend(Ext.Window, {
    title: "Barang Sedang Dipesan",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 15,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportBarangSedangPesan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportBarangSedangPesan.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBarangSedangPesan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangSedangPesan").getForm().url = "Report/SedangPesan";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportBarangSedangPesan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportBarangSedangPesan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangSedangPesan").getForm().url = "Report/SedangPesan";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportBarangSedangPesan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBarangHabis = Ext.extend(Ext.Window, {
    title: "Persediaan Habis",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 15,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportBarangHabis",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportBarangHabis.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBarangHabis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangHabis").getForm().url = "Report/PersediaanHabis";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportBarangHabis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportBarangHabis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangHabis").getForm().url = "Report/PersediaanHabis";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportBarangHabis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportUtang = Ext.extend(Ext.Window, {
    title: "Utang",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 15,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportUtang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportUtang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportUtang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportUtang").getForm().url = "Report/Utang";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportUtang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportUtang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportUtang").getForm().url = "Report/Utang";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportUtang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportPembelian = Ext.extend(Ext.Window, {
    title: "Pembelian",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 250,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    resizable: !1,
    initComponent: function () {
        /* if (jun.rztSupplier.getTotalCount() === 0) {
         jun.rztSupplier.load();
         }*/
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztUsersCmp.getTotalCount() === 0) {
            jun.rztUsersCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportPembelian",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    //{
                    //    xtype: "label",
                    //    text: 'Jenis Transaksi :'
                    //},
                    {
                        xtype: 'combo',
                        fieldLabel: 'Jenis Transaksi',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["P", "Pembelian"],
                                ["RP", "Retur Pembelian"]
                            ]
                        }),
                        hiddenName: 'jnstrans',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnstrans',
                        emptyText: "SEMUA",
                        hiddenValue: 'P',
                        value: 'P',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    /* {
                     xtype: 'combo',
                     typeAhead: true,
                     triggerAction: 'all',
                     lazyRender: true,
                     mode: 'local',
                     fieldLabel: 'Pemasok',
                     store: jun.rztSupplier,
                     hiddenName: 'supplier_id',
                     valueField: 'supplier_id',
                     forceSelection: true,
                     displayField: 'supplier_name',
                     ref: '../supplier_code',
                     listWidth: 350,
                     matchFieldWidth: !1,
                     allowBlank: true,
                     emptyText: "SEMUA",
                     anchor: '100%'
                     },*/
                    //{
                    //    xtype: "label",
                    //    text: 'Konsumen :'
                    //},
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        emptyText: "Semua",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Supplier',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        forceSelection: true,
                        displayField: 'nama',
                        allowBlank: true,
                        ref: '../orangcmp',
                        anchor: '100%'
                    },
                    //{
                    //    xtype: "label",
                    //    text: 'Jenis Laporan :'
                    //},
                    {
                        xtype: 'combo',
                        colspan: 2,
                        fieldLabel: 'Jenis Laporan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["D", "Detil"],
                                ["S", "Summary"]
                            ]
                        }),
                        hiddenName: 'jnslap',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnslap',
                        emptyText: "",
                        hiddenValue: 'S',
                        value: 'S',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        emptyText: "Semua",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kasir',
                        store: jun.rztUsersCmp,
                        hiddenName: 'id_user',
                        valueField: 'id_user',
                        forceSelection: true,
                        displayField: 'username',
                        ref: '../usercmp',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }//,
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportPembelian.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportPembelian").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPembelian").getForm().url = "Report/Pembelian";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportPembelian').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportPembelian").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPembelian").getForm().url = "Report/Pembelian";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportPembelian').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTerimaBarang = Ext.extend(Ext.Window, {
    title: "Penerimaan Barang",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportTerimaBarang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        ref: '../supplier',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        emptyText: 'All Supplier',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportTerimaBarang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTerimaBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarang").getForm().url = "Report/TerimaBarang";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportTerimaBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportTerimaBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarang").getForm().url = "Report/TerimaBarang";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportTerimaBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportMutasiKas = Ext.extend(Ext.Window, {
    title: "Mutasi Kas",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBank.getTotalCount() === 0) {
            jun.rztBank.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportMutasiKas",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Bank',
                        forceSelection: true,
                        store: jun.rztBank,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportMutasiKas.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportMutasiKas").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMutasiKas").getForm().url = "Report/MutasiKas";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportMutasiKas').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportMutasiKas").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMutasiKas").getForm().url = "Report/MutasiKas";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportMutasiKas').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportJurnalUmum = Ext.extend(Ext.Window, {
    title: "Jurnal Umum",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportJurnalUmum",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportJurnalUmum.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportJurnalUmum").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportJurnalUmum").getForm().url = "Report/JurnalUmum";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportJurnalUmum').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportJurnalUmum").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportJurnalUmum").getForm().url = "Report/JurnalUmum";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportJurnalUmum').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportKelolaStok = Ext.extend(Ext.Window, {
    title: "Pengelolaan Persediaan",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportKelolaStok",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportKelolaStok.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKelolaStok").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKelolaStok").getForm().url = "Report/KelolaStok";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportKelolaStok').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportKelolaStok").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKelolaStok").getForm().url = "Report/KelolaStok";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportKelolaStok').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLabaRugi = Ext.extend(Ext.Window, {
    title: "Laporan Laba Rugi",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = "Report/LabaRugi";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = "Report/LabaRugi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLabaKotor = Ext.extend(Ext.Window, {
    title: "Laporan Laba Kotor",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportLabaKotor",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "label",
                        text: 'Jenis Transaksi :'
                    },
                    {
                        xtype: 'combo',
                        colspan: 2,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "text"],
                            data: [
                                ["P", "Penjualan"],
                                ["RP", "Retur Penjualan"]
                            ]
                        }),
                        hiddenName: 'jnstrans',
                        valueField: 'id',
                        forceSelection: true,
                        displayField: 'text',
                        ref: '../jnstrans',
                        emptyText: "SEMUA",
                        hiddenValue: 'P',
                        value: 'P',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLabaKotor.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaKotor").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaKotor").getForm().url = "Report/LabaKotor";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaKotor').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLabaKotor").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaKotor").getForm().url = "Report/LabaKotor";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportLabaKotor').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSelisihStok = Ext.extend(Ext.Window, {
    title: "Selisih Persediaan",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportSelisihStok",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportSelisihStok.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this)
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSelisihStok").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSelisihStok").getForm().url = "Report/SelisihStok";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportSelisihStok').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSelisihStok").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSelisihStok").getForm().url = "Report/SelisihStok";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSelisihStok').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportGeneralLedger = Ext.extend(Ext.Window, {
    title: "General Ledger",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztChartMasterComp.getTotalCount() === 0) {
            jun.rztChartMasterComp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralLedger",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kode Rekening',
                        store: jun.rztChartMasterComp,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">', '<h3><span">{account_code} - {account_name}</span></h3><br />{description}', "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_name',
                        forceSelection: true,
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportGeneralLedger.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Mutasi Stok",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovements";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryMovements").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            downloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInventoryMovements").getForm().getValues());
        }
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Kartu Stok",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Barang',
                        xtype: 'combo',
                        colspan: 5,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                            '<span style="float:left;width: 15%;">{barcode}</span><span style="float:left;width: 75%;">{barang_name}</span><span>{sisa}</span>',
                            "</div></tpl>"),
                        name: "barang_id",
                        hiddenName: "barang_id",
                        listWidth: 750,
                        store: jun.rztBarang,
                        valueField: 'barang_id',
                        displayField: 'barang_name',
                        forceSelection: true,
                        hideTrigger: true,
                        ref: '../../barang',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.GenerateLabaRugi = Ext.extend(Ext.Window, {
    title: "Generate Profit Lost",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GenerateLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    new jun.comboBulan({
                        hiddenName: "month",
                        name: 'month',
                        fieldLabel: 'Month',
                        anchor: '100%'
                    }),
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Tahun",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Generate",
                    ref: "../btnSave"
                }
            ]
        };
        jun.GenerateLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    onbtnSaveclick: function () {
        Ext.getCmp('form-GenerateLabaRugi').getForm().submit({
            url: 'Report/GenerateLabaRugi',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.Neraca = Ext.extend(Ext.Window, {
    title: "Neraca",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Neraca",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Pada Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.Neraca.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //var dt = new Date();
        //this.year.setValue(dt.format('Y'));
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-Neraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-Neraca").getForm().url = "Report/Neraca";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-Neraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ClosingYear = Ext.extend(Ext.Window, {
    title: "Closing",
    iconCls: "silk13-report",
    modez: 1,
    width: 350,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ClosingYear",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'Tanggal Closing',
                        format: 'd M Y',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format",
                        value: 'excel'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Ready Closing",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ClosingYear.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //var dt = new Date();
    },
    onbtnSaveclick: function () {
        this.btnSave.setDisabled(true);
        Ext.getCmp("form-ClosingYear").getForm().standardSubmit = !0;
        Ext.getCmp("form-ClosingYear").getForm().url = "Site/Closing";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ClosingYear').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        this.btnSave.setDisabled(false);
        this.close();
        //this.btnSave.setDisabled(true);
        //var urlz = 'Site/Closing';
        //Ext.getCmp('form-ClosingYear').getForm().submit({
        //    url: urlz,
        //    timeOut: 180000,
        //    scope: this,
        //    success: function (f, a) {
        //        var response = Ext.decode(a.response.responseText);
        //        Ext.MessageBox.show({
        //            title: 'Info',
        //            msg: response.msg,
        //            buttons: Ext.MessageBox.OK,
        //            icon: Ext.MessageBox.INFO
        //        });
        //        this.btnSave.setDisabled(false);
        //        this.close();
        //
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //        this.btnSave.setDisabled(false);
        //    }
        //});
    }
});
jun.ReportKartuHutang = Ext.extend(Ext.Window, {
    title: "Kartu Hutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKartuHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Supplier',
                        typeAhead: true,
                        triggerAction: 'all',
                        id: 'supplierid',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        matchFieldWidth: !1,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKartuHutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuHutang").getForm().url = "Report/KartuHutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKartuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/KartuHutang";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuHutang").getForm().url = url;
        var form = Ext.getCmp('form-ReportKartuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportKartuPiutang = Ext.extend(Ext.Window, {
    title: "Kartu Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKartuPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Konsumen',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztKonsumenCmp,
                        minChars: 3,
                        hiddenName: 'konsumen_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="float:left;width:120;font-weight: bold">{kode}</span>' +
                            '<span style="font-weight: bold">{nama}</span>', "</div></tpl>"),
                        listWidth: 350,
                        hideTrigger: true,
                        ref: '../konsumen_retur',
                        allowBlank: true,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKartuPiutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKartuPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuPiutang").getForm().url = "Report/KartuPiutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKartuPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/KartuPiutang";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportKartuPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuPiutang").getForm().url = url;
        var form = Ext.getCmp('form-ReportKartuPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportCetakBarang = Ext.extend(Ext.Window, {
    title: "Cetak Barang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCetakBarang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Mulai Baris ke',
                        xtype: 'numericfield',
                        name: "from",
                        ref: '../from',
                        value: 0,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Jumlah Baris',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 1000,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportCetakBarang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCetakBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCetakBarang").getForm().url = "Report/CetakBarang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCetakBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CetakBarang";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportCetakBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCetakBarang").getForm().url = url;
        var form = Ext.getCmp('form-ReportCetakBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSupplier = Ext.extend(Ext.Window, {
    title: "Cetak Supplier",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSupplier",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Mulai Baris ke',
                        xtype: 'numericfield',
                        name: "from",
                        ref: '../from',
                        value: 0,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Jumlah Baris',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 1000,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportSupplier.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnSaveclick: function () {
        var url = "Report/CetakSupplier";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportSupplier").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSupplier").getForm().url = url;
        var form = Ext.getCmp('form-ReportSupplier').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});