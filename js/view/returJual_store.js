jun.ReturJualstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturJualstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturJualStoreId',
            url: 'ReturJual',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_id'},
                {name: 'doc_ref'},
                {name: 'salesman_id'},
                {name: 'konsumen_id'},
                {name: 'tempo'},
                {name: 'sub_total'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'no_bg_cek'},
                {name: 'sisa_tagihan'},
                {name: 'lunas'},
                {name: 'final'},
                {name: 'tgl'},
                {name: 'id_user'},
                {name: 'parent'},
                {name: 'bruto'},
                {name: 'disc'}
            ]
        }, cfg));
    }
});
jun.rztReturJual = new jun.ReturJualstore();
//jun.rztReturJual.load();
