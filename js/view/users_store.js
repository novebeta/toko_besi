jun.Usersstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Usersstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UsersStoreId',
            url: 'Users',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'id_user'},
                {name: 'username'},
                {name: 'password'},
                {name: 'last_login'},
                {name: 'role_id'},
                {name: 'last_ip'},
                {name: 'status_online'},
                {name: 'status'}
            ]
        }, cfg));
    }, FilterData: function() {
        this.filter([{property: "status", value: "1"}])
    }
});
jun.rztUsers = new jun.Usersstore();
jun.rztUsersLib = new jun.Usersstore();
jun.rztUsersCmp = new jun.Usersstore();
//jun.rztUsers.load();
