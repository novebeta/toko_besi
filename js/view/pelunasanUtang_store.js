jun.PelunasanUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanUtangStoreId',
            url: 'PelunasanUtang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_Utang_id'},
                {name: 'total'},
                {name: 'id_bank'},
                {name: 'no_bg_cek'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'supplier_id'},
                {name: 'tgl'},
                {name: 'kode'},
                {name: 'alamat'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztPelunasanUtang = new jun.PelunasanUtangstore();
//jun.rztPelunasanUtang.load();
