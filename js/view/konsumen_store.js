jun.Konsumenstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Konsumenstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KonsumenStoreId',
            url: 'Konsumen',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: "orang_id"},
                {name: "kode"},
                {name: "nama"},
                {name: "phone"},
                {name: "alamat"}
            ]
        }, cfg));
    }
});
jun.rztKonsumenMaster = new jun.Konsumenstore();
jun.rztKonsumenLib = new jun.Konsumenstore();
jun.rztKonsumenCmp = new jun.Konsumenstore();
//jun.rztKonsumen.load();
