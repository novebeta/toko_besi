jun.Barangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Barangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangStoreId',
            url: 'Barang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'barcode'},
                {name: 'barang_name'},
                {name: 'stok_minimal'},
                {name: 'status'},
                {name: 'supplier_id'},
                {name: 'kategori_barang_id'},
                {name: 'price_buy'},
                {name: 'price'},
                {name: 'disc_rp'},
                {name: 'disc'},
                {name: 'ppn'},
                {name: 'satuan'},
                {name: 'warna'},
                {name: 'merk'}
            ]
        }, cfg));
    }
});
jun.rztBarangLib = new jun.Barangstore();
jun.rztBarangMaster = new jun.Barangstore();
jun.BarangCmpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangCmpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangCmpStoreId',
            url: 'Barang/cmp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'barcode'},
                {name: 'barang_name'},
                {name: 'sisa', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'disc_rp', type: 'float'}
            ]
        }, cfg));
    }, FilterData: function () {
        this.filter([
            {property: "status", value: "1"}
        ])
    }
});
jun.rztBarang = new jun.BarangCmpstore();