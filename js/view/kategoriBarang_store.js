jun.KategoriBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KategoriBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KategoriBarangStoreId',
            url: 'KategoriBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kategori_barang_id'},
                {name: 'nama_kategori'},
            ]
        }, cfg));
    }
});
jun.rztKategoriBarang = new jun.KategoriBarangstore();
jun.rztKategoriBarangLib = new jun.KategoriBarangstore();
jun.rztKategoriBarangCmp = new jun.KategoriBarangstore();
