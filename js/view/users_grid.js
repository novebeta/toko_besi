jun.UsersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Manajemen User",
    id: "docs-jun.UsersGrid",
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "id",
            sortable: !0,
            resizable: !0,
            dataIndex: "id_user",
            hidden: !0
        },
        {
            header: "User Name",
            sortable: !0,
            resizable: !0,
            dataIndex: "username",
            width: 100
        },
        {
            header: "Terakhir Login",
            sortable: !0,
            resizable: !0,
            dataIndex: "last_login",
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztUsers, this.bbar = {
            items: [
                {
                    xtype: "paging",
                    store: this.store,
                    displayInfo: !0,
                    pageSize: 20
                }
            ]
        }, this.tbar = {
            xtype: "toolbar",
            items: [
                {
                    iconCls: "asp-user2_add",
                    xtype: "button",
                    text: "Tambah User",
                    ref: "../btnAdd"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    iconCls: "asp-access",
                    text: "Reset Password",
                    ref: "../btnEdit"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    iconCls: "asp-user2_delete",
                    text: "Ubah Security Role",
                    ref: "../btnSecurityRole"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    text: "Refresh",
                    ref: "../btnRefresh"
                }
            ]
        },
            jun.rztUsers.reload(), jun.UsersGrid.superclass.initComponent.call(this), this.btnAdd.on("Click",
            this.loadForm, this), this.btnEdit.on("Click", this.loadEditForm, this), this.btnSecurityRole.on("Click",
            this.deleteRec, this), this.btnRefresh.on("Click", this.refreshData,
            this), this.getSelectionModel().on("rowselect", this.getrow, this)
    },
    refreshData: function () {
        jun.rztUsers.reload()
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections()
    },
    loadForm: function () {
        var a = new jun.UsersWin({
            modez: 0
        });
        a.show()
    },
    loadEditForm: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin mereset password user ini?", this.deleteRecYes,
            this)
    },
    deleteRec: function () {
        var a = this.sm.getSelected();
        if (a == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih User");
            return
        }
        var b = new jun.UbahSecurity({
            modez: 0
        });
        b.show(), b.formz.getForm().loadRecord(this.record)
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih User");
            return
        }
        var c = jun.StringGenerator(8, "#aA"),
            d = jun.EncryptPass(c);
        Ext.Ajax.request({
            url: "Users/update/id/" + b.json.id,
            method: "POST",
            params: {
                password: d
            },
            scope: this,
            success: function (a, b) {
                jun.rztUsers.reload();
                var d = Ext.decode(a.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: d.msg + "<br>Password : " + c,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                })
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        })
    }
});