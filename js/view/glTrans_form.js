jun.GlTransDetilWin = Ext.extend(Ext.Window, {
    title: "Alokasi",
    modez: 1,
    width: 335,
    height: 165,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    debit: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-GlTransDetil",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kode Rekening',
                        store: jun.rztChartMasterNonKasBank,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        matchFieldWidth: !1,
                        forceSelection: true,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_name',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Jumlah",
                        hideLabel: !1,
                        name: "amount",
                        id: "amountid",
                        ref: "../amount",
                        maxLength: 30,
                        value: 0,
                        anchor: "100%"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Simpan & Tutup",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        }, jun.GlTransDetilWin.superclass.initComponent.call(this),
            this.on("activate", this.onActivate, this), this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this),
            this.btnSave.on("click", this.onbtnSaveclick, this), this.btnCancel.on("click", this.onbtnCancelclick,
            this),
            this.cmbkode.on("focus", this.onLoadChartMaster, this), this.on("close", this.onWinClose, this);
    },
    onLoadChartMaster: function () {
        jun.rztChartMasterNonKasBank.FilterData();
    },
    onWinClose: function () {
        jun.rztChartMasterNonKasBank.clearFilter();
    },
    onActivate: function () {
        this.btnSave.hidden = !1;
    },
    saveForm: function () {
        if (parseFloat(this.amount.value) === 0) {
            Ext.MessageBox.alert("Error", "Jumlah tidak boleh 0");
            return;
        }
        var a = jun.rztGlTrans.find("account", this.cmbkode.value);
        if (a > -1 && this.modez == 0) {
            Ext.MessageBox.show({
                title: "Error",
                msg: "Kode rekening sudah di dipakai!",
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }
        var b = jun.rztGlTrans.recordType, c = new b({
            account: this.cmbkode.value,
            debit: this.debit ? parseFloat(this.amount.value) : 0,
            kredit: this.debit ? 0 : parseFloat(this.amount.value)
        });
        jun.rztGlTrans.insert(jun.rztGlTrans.getCount(), c), jun.rztGlTrans.refreshData(),
            Ext.getCmp("form-GlTransDetil").getForm().reset(), this.closeForm && this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.GlTransWin = Ext.extend(Ext.Window, {
    title: "Jurnal Umum",
    modez: 1,
    width: 800,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-GlTrans",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "xdatefield",
                        ref: "../tran_date",
                        fieldLabel: "Tanggal Transaksi",
                        name: "tran_date",
                        id: "tran_dateid",
                        format: "d M Y"
                    },
                    {
                        xtype: "textfield",
                        ref: "../memo_",
                        fieldLabel: "Keterangan",
                        name: "memo_",
                        id: "memo_id",
                        anchor: '100%'
                    },
                    new jun.GlTransGrid({
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "."
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Total Debit",
                        hideLabel: !1,
                        name: "tot_debit",
                        id: "tot_debit_id",
                        readOnly: !0,
                        ref: "../TotDebit"
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Total Kredit",
                        hideLabel: !1,
                        name: "tot_kredit",
                        id: "tot_kredit_id",
                        readOnly: !0,
                        ref: "../TotKredit"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Simpan & Tutup",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.rztGlTrans.removeAll();
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        jun.GlTransWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    onActivate: function () {
        jun.rztGlTrans.refreshData();
        if(this.modez == 0){
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }else{
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        }
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a), this.btnSaveClose.setDisabled(a);
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.TotDebit.value != this.TotKredit.value) {
            Ext.MessageBox.alert("Error", "Total Debit dan Total Kredit harus sama."), this.btnDisabled(!1);
            return;
        }
        if (parseFloat(this.TotDebit.value) === 0 || parseFloat(this.TotKredit.value) === 0) {
            Ext.MessageBox.alert("Error", "Total Debit atau Total Kredit tidak boleh nol."),
                this.btnDisabled(!1);
            return;
        }
        Ext.getCmp("form-GlTrans").getForm().submit({
            url: "GlTrans/CreateJurnalUmum",
            params: {
                detil: Ext.encode(Ext.pluck(jun.rztGlTrans.data.items, "data"))
            },
            timeOut: 1e3,
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                this.btnDisabled(!1);
                if (c.success == 0) {
                    Ext.MessageBox.show({
                        title: "Jurnal Umum",
                        msg: "Jurnal umum gagal disimpan.<br /> Alasan :" + c.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                Ext.MessageBox.show({
                    title: "Jurnal Umum",
                    msg: c.msg + "<br /> Ref. Dokumen : " + c.id,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp("form-GlTrans").getForm().reset();
                jun.rztGlTrans.removeAll();
                jun.rztJurnalUmum.reload();
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(!1);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SaldoAwalWin = Ext.extend(Ext.Window, {
    title: "Saldo Awal Rekening",
    modez: 1,
    width: 350,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-SaldoAwal",
                labelWidth: 125,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "xdatefield",
                        ref: "../trans_date",
                        fieldLabel: "Tanggal Transaksi",
                        name: "trans_date",
                        id: "trans_dateid",
                        format: "d M Y",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kode Rekening',
                        store: jun.rztChartMasterNonKasBank,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        matchFieldWidth: !1,
                        forceSelection: true,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_name',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Jumlah",
                        hideLabel: !1,
                        name: "amount",
                        id: "amountid",
                        ref: "../amount",
                        maxLength: 30,
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
//        jun.rztChartMaster.reload();
        jun.SaldoAwalWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a);
    },
    onActivate: function () {
        this.btnSave.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0), Ext.getCmp("form-SaldoAwal").getForm().submit({
            url: "GlTrans/SetSaldoAwal/",
            timeOut: 1e3,
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                }), Ext.getCmp("form-SaldoAwal").getForm().reset(), this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SaldoAwalBarangWin = Ext.extend(Ext.Window, {
    title: "Saldo Awal Barang",
    modez: 1,
    width: 350,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-SaldoAwal",
                labelWidth: 125,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "xdatefield",
                        ref: "../trans_date",
                        fieldLabel: "Tanggal Transaksi",
                        name: "trans_date",
                        id: "trans_dateid",
                        format: "d M Y",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: "Barang",
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                            '<span style="float:left;width: 25%;">{barcode}</span><span>{barang_name}</span>',
                            "</div></tpl>"),
                        listWidth: 550,
                        store: jun.rztBarang,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'barang_name',
                        forceSelection: true,
                        hideTrigger: true,
                        ref: '../barang',
                        anchor: "100%"
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Jumlah",
                        hideLabel: !1,
                        name: "amount",
                        id: "amountid",
                        ref: "../amount",
                        maxLength: 30,
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
//        if (jun.rztChartMasterLib.getTotalCount() === 0) {
//            jun.rztChartMasterLib.load();
//        }
//        jun.rztChartMaster.reload();
        jun.SaldoAwalBarangWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a);
    },
    onActivate: function () {
        this.btnSave.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0), Ext.getCmp("form-SaldoAwal").getForm().submit({
            url: "StockMoves/SetSaldoAwal/",
            timeOut: 1e3,
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                }), Ext.getCmp("form-SaldoAwal").getForm().reset(), this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
