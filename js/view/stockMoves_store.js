jun.StockMovesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StockMovesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StockMovesStoreId',
            url: 'StockMoves',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'trans_id'},
                {name: 'trans_no'},
                {name: 'type'},
                {name: 'tran_date'},
                {name: 'price'},
                {name: 'reference'},
                {name: 'qty'},
                {name: 'discount_percent'},
                {name: 'standard_cost'},
                {name: 'visible'},
                {name: 'gudang_id'},
                {name: 'barang_id'}
            ]
        }, cfg));
    }
});
jun.KelolaStokStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KelolaStokStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KelolaStokStoreId',
            url: 'StockMoves/kelolaStok',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'trans_no'},
                {name: 'tran_date'},
                {name: 'reference'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.KelolaStokDetilStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KelolaStokDetilStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KelolaStokDetilStoreId',
            url: 'StockMoves/kelolaStokDetil',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'qty'}
            ]
        }, cfg));
    }
});
jun.SelisihStokStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SelisihStokStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SelisihStokStoreId',
            url: 'StockMoves/SelisihStok',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'trans_no'},
                {name: 'tran_date'},
                {name: 'reference'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.SelisihStokDetilStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SelisihStokDetilStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SelisihStokDetilStoreId',
            url: 'StockMoves/SelisihStokDetil',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'qty'}
            ]
        }, cfg));
    }
});
jun.rztStockMoves = new jun.StockMovesstore();
jun.rztKelolaStok = new jun.KelolaStokStore();
jun.rztKelolaStokDetil = new jun.KelolaStokDetilStore();
jun.rztSelisihStok = new jun.SelisihStokStore({baseParams: {mode: 'grid'}});
jun.rztSelisihStokDetil = new jun.SelisihStokDetilStore();
