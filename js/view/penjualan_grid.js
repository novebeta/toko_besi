jun.PenjualanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penjualan Tunai",
    id: 'docs-jun.PenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            xtype: 'checkcolumn',
            header: 'Bayar',
            dataIndex: 'bayar',
            width: 55
        }
    ],
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKonsumenLib.getTotalCount() === 0) {
            jun.rztKonsumenLib.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.rztPenjualan.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.arus = 1;
                    b.params.mode = 'grid';
                    b.params.status_kirim = Ext.getCmp('statuspenjualantunaigridid').getValue();
                }
            }
        });
        this.store = jun.rztPenjualan;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Faktur',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Faktur',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    text: 'Status\xA0'
                },
                new jun.comboStatusPenjualan({
                    id: 'statuspenjualantunaigridid',
                    ref: '../status'
                }),
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'DO',
                    ref: '../btnDO'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Bayar',
                    ref: '../btnBayar'
                }
            ]
        };
        jun.PenjualanGrid.superclass.initComponent.call(this);
        jun.rztPenjualan.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        this.status.on('select', this.refreshTgl, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.btnDO.on('Click', this.printDO, this);
        this.btnBayar.on('Click', this.btnBayarClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        //this.store.on('update',this.onClick,this);
    },
    onClick: function () {
        //jun.rztPenjualan.commitChanges();
    },
    btnBayarClick: function () {
        var form = new jun.BayarWin();
        form.show();
    },
    printSales: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('Penjualan/Print', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    printDO: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('Penjualan/Do', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        //if (jun.rztDof.totalLength > 0) {
        //    Ext.MessageBox.show({
        //        title: 'Error',
        //        msg: "Tidak bisa membuat faktur baru karena DO Final sudah dibuat.",
        //        buttons: Ext.MessageBox.OK,
        //        icon: Ext.MessageBox.ERROR
        //    });
        //    return;
        //}
        //var tgl = this.tgl.getValue();
        //var sales = this.sales.getValue();
        //if (tgl == "" || sales == "") {
        //    Ext.MessageBox.alert("Warning",
        //        "Anda belum memilih Tanggal atau Sales");
        //    return;
        //}
        var form = new jun.PenjualanWin({
            modez: 0
        });
        form.show();
        //form.tgl.setValue(tgl);
        //form.sales.setValue(sales);
        //form.btnSave.setVisible(true);
        //form.btnFinal.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning",
                "Anda belum memilih faktur");
            return;
        }
        var idz = selectedz.json.penjualan_id;
        var form = new jun.PenjualanWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //if (this.record.data.final == "1") {
        //    form.btnSave.setVisible(false);
        //    //form.btnFinal.setVisible(false);
        //} else {
        //    form.btnSave.setVisible(true);
        //    //form.btnFinal.setVisible(true);
        //}
        //form.onKonsumenChange();
        jun.rztDetilPenjualan.baseParams = {
            penjualan_id: idz
        };
        jun.rztDetilPenjualan.load();
        jun.rztDetilPenjualan.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Penjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPenjualan.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
jun.PenjualanKreditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penjualan Kredit",
    id: 'docs-jun.PenjualanKreditGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKonsumenLib.getTotalCount() === 0) {
            jun.rztKonsumenLib.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.rztPenjualanKredit.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.arus = 2;
                    b.params.mode = 'grid';
                    b.params.status_kirim = Ext.getCmp('statuspenjualankreditgridid').getValue();
                }
            }
        });
        this.store = jun.rztPenjualanKredit;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Faktur',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Faktur',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    //style: 'margin:5px',
                    text: 'Status\xA0'
                },
                new jun.comboStatusPenjualan({
                    id: 'statuspenjualankreditgridid',
                    ref: '../status'
                }),
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'DO',
                    ref: '../btnDO'
                }
            ]
        };
        jun.PenjualanKreditGrid.superclass.initComponent.call(this);
        jun.rztPenjualanKredit.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        this.status.on('select', this.refreshTgl, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.btnDO.on('Click', this.printDO, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    printSales: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('Penjualan/Print', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
        //Ext.Ajax.request({
        //    url: 'Penjualan/Print',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        salestrans_id: record.json.salestrans_id
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        //findPrinterReceipt();
        //        if (notReady()) {
        //            return;
        //        }
        //        printHTML('EPSON_LX-310', response.msg);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    refreshTgl: function () {
        this.store.reload();
    },
    printDO: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('Penjualan/Do', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        //if (jun.rztDof.totalLength > 0) {
        //    Ext.MessageBox.show({
        //        title: 'Error',
        //        msg: "Tidak bisa membuat faktur baru karena DO Final sudah dibuat.",
        //        buttons: Ext.MessageBox.OK,
        //        icon: Ext.MessageBox.ERROR
        //    });
        //    return;
        //}
        //var tgl = this.tgl.getValue();
        //var sales = this.sales.getValue();
        //if (tgl == "" || sales == "") {
        //    Ext.MessageBox.alert("Warning",
        //        "Anda belum memilih Tanggal atau Sales");
        //    return;
        //}
        var form = new jun.PenjualanKreditWin({
            modez: 0
        });
        form.show();
        //form.tgl.setValue(tgl);
        //form.sales.setValue(sales);
        //form.btnSave.setVisible(true);
        //form.btnFinal.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning",
                "Anda belum memilih faktur");
            return;
        }
        var idz = selectedz.json.penjualan_id;
        var form = new jun.PenjualanKreditWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //if (this.record.data.final == "1") {
        //    form.btnSave.setVisible(false);
        //    //form.btnFinal.setVisible(false);
        //} else {
        //    form.btnSave.setVisible(true);
        //    //form.btnFinal.setVisible(true);
        //}
        //form.onKonsumenChange();
        jun.rztDetilPenjualan.baseParams = {
            penjualan_id: idz
        };
        jun.rztDetilPenjualan.load();
        jun.rztDetilPenjualan.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Penjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPenjualanKredit.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
jun.ReturPenjualanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Retur Penjualan Tunai",
    id: 'docs-jun.ReturPenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }//,
        //{
        //    xtype: 'checkcolumn',
        //    header: 'Bayar',
        //    dataIndex: 'bayar',
        //    width: 55
        //}
    ],
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKonsumenLib.getTotalCount() === 0) {
            jun.rztKonsumenLib.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.rztReturPenjualan.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.arus = 1;
                    b.params.mode = 'grid';
                    //b.params.status_kirim = Ext.getCmp('statusReturPenjualantunaigridid').getValue();
                }
            }
        });
        this.store = jun.rztReturPenjualan;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Faktur',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Faktur',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                //{
                //    xtype: 'label',
                //    text: 'Status\xA0'
                //},
                //new jun.comboStatusPenjualan({
                //    id: 'statusReturPenjualantunaigridid',
                //    ref: '../status'
                //}),
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint'
                }//,
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'DO',
                //    ref: '../btnDO'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Bayar',
                //    ref: '../btnBayar'
                //}
            ]
        };
        jun.ReturPenjualanGrid.superclass.initComponent.call(this);
        jun.rztReturPenjualan.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        //this.status.on('select', this.refreshTgl, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        //this.btnDO.on('Click', this.printDO, this);
        //this.btnBayar.on('Click', this.btnBayarClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        //this.store.on('update',this.onClick,this);
        this.store.load();
    },
    onClick: function () {
        //jun.rztReturPenjualan.commitChanges();
    },
    btnBayarClick: function () {
        var form = new jun.BayarWin();
        form.show();
    },
    printSales: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('ReturJual/Print', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    printDO: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('ReturJual/Do', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        //if (jun.rztDof.totalLength > 0) {
        //    Ext.MessageBox.show({
        //        title: 'Error',
        //        msg: "Tidak bisa membuat faktur baru karena DO Final sudah dibuat.",
        //        buttons: Ext.MessageBox.OK,
        //        icon: Ext.MessageBox.ERROR
        //    });
        //    return;
        //}
        //var tgl = this.tgl.getValue();
        //var sales = this.sales.getValue();
        //if (tgl == "" || sales == "") {
        //    Ext.MessageBox.alert("Warning",
        //        "Anda belum memilih Tanggal atau Sales");
        //    return;
        //}
        var form = new jun.ReturPenjualanWin({
            modez: 0
        });
        form.show();
        //form.tgl.setValue(tgl);
        //form.sales.setValue(sales);
        //form.btnSave.setVisible(true);
        //form.btnFinal.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning",
                "Anda belum memilih faktur");
            return;
        }
        var idz = selectedz.json.penjualan_id;
        var form = new jun.ReturPenjualanWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //if (this.record.data.final == "1") {
        //    form.btnSave.setVisible(false);
        //    //form.btnFinal.setVisible(false);
        //} else {
        //    form.btnSave.setVisible(true);
        //    //form.btnFinal.setVisible(true);
        //}
        //form.onKonsumenChange();
        jun.rztDetilPenjualan.baseParams = {
            penjualan_id: idz
        };
        jun.rztDetilPenjualan.load();
        jun.rztDetilPenjualan.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturPenjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturPenjualan.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
jun.ReturPenjualanKreditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Retur Penjualan Kredit",
    id: 'docs-jun.ReturPenjualanKreditGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKonsumenLib.getTotalCount() === 0) {
            jun.rztKonsumenLib.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.rztReturPenjualanKredit.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.arus = 2;
                    b.params.mode = 'grid';
                    //b.params.status_kirim = Ext.getCmp('statusReturPenjualankreditgridid').getValue();
                }
            }
        });
        this.store = jun.rztReturPenjualanKredit;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Faktur',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Faktur',
                    ref: '../btnEdit'
                },
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'label',
                //    //style: 'margin:5px',
                //    text: 'Status\xA0'
                //},
                //new jun.comboStatusPenjualan({
                //    id: 'statusReturPenjualankreditgridid',
                //    ref: '../status'
                //}),
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint'
                }//,
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'DO',
                //    ref: '../btnDO'
                //}
            ]
        };
        jun.ReturPenjualanKreditGrid.superclass.initComponent.call(this);
        jun.rztReturPenjualanKredit.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        //this.status.on('select', this.refreshTgl, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        //this.btnDO.on('Click', this.printDO, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.load();
    },
    printSales: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('ReturJual/Print', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
        //Ext.Ajax.request({
        //    url: 'ReturPenjualan/Print',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        salestrans_id: record.json.salestrans_id
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        //findPrinterReceipt();
        //        if (notReady()) {
        //            return;
        //        }
        //        printHTML('EPSON_LX-310', response.msg);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    refreshTgl: function () {
        this.store.reload();
    },
    printDO: function () {
        var record = this.sm.getSelected();
        //// Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        downloadPrintFileRaw('ReturJual/Do', 'POST', '64', {'penjualan_id': record.json.penjualan_id}, 'default');
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        //if (jun.rztDof.totalLength > 0) {
        //    Ext.MessageBox.show({
        //        title: 'Error',
        //        msg: "Tidak bisa membuat faktur baru karena DO Final sudah dibuat.",
        //        buttons: Ext.MessageBox.OK,
        //        icon: Ext.MessageBox.ERROR
        //    });
        //    return;
        //}
        //var tgl = this.tgl.getValue();
        //var sales = this.sales.getValue();
        //if (tgl == "" || sales == "") {
        //    Ext.MessageBox.alert("Warning",
        //        "Anda belum memilih Tanggal atau Sales");
        //    return;
        //}
        var form = new jun.ReturPenjualanKreditWin({
            modez: 0
        });
        form.show();
        //form.tgl.setValue(tgl);
        //form.sales.setValue(sales);
        //form.btnSave.setVisible(true);
        //form.btnFinal.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning",
                "Anda belum memilih faktur");
            return;
        }
        var idz = selectedz.json.penjualan_id;
        var form = new jun.ReturPenjualanKreditWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //if (this.record.data.final == "1") {
        //    form.btnSave.setVisible(false);
        //    //form.btnFinal.setVisible(false);
        //} else {
        //    form.btnSave.setVisible(true);
        //    //form.btnFinal.setVisible(true);
        //}
        //form.onKonsumenChange();
        jun.rztDetilPenjualan.baseParams = {
            penjualan_id: idz
        };
        jun.rztDetilPenjualan.load();
        jun.rztDetilPenjualan.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturPenjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturPenjualanKredit.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
