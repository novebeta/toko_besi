jun.ReturJualDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ReturJualDetil",
    id: 'docs-jun.ReturJualDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    columns: [
        {
            header: 'Barcode',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'Sat',
            resizable: true,
            dataIndex: 'sat',
            width: 100
        },
        {
            header: 'Jml',
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Harga',
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc 1',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc 2',
            sortable: true,
            resizable: true,
            dataIndex: 'disc2',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Pot',
            sortable: true,
            resizable: true,
            dataIndex: 'pot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztReturJualDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
//                    title: 'Clipboard',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
//                            xtype: 'label',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            colspan: 5,
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                                '<span style="float:left;width: 15%;">{barcode}</span><span style="float:left;width: 75%;">{barang_name}</span><span>{sisa}</span>',
                                "</div></tpl>"),
                            listWidth: 750,
                            store: jun.rztBarang,
                            valueField: 'barang_id',
                            displayField: 'barang_name',
                            forceSelection: true,
                            hideTrigger: true,
                            ref: '../../barang',
                            width: 258
                        },
                        {
//                            xtype: 'label',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'hrgid',
                            ref: '../../hrg',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
//                            xtype: 'label',
                            text: '\xA0Satuan :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            editable: false,
                            mode: 'local',
                            store: jun.Sat,
                            forceSelection: true,
                            allowBlank: false,
                            valueField: 'val',
                            displayField: 'name',
                            ref: '../../sat',
                            width: 55
                        },
                        {
//                            xtype: 'label',
                            text: '\xA0Jumlah :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'jmlid',
                            ref: '../../jml',
                            width: 50,
                            value: 1,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
//                    title: 'Clipboard',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
//                            xtype: 'label',
                            text: 'Disk 1 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'disc1id',
                            ref: '../../disc1',
                            width: 45,
                            minValue: 0,
                            value: 0,
                            maxValue: 100
                        },
                        {
//                            xtype: 'label',
                            text: '\xA0Disc 2 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'disc2id',
                            ref: '../../disc2',
                            width: 45,
                            value: 0,
                            minValue: 0,
                            maxValue: 100
                        },
                        {
                            text: 'Potongan :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'potid',
                            ref: '../../pot',
                            value: 0,
                            colspan: 3,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
//                    title: 'Clipboard',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Tambah',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Hapus',
                            height: 44,
                            ref: '../   ../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ReturJualDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('change', this.onChangeBarang, this);
        this.sat.on('change', this.onChangeSat, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        this.hrg.setValue(barang.data.price_pcs);
        this.sat.reset();
        this.sat.store.removeAll(true);
        var data = [
            ["PCS", "PCS"],
            ["LSN", "LSN"],
            ["PAK", "PAK"],
            [barang.data.sat_max, barang.data.sat_max]
        ];
        this.sat.store.loadData(data, false);
    },
    onChangeSat: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        var sat = this.sat.getValue();
        if (sat == "") {
            return
        }
        switch (sat) {
            case "PCS" :
                this.hrg.setValue(barang.data.price_pcs);
                break;
            case "LSN" :
                this.hrg.setValue(barang.data.price_lsn);
                break;
            case "PAK" :
                this.hrg.setValue(barang.data.price_pak);
                break;
            default :
                this.hrg.setValue(barang.data.price_max);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//        kunci barang sama tidak boleh masuk dua kali
//        var a = jun.rztReturJualDetil.findExact("barang_id", barang_id);
//        if (a > -1) {
//            Ext.MessageBox.alert("Error",
//                "Item barang sudah dimansukkan.");
//            return
//        }
        var sat = this.sat.getValue();
        if (sat == "") {
            Ext.MessageBox.alert("Error", "Satuan harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        var pcs;
        var price = parseFloat(this.hrg.getValue());
        var jml = parseFloat(this.jml.getValue());
        var disc1 = parseFloat(this.disc1.getValue());
        var disc2 = parseFloat(this.disc2.getValue());
        var pot = parseInt(this.pot.getValue());
        var bruto = Math.round(price * jml);
//        var totdisc1 = bruto * (disc1 / 100);
//        var totdisc2 = bruto * (disc2 / 100);
//        var totalpot = (totdisc1 + totdisc2) + pot;
        var nominal = jun.calculateDisc(jml, price, disc1,disc2, pot);
        var totalpot = bruto - nominal;

        switch (sat) {
            case "PCS" :
                pcs = jml;
                break;
            case "LSN" :
                pcs = Math.round(jml * 12);
                break;
            case "PAK" :
                pcs = Math.round(jml * parseFloat(barang.data.pak_2_pcs));
                break;
            default :
                pcs = Math.round(jml * parseFloat(barang.data.max_2_pcs));
        }
        var hpp = Math.round(pcs * barang.data.price_buy);
        var c = this.store.recordType, d = new c({
            barang_id: barang_id,
            sat: sat,
            jml: jml,
            price: price,
            disc1: disc1,
            disc2: disc2,
            pot: pot,
            totalpot: totalpot,
            nominal: nominal,
            pcs: pcs,
            bruto: bruto,
            hpp: hpp
        });
        this.store.add(d);
        this.store.refreshData();
        this.barang.reset();
        this.sat.reset();
        this.jml.reset();
        this.hrg.reset();
        this.disc1.reset();
        this.disc2.reset();
        this.pot.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record), this.store.refreshData()
    }
})
