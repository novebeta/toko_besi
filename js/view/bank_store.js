jun.Bankstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Bankstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankStoreId',
            url: 'Bank',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id_bank'},
{name:'nama_akun'},
{name:'no_rekening'},
{name:'nama_bank'},
{name:'alamat_bank'},
{name:'no_tlp'},
{name:'account_code'},
{name:'status'}
            ]
        }, cfg));
    }, FilterData: function () {
        this.filter([
            {property: "status", value: "1"}
        ])
    }
});
jun.rztBank = new jun.Bankstore();
jun.rztBankCmp = new jun.Bankstore();
jun.rztBankLib = new jun.Bankstore();
//jun.rztBank.load();
