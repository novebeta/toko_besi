jun.PelunasanUtangDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PelunasanUtangDetil",
    id: 'docs-jun.PelunasanUtangDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Tanggal Faktur',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No Faktur Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur_beli',
            width: 100
        },
        {
            header: 'Nilai Faktur',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Sisa Tagihan',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Kas Dibayar',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_dibayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPelunasanUtangDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'No Faktur :\xA0'
                },
                {
                    xtype: "combo",
                    typeAhead: !0,
                    triggerAction: "all",
                    lazyRender: !0,
                    mode: "local",
                    store: jun.rztFakturUtang,
                    forceSelection: !0,
                    valueField: "pembelian_id",
                    matchFieldWidth: !1,
                    itemSelector: "div.search-item-table",
                    tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                        '<div class="cell4" style="width: 150px;">{no_faktur_beli}</div>',
                        '<div class="cell4" style="width: 100px;">{tgl:date("M j, Y")}</div>',
                        '<div class="cell4" style="width: 50px;text-align: right;">{total:number("0,0")}</div>',
                        '<div class="cell4" style="width: 50px;text-align: right;">{sisa:number("0,0")}</div>',
                        "</div></tpl>", '</div>'),
                    displayField: "no_faktur_beli",
                    listWidth: 350,
                    editable: !0,
                    ref: "../jual",
                    lastQuery: ""
                },
                {
                    xtype: 'label',
                    text: '\xA0Kas Dibayar :\xA0'
                },
                {
                    xtype: 'numericfield',
                    ref: '../kas',
                    value: 0,
                    width: 100
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDel'
                }
            ]
        };
        jun.PelunasanUtangDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', function (c, r, i) {this.selectedRecord = r;}, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onStoreChange: function () {
        jun.rztPelunasanUtangDetil.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PelunasanUtangDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        //var jual = this.jual.getValue();
        if (this.selectedRecord == null) {
            Ext.MessageBox.alert("Error", "Faktur harus dipilih.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
//        if (kas <= 0) {
//            Ext.MessageBox.alert("Error", "Kas harus lebih dari 0.");
//            return
//        }
//        var faktur_id = jun.rztFakturUtang.findExact('row', jual);
//        if (faktur_id == -1) {
//            Ext.MessageBox.alert("Error", "Kesalahan tak terduga.");
//            return
//        }
//        var faktur = jun.rztFakturUtang.getAt(faktur_id);
        var a = this.store.findExact("no_faktur_beli", this.selectedRecord.data.no_faktur_beli);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Faktur sudah dimansukkan.");
            return;
        }
        if (kas > 0 && kas > this.selectedRecord.data.sisa) {
            Ext.MessageBox.alert("Error", "Kas yang diterima tidak boleh lebih dari sisa tagihan.");
            return;
        } else if (kas < 0 && kas < this.selectedRecord.data.sisa) {
            Ext.MessageBox.alert("Error", "Kas yang diterima tidak boleh kurang dari debit Utang.");
            return;
        }
        var c = jun.rztPelunasanUtangDetil.recordType,
            d = new c({
                pembelian_id: this.selectedRecord.data.pembelian_id,
                kas_dibayar: kas,
                tgl: this.selectedRecord.data.tgl,
                no_faktur_beli: this.selectedRecord.data.no_faktur_beli,
                total: this.selectedRecord.data.total,
                sisa: this.selectedRecord.data.sisa
            });
        jun.rztPelunasanUtangDetil.add(d);
        this.selectedRecord = null;
        this.jual.reset();
        this.kas.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
jun.UtangPerSalesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "UtangPerSales",
    id: 'docs-jun.UtangPerSalesGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No Faktur',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur',
            width: 100
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'konsumen_code',
            width: 100
        },
        {
            header: 'Nama Konsumen',
            sortable: true,
            resizable: true,
            dataIndex: 'konsumen_name',
            width: 100
        },
        {
            header: 'Nilai Faktur',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Sisa Tagihan',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            xtype: 'checkcolumn',
            header: 'Cetak?',
            dataIndex: 'cetak',
            width: 55
        }
    ],
    initComponent: function () {
        this.store = jun.rztUtangPerSales;
        jun.UtangPerSalesGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('mousedown', this.onClick, this);
    },
    onClick: function () {
        this.store.commitChanges();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
//jun.PelunasanUtangDetilGridReturJual = Ext.extend(Ext.grid.GridPanel, {
//    id: 'docs-jun.PelunasanUtangDetilGridReturJual',
//    total: 0,
//    iconCls: "silk-grid",
//    viewConfig: {
//        forceFit: true
//    },
//    sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
//    columns: [
//        {
//            header: 'Tanggal Faktur',
//            resizable: true,
//            dataIndex: 'tgl',
//            width: 100
//        },
//        {
//            header: 'No Faktur',
//            resizable: true,
//            dataIndex: 'no_faktur',
//            width: 100
//        },
//        {
//            header: 'Nilai Faktur',
//            resizable: true,
//            dataIndex: 'nilai',
//            width: 100,
//            align: "right",
//            renderer: Ext.util.Format.numberRenderer("0,0")
//        },
//        {
//            header: 'Sisa Tagihan',
//            resizable: true,
//            dataIndex: 'sisa',
//            width: 100,
//            align: "right",
//            renderer: Ext.util.Format.numberRenderer("0,0")
//        }
//    ],
//    initComponent: function () {
//        this.store = jun.rztFakturUtangReturJual;
//        jun.PelunasanUtangDetilGridReturJual.superclass.initComponent.call(this);
//        this.getSelectionModel().on('rowselect', this.getrow, this);
//    },
//    getrow: function (sm, idx, r) {
//        this.record = r;
//        var s = this.sm.getSelections();
//        var total = 0;
//        for (var i = 0; i < s.length; i++) {
//            total += s[i].data.sisa;
//        }
//        Ext.getCmp('total_kreditUtangid').setValue(total);
//        var Utang = parseInt(Ext.getCmp('total_Utangid').getValue());
//        Ext.getCmp('total_alokasiid').setValue(total - Utang);
//    }
//})
//jun.FakturUtangReturJualWin = Ext.extend(Ext.Window, {
//    title: 'Alokasi Kredit Utang',
//    modal: !0,
//    padding: 5,
//    closeForm: !1,
//    layout: 'absolute',
//    width: 500,
//    height: 450,
//    iswin: !0,
//    btn: 0,
//    initComponent: function () {
//        this.items = [new jun.PelunasanUtangDetilGridReturJual({
//            x: 5,
//            y: 5,
//            height: 285,
//            frameHeader: !1,
//            header: !1,
//            total: this.Utang
//        }),
//            {
//                xtype: "label",
//                text: "Utang",
//                x: 215,
//                y: 295
//            },
//            {
//                xtype: 'numericfield',
//                id: 'total_kreditUtangid',
//                maxLength: 30,
//                readOnly: true,
//                value: 0,
//                x: 310,
//                y: 292
//            },
//            {
//                xtype: "label",
//                text: "Kredit Utang",
//                x: 215,
//                y: 325
//            },
//            {
//                xtype: 'numericfield',
//                id: 'total_Utangid',
//                ref: 'Utang',
//                maxLength: 30,
//                readOnly: true,
//                value: 0,
//                x: 310,
//                y: 322
//            },
//            {
//                xtype: "label",
//                text: "Sisa Alokasi",
//                x: 215,
//                y: 355
//            },
//            {
//                xtype: 'numericfield',
//                id: 'total_alokasiid',
//                maxLength: 30,
//                readOnly: true,
//                ref: 'alokasi',
//                value: 0,
//                x: 310,
//                y: 352
//            }
//        ];
//        this.fbar = {
//            xtype: 'toolbar',
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Simpan & Tutup',
//                    ref: '../btnSaveClose'
//                },
//                {
//                    xtype: 'button',
//                    text: 'Batal',
//                    ref: '../btnCancel'
//                }
//            ]
//        };
//        jun.FakturUtangReturJualWin.superclass.initComponent.call(this);
//        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//        this.btnCancel.on('click', this.onbtnCancelclick, this);
//    },
//    onbtnSaveCloseClick: function () {
//        var alokasi = parseInt(this.alokasi.getValue());
//        if (alokasi < 0) {
//            Ext.MessageBox.show({
//                title: 'Error',
//                msg: "Kredit Utang harus lebih besar atau sama dengan Utang.<br>" +
//                    "NOTE : Ctrl + Click untuk memilih lebih dari satu faktur.",
//                buttons: Ext.MessageBox.OK,
//                icon: Ext.MessageBox.ERROR
//            });
//            return;
//        }
//        this.sm = this.items.items[0].sm.getSelections();
//        this.btn = 1;
//        this.close();
//    },
//    onbtnCancelclick: function () {
//        this.btn = 0;
//        this.close();
//    }
//});
