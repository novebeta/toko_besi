jun.BankWin = Ext.extend(Ext.Window, {
    title: 'Kas dan Bank',
    modez: 1,
    width: 400,
    height: 270,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Bank',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kode Rekening',
                        store: jun.rztChartMasterComp,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        matchFieldWidth: !1,
                        forceSelection: true,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_name',
                        ref: '../akun',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kas/Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_akun',
                        id: 'nama_akunid',
                        ref: '../nama_akun',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_bank',
                        id: 'nama_bankid',
                        ref: '../nama_bank',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'No Rekening Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_rekening',
                        id: 'no_rekeningid',
                        ref: '../no_rekening',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Telepon',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_tlp',
                        id: 'no_tlpid',
                        ref: '../no_tlp',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Alamat Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'alamat_bank',
                        id: 'alamat_bankid',
                        ref: '../alamat_bank',
                        maxLength: 225,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    new jun.comboActive({
                        fieldLabel: "Status",
                        hideLabel: !1,
                        width: 100,
                        height: 20,
                        name: "status",
                        id: "statusid",
                        ref: "../status",
                        hiddenName: "status"
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BankWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.akun.on('expand', this.onFocuskatagori, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onActivate: function () {
        jun.rztChartMasterComp.FilterDataHeader();
    },
    onWinClose: function () {
        jun.rztChartMasterComp.clearFilter();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Bank/update/id/' + this.id;
        } else {
            urlz = 'Bank/create/';
        }
        Ext.getCmp('form-Bank').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBank.reload();
                jun.rztBankCmp.reload();
                jun.rztBankLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Bank').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.TranferBankWin = Ext.extend(Ext.Window, {
    title: "Mutasi Kas",
    modez: 1,
    id: "win-transfer-bank",
    width: 600, height: 250,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-Transfer-bank",
                labelWidth: 100,
                labelAlign: "left",
                layout: "column",
                ref: "formz",
                border: !1,
                items: [
                    {
                        columnWidth: .5,
                        baseCls: "x-plain",
                        bodyStyle: "padding:1px",
                        layout: "form",
                        ref: "col1",
                        items: [
                            {
                                xtype: "combo",
                                typeAhead: !0,
                                allowBlank: false,
                                triggerAction: "all",
                                lazyRender: !0,
                                mode: "local",
                                fieldLabel: "Bank Asal",
                                store: jun.rztBankCmp,
                                hiddenName: "bank_act_asal",
                                valueField: "id_bank",
                                ref: "../../cmbBankAsal",
                                forceSelection: !0,
                                displayField: "nama_akun",
                                anchor: "100%",
                                lastQuery: ""
                            },
                            {
                                xtype: "numericfield",
                                fieldLabel: "Bank Balance",
                                hideLabel: !1,
                                name: "bank_bal",
                                id: "bank_bal_id",
                                readOnly: !0,
                                ref: "../../lblBankBal",
                                anchor: "100%"
                            },
                            {
                                xtype: "combo",
                                typeAhead: !0,
                                allowBlank: false,
                                triggerAction: "all",
                                lazyRender: !0,
                                mode: "local",
                                fieldLabel: "Bank Tujuan",
                                store: jun.rztBankCmp,
                                hiddenName: "bank_act_tujuan",
                                valueField: "id_bank",
                                forceSelection: !0,
                                displayField: "nama_akun",
                                anchor: "100%",
                                ref: "../../cmbBankTujuan",
                                lastQuery: ""
                            },
                            {
                                xtype: "xdatefield",
                                allowBlank: false,
                                fieldLabel: "Tanggal Transfer",
                                hideLabel: !1,
                                name: "trans_date",
                                id: "refid",
                                maxLength: 40,
                                anchor: "100%"
                            },
                            {
                                xtype: "numericfield",
                                fieldLabel: "Jumlah",
                                allowBlank: false,
                                value: 0,
                                hideLabel: !1,
                                name: "amount",
                                id: "amountid",
                                ref: "../../amount",
                                maxLength: 20,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        columnWidth: .5,
                        baseCls: "x-plain",
                        bodyStyle: "padding:5px 0 5px 5px",
                        layout: "column",
                        items: [
                            {xtype: "label", text: "Memo :"},
                            {xtype: "textarea", name: "memo", id: "memoid", height: "77%", width: "100%"}
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Simpan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Simpan & Tutup",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.TranferBankWin.superclass.initComponent.call(this);
        this.cmbBankAsal.on("select", this.oncmbBankAsalChange, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.cmbBankAsal.on("focus", this.onLoadBank, this);
        this.cmbBankTujuan.on("focus", this.onLoadBank, this);
        this.on("close", this.onWinClose, this)
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a);
        this.btnSaveClose.setDisabled(a);
    },
    onWinClose: function () {
        jun.rztBankCmp.clearFilter();
    },
    onLoadBank: function () {
        jun.rztBankCmp.FilterData();
    },
    onActivate: function () {
        this.btnSave.hidden = !1;
    },
    oncmbBankAsalChange: function (a, b, c) {
        Ext.Ajax.request({
            waitMsg: "Please Wait",
            url: "BankTrans/GetBalance",
            params: {
                id: this.cmbBankAsal.getValue()
            },
            success: function (a) {
                var a = Ext.decode(a.responseText);
                Ext.getCmp("bank_bal_id").setValue(a.id);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        })
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.cmbBankAsal.getValue() == this.cmbBankTujuan.getValue()) {
            Ext.MessageBox.alert("Error", "Bank tujuan tidak boleh sama dengan bank asal.");
            this.btnDisabled(!1);
            return;
        }
        if (parseInt(this.amount.getValue()) == 0) {
            Ext.MessageBox.alert("Error", "Jumlah transfer tidak boleh nol.");
            this.btnDisabled(!1);
            return;
        }
        var a = "BankTrans/createtransfer";
        Ext.getCmp("form-Transfer-bank").getForm().submit({
            url: a,
            timeOut: 1e3,
            scope: this,
            success: function (a, b) {
                var c = Ext.decode(b.response.responseText);
                if (c.success == 0) {
                    Ext.MessageBox.show({
                        title: "Transfer",
                        msg: c.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    this.btnDisabled(!1);
                    return;
                }
                Ext.MessageBox.show({
                    title: "Transfer", msg: c.msg + "<br /> Ref. Dokumen : " +
                                            c.id, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO
                });
                Ext.getCmp("form-Transfer-bank").getForm().reset();
                jun.rztBankTrans.reload();
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(!1);
            }
        })
    }, onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    }, onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    }, onbtnCancelclick: function () {
        this.close();
    }
});