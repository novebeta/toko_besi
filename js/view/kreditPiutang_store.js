jun.KreditPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.KreditPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KreditPiutangStoreId',
            url: 'KreditPiutang',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id'},
{name:'retur_jual_id'},
{name:'kas_diterima'},
{name:'penjualan_id'},
                
            ]
        }, cfg));
    }
});
jun.rztKreditPiutang = new jun.KreditPiutangstore();
//jun.rztKreditPiutang.load();
