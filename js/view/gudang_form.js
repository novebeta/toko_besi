jun.GudangWin = Ext.extend(Ext.Window, {
    title: 'Gudang',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Gudang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'gudang_code',
                        hideLabel: false,
                        //hidden:true,
                        name: 'gudang_code',
                        id: 'gudang_codeid',
                        ref: '../gudang_code',
                        maxLength: 5,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfieldld',
                        fieldLabel: 'gudang_name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'gudang_name',
                        id: 'gudang_nameid',
                        ref: '../gudang_name',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfieldld',
                        fieldLabel: 'delivery_address',
                        hideLabel: false,
                        //hidden:true,
                        name: 'delivery_address',
                        id: 'delivery_addressid',
                        ref: '../delivery_address',
                        anchor: '100%'
                        //allowBlank:
                    },
                    {
                        xtype: 'uctextfieldld',
                        fieldLabel: 'phone',
                        hideLabel: false,
                        //hidden:true,
                        name: 'phone',
                        id: 'phoneid',
                        ref: '../phone',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfieldld',
                        fieldLabel: 'phone2',
                        hideLabel: false,
                        //hidden:true,
                        name: 'phone2',
                        id: 'phone2id',
                        ref: '../phone2',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfieldld',
                        fieldLabel: 'fax',
                        hideLabel: false,
                        //hidden:true,
                        name: 'fax',
                        id: 'faxid',
                        ref: '../fax',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        id: 'emailid',
                        ref: '../email',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'contact',
                        hideLabel: false,
                        //hidden:true,
                        name: 'contact',
                        id: 'contactid',
                        ref: '../contact',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'status',
                        hideLabel: false,
                        //hidden:true,
                        name: 'status',
                        id: 'statusid',
                        ref: '../status',
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GudangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Gudang/update/id/' + this.id;
        } else {
            urlz = 'Gudang/create/';
        }
        Ext.getCmp('form-Gudang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztGudang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Gudang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});