jun.PelunasanUtangWin = Ext.extend(Ext.Window, {
    title: 'Pelunasan Utang',
    modez: 1,
    width: 590,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PelunasanUtang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Dokumen:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        allowBlank: false,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "No Bukti:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_bukti',
                        ref: '../no_bukti',
                        maxLength: 15,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Suplier:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        width: 175,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Rekening:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        hiddenValue: DEFKASPELPIU,
                        value: DEFKASPELPIU,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: 50,
                        width: 175,
                        readOnly: true,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "BG/Cek:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'no_bg_cek',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_bg_cek',
                        id: 'no_bg_cekid',
                        ref: '../no_bg_cek',
                        width: 175,
                        value: '-',
                        x: 85,
                        y: 92
                    },
                    new jun.PelunasanUtangDetilGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        name: 'total',
                        id: 'totalPelunasanUtangid',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        x: 375,
                        y: 422
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose',
                    id:'btnSaveClosePelunasanUtang'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        jun.PelunasanUtangWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.supplier.on('select', this.onSupplierChange, this);
        this.on("close", this.onWinClose, this);

    },
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            Ext.getCmp("btnSaveClosePelunasanUtang").setVisible(false);
        } else {
            Ext.getCmp("btnSaveClosePelunasanUtang").setVisible(true);
        }
    },
    onWinClose : function() {
        jun.rztPelunasanUtangDetil.removeAll();
        jun.rztFakturUtang.removeAll();
//        Ext.getCmp('form-PelunasanUtang').getForm().reset();
    },
    onSupplierChange: function (c,r,i) {
        //this.doc_ref.reset();
        this.id_bank.reset();
        this.no_bg_cek.reset();
        //var supplier = jun.getSupplier(r.data.orang_id);
        this.alamat.setValue(r.data.alamat);

//        jun.rztPelunasanUtang.load();
//        jun.rztPelunasanUtangDetil.removeAll();
//        jun.rztPelunasanUtangDetil.baseParams = {
//            supplier_id: supplier_id
//        };
//        jun.rztPelunasanUtangDetil.load();
//        jun.rztPelunasanUtangDetil.baseParams = {};
        jun.rztFakturUtang.baseParams = {
            supplier_id: r.data.orang_id
        };
        jun.rztFakturUtang.load();
        jun.rztFakturUtang.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'PelunasanUtang/create';
        Ext.getCmp('form-PelunasanUtang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPelunasanUtangDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPelunasanUtang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PelunasanUtang').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});