jun.PelunasanPiutangWin = Ext.extend(Ext.Window, {
    title: 'Pelunasan Piutang',
    modez: 1,
    width: 590,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PelunasanPiutang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 15,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        allowBlank: false,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Konsumen:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: "Rekening:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        hiddenValue: DEFKASPELPIU,
                        value: DEFKASPELPIU,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "BG/Cek:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'no_bg_cek',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_bg_cek',
                        id: 'no_bg_cekid',
                        ref: '../no_bg_cek',
                        width: 175,
                        value: '-',
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        minChars:3,
                        store: jun.rztKonsumenCmp,
                        forceSelection: true,
                        hiddenName: 'konsumen_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item-table",
                        tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                            '<div class="cell4" style="width: 100px;text-align: right;">{kode}</div>',
                            '<div class="cell4" style="width: 250px;">{nama}</div>',
                            "</div></tpl>", '</div>'),
                        listWidth: 350,
                        hideTrigger: true,
                        ref: '../konsumen',
                        allowBlank: false,
                        width: 175,
                        x: 375,
                        y: 2
                    },

                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: 50,
                        width: 175,
                        readOnly: true,
                        x: 375,
                        y: 32
                    },
                    new jun.PelunasanPiutangDetilGrid({
                        x: 5,
                        y: 125,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        x: 375,
                        y: 422
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose',
                    id:'btnSaveClosePelunasanPiutang'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PelunasanPiutangWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.konsumen.on('select', this.onKonsumenChange, this);
        this.on("close", this.onWinClose, this);
        //jun.rztPelunasanPiutang.on({
        //    beforeload : {
        //        fn : function(a, b) {
        //            b.params = {
        //                tgl : Ext.getCmp("tglid").getValue(),
        //                konsumen_id :  Ext.getCmp("konsumen_id").getValue()
        //            }
        //        },
        //        scope : this
        //    },
        //    load : {
        //        fn : function(a, b, c) {
        //            if (b.length > 0) {
        //                this.modez = 1;
        //                var pelunasan = jun.rztPelunasanPiutang.getAt(0);
        //                this.id = pelunasan.data.pelunasan_piutang_id;
        //                Ext.getCmp("doc_refid").setValue(pelunasan.data.doc_ref);
        //                Ext.getCmp("id_banktid").setValue(pelunasan.data.id_bank);
        //                Ext.getCmp("no_bg_cekid").setValue(pelunasan.data.no_bg_cek);
        //                Ext.getCmp("totalid").setValue(pelunasan.data.total);
        //            }else{
        //                if (jun.rztSetorKasSales.totalLength == 0) {
        //                    this.modez = 0
        //                } else {
        //                    this.modez = 1;
        //                }
        //            }
        //            c.params = {};
        //            this.onActivate();
        //        },
        //        scope : this
        //    }
        //})
    },
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            Ext.getCmp("btnSaveClosePelunasanPiutang").setVisible(false);
        } else {
            Ext.getCmp("btnSaveClosePelunasanPiutang").setVisible(true);
        }
    },
    onWinClose : function() {
        jun.rztPelunasanPiutangDetil.removeAll();
        jun.rztFakturPiutang.removeAll();
//        Ext.getCmp('form-PelunasanPiutang').getForm().reset();
    },
    onKonsumenChange: function (c,r,i) {
        //this.doc_ref.reset();
        //this.id_bank.reset();
        //this.no_bg_cek.reset();
        //var konsumen_id = this.konsumen.getValue();
        //var konsumen = jun.getKonsumen(konsumen_id);
        //var area = jun.getArea(konsumen.data.area_id);
        this.alamat.setValue(r.data.alamat);
        jun.rztPelunasanPiutangDetil.baseParams = {};
        jun.rztFakturPiutang.baseParams = {
            konsumen_id: r.data.orang_id
            //tgl: this.tgl.getValue()
        };
        jun.rztFakturPiutang.load();
        jun.rztFakturPiutang.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztPelunasanPiutangDetil.data.length === 0) {
            Ext.MessageBox.alert("Error", "Item details must set.");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'PelunasanPiutang/create';
        Ext.getCmp('form-PelunasanPiutang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPelunasanPiutangDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
//                jun.rztPelunasanPiutang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PelunasanPiutang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});