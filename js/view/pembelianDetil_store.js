jun.PembelianDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembelianDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembelianDetilStoreId',
            url: 'PembelianDetil',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'pembelian_detil_id'},
                {name: 'barang_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'nominal', type: 'float'},
                {name: 'pcs', type: 'float'},
                {name: 'totalpot', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'pembelian_id'},
                {name: 'disc2', type: 'float'},
                {name: 'pot', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var biaya_bongkar = parseFloat(Ext.getCmp("biaya_bongkarid").getValue());
        var biaya_angkut = parseFloat(Ext.getCmp("biaya_angkutid").getValue());
        var allowance = parseFloat(Ext.getCmp("allowanceid").getValue());
        var disc = parseFloat(Ext.getCmp("discid").getValue());
        var nominal = this.sum("nominal");
        Ext.getCmp("sub_totalid").setValue(nominal);
        var total = nominal - disc + biaya_bongkar + biaya_angkut - allowance;
        Ext.getCmp("totalid").setValue(total);
        var dp = parseFloat(Ext.getCmp("uang_mukaid").getValue());
        if (total - dp < 0) {
            Ext.getCmp("uang_mukaid").setValue(0);
            Ext.getCmp("sisa_tagihanid").setValue(total);
        } else {
            Ext.getCmp("sisa_tagihanid").setValue(total - dp);
        }
    }
});
jun.rztPembelianDetil = new jun.PembelianDetilstore();
//jun.rztPembelianDetil.load();
