jun.PelunasanPiutangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanPiutangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanPiutangDetilStoreId',
            url: 'PelunasanPiutangDetil',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_piutang_detil_id'},
                {name: 'kas_diterima', type: 'float'},
                {name: 'pelunasan_piutang_id'},
                {name: 'penjualan_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_diterima");
        Ext.getCmp("totalid").setValue(subtotal);
    }
});
jun.rztPelunasanPiutangDetil = new jun.PelunasanPiutangDetilstore();
jun.FakturPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FakturPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FakturPiutangStoreId',
            url: 'PelunasanPiutangDetil/faktur',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_id'},
                {name: 'konsumen_id'},
                {name: 'so'},
                {name: 'tgl'},
                {name: 'tempo'},
                {name: 'doc_ref'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztFakturPiutang = new jun.FakturPiutangstore();

//jun.FakturPiutangReturJualstore = Ext.extend(Ext.data.JsonStore, {
//    constructor: function (cfg) {
//        cfg = cfg || {};
//        jun.FakturPiutangstore.superclass.constructor.call(this, Ext.apply({
//            storeId: 'FakturPiutangReturJualStoreId',
//            url: 'PelunasanPiutangDetil/faktur',
//            autoLoad: !1,
//            autoSave: !1,
//            root: 'results',
//            totalProperty: 'total',
//            fields: [
//                {name: 'penjualan_id'},
//                {name: 'tgl'},
//                {name: 'no_faktur'},
//                {name: 'nilai', type: 'float'},
//                {name: 'sisa', type: 'float'}
//            ]
//        }, cfg));
//    }
//});
//jun.rztFakturPiutangReturJual = new jun.FakturPiutangReturJualstore();

jun.PiutangSalesDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PiutangSalesDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PiutangSalesDetilstoreId',
            url: 'PelunasanPiutangDetil/piutangSales',
            autoLoad: !1,
            autoSave: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_id'},
                {name: 'tgl'},
                {name: 'no_faktur'},
                {name: 'konsumen_code'},
                {name: 'konsumen_name'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'},
                {name: 'cetak', type: 'boolean'}
            ]
        }, cfg));
    }
});
jun.rztPiutangPerSales = new jun.PiutangSalesDetilstore();