jun.ReturJualDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturJualDetilstore.superclass.constructor.call(this, Ext
            .apply({
                storeId: 'ReturJualDetilStoreId',
                autoLoad: !1,
                autoSave: !1,
                url: 'ReturJualDetil',
                root: 'results',
                totalProperty: 'total',
                fields: [
                    {
                        name: 'retur_jual_detil_id'
                    },
                    {
                        name: 'retur_jual_id'
                    },
                    {
                        name: 'sat'
                    },
                    {
                        name: 'jml', type: 'float'
                    },
                    {
                        name: 'price', type: 'float'
                    },
                    {
                        name: 'disc1', type: 'float'
                    },
                    {
                        name: 'disc2', type: 'float'
                    },
                    {
                        name: 'pot', type: 'float'
                    },
                    {
                        name: 'totalpot', type: 'float'
                    },
                    {
                        name: 'bruto', type: 'float'
                    },
                    {
                        name: 'nominal', type: 'float'
                    },
                    {
                        name: 'pcs', type: 'float'
                    },
                    {
                        name: 'barang_id'
                    },
                    {name: 'hpp', type: 'float'}
                ]
            }, cfg));
    },
    refreshData: function (a) {
        var total = this.sum("nominal");
        var disc = this.sum("totalpot");
        var bruto = this.sum("bruto");
        Ext.getCmp("discreturid").setValue(disc);
        Ext.getCmp("brutoreturid").setValue(bruto);
        Ext.getCmp("totalreturid").setValue(total);
        var dp = parseFloat(Ext.getCmp("kaskeluarid").getValue());
        if (total - dp < 0) {
            Ext.getCmp("kaskeluarid").setValue(0);
        } else {
            Ext.getCmp("kreditpiutangid").setValue(total - dp);
        }
    }
});
jun.rztReturJualDetil = new jun.ReturJualDetilstore();
// jun.rztReturJualDetil.load();
