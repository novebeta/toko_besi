jun.BankTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.BankTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankTransStoreId',
            url: 'BankTrans',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'bank_trans_id'},
{name:'type'},
{name:'trans_no'},
{name:'ref'},
{name:'trans_date'},
{name:'amount'},
{name:'id_bank'},
{name:'id_user'},
                
            ]
        }, cfg));
    }
});
jun.rztBankTrans = new jun.BankTransstore();
//jun.rztBankTrans.load();
