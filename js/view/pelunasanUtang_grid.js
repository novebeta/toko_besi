jun.PelunasanUtangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pelunasan Utang",
    id: 'docs-jun.PelunasanUtangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Dokumen',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header:'Suplier',
            sortable:true,
            resizable:true,
            dataIndex:'nama',
            width:100
        },
        {
            header: 'No. Bukti',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bukti',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        jun.rztPelunasanUtang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.mode = 'grid';
                    b.params.supplier_id = Ext.getCmp('supplierpelunasanutanggridid').getValue();
                }
            }
        });
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        this.store = jun.rztPelunasanUtang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Pelunasan Utang',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Pelunasan Utang',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Supplier :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    ref: '../supplier',
                    id: 'supplierpelunasanutanggridid',
                    fieldLabel: 'supplier_id',
                    store: jun.rztSupplierCmp,
                    hiddenName: 'supplier_id',
                    valueField: 'orang_id',
                    displayField: 'nama',
                    listWidth: 350,
                    matchFieldWidth: !1,
                    width: 175
                }
            ]
        };
//        this.store.baseParams = {mode: "grid"};
//        this.store.reload();
//        this.store.baseParams = {};
        jun.PelunasanUtangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.supplier.on('select', function(){this.store.reload();}, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztPelunasanUtang.removeAll();
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PelunasanUtangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pelunasan utang");
            return;
        }
        var idz = selectedz.json.pelunasan_utang_id;
        var form = new jun.PelunasanUtangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //var supplier = jun.getSupplier(selectedz.json.supplier_id);
        //form.onSupplierChange(null,supplier,-1);
        jun.rztPelunasanUtangDetil.baseParams = {
            pelunasan_utang_id: idz
        };
        jun.rztPelunasanUtangDetil.load();
        jun.rztPelunasanUtangDetil.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PelunasanUtang/delete/id/' + record.json.pelunasan_Utang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPelunasanUtang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
