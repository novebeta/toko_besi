jun.SupplierGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Suplier",
    id: 'docs-jun.SupplierGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [{
                  header: 'Kode Suplier',
                  sortable: true,
                  resizable: true,
                  dataIndex: 'kode',
                  width: 100,
                  filter: {
                      xtype: "textfield"
                  }
              },
              {
                  header: 'Nama Suplier',
                  sortable: true,
                  resizable: true,
                  dataIndex: 'nama',
                  width: 100,
                  filter: {
                      xtype: "textfield"
                  }
              },
              {
                  header: 'Phone',
                  sortable: true,
                  resizable: true,
                  dataIndex: 'phone',
                  width: 100,
                  filter: {
                      xtype: "textfield"
                  }
              }
    ],
    initComponent: function () {
        this.store = jun.rztSupplierMaster;
        this.bbar = {
            items: [{
                        xtype: 'paging',
                        store: this.store,
                        displayInfo: true,
                        pageSize: 20
                    }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [{
                        xtype: 'button',
                        text: 'Tambah Suplier',
                        ref: '../btnAdd'
                    }, {
                        xtype: 'tbseparator'
                    }, {
                        xtype: 'button',
                        text: 'Ubah Suplier',
                        ref: '../btnEdit'
                    }]
        };
        this.store.baseParams = {
            mode: "grid"
        };
        this.store.reload();
        this.store.baseParams = {};
        jun.SupplierGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SupplierWin({
            modez: 0
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.orang_id;
        var form = new jun.SupplierWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Supplier/delete/id/' + record.json.supplier_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSupplier.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})