jun.ReturPembelianWin = Ext.extend(Ext.Window, {
    title: 'Retur Pembelian',
    modez: 1,
    width: 1015,
    height: 485,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturPembelian',
                labelWidth: 100,
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Retur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'no_faktur_beli',
                        hideLabel: false,
                        name: 'no_faktur_beli',
                        ref: '../no_faktur_beli',
                        x: 105,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true,
                        listeners: {
                            render: function (c) {
                                Ext.QuickTips.register({
                                    target: c,
                                    text: 'No faktur ReturPembelian otomatis'
                                });
                            }
                        }
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 310,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "52",
                        width: 175,
                        readOnly: true,
                        x: 475,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tgl Faktur:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        x: 105,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Pemasok:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        width: 175,
                        x: 105,
                        y: 62
                    },
                    new jun.ReturPembelianDetilGrid({
                        x: 5,
                        y: 95,
                        height: 250,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 5,
                        y: 355
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'sub_total',
                        hideLabel: false,
                        name: 'sub_total',
                        id: 'sub_totalid',
                        value: 0,
                        ref: '../sub_total',
                        readOnly: true,
                        width: 175,
                        x: 105,
                        y: 352
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 5,
                        y: 385
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'disc',
                        hideLabel: false,
                        name: 'disc',
                        id: 'discid',
                        value: 0,
                        ref: '../disc',
                        //readOnly: true,
                        width: 175,
                        x: 105,
                        y: 382
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Ongkos Angkut:",
                    //    x: 310,
                    //    y: 355
                    //},
                    //{
                    //    xtype: 'numericfield',
                    //    hideLabel: false,
                    //    name: 'biaya_angkut',
                    //    id: 'biaya_angkutid',
                    //    value: 0,
                    //    ref: '../biaya_angkut',
                    //    //readOnly: true,
                    //    width: 175,
                    //    x: 475,
                    //    y: 352
                    //},
                    {
                        xtype: 'hidden',
                        id: 'biaya_angkutid',
                        name: 'biaya_angkut',
                        value: 0
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 310,
                        y: 355
                        //x: 310,
                        //y: 382
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        name: 'total',
                        id: 'totalid',
                        value: 0,
                        ref: '../total',
                        readOnly: true,
                        width: 175,
                        x: 475,
                        y: 352
                        //x: 475,
                        //y: 385
                    },
                    {
                        xtype: "label",
                        text: "Bank:",
                        x: 715,
                        y: 355
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBank,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        width: 175,
                        x: 805,
                        y: 352
                    },
                    {
                        xtype: "label",
                        text: "Kas Masuk:",
                        x: 310,
                        y: 382
                        //x: 715,
                        //y: 385
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'uang_muka',
                        hideLabel: false,
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        ref: '../uang_muka',
                        value: 0,
                        width: 175,
                        x: 475,
                        y: 385
                        //x: 805,
                        //y: 382
                    },
                    {
                        xtype: "label",
                        text: "Debet Hutang:",
                        x: 715,
                        y: 385
                        //x: 715,
                        //y: 415
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'sisa_tagihan',
                        hideLabel: false,
                        name: 'sisa_tagihan',
                        id: 'sisa_tagihanid',
                        ref: '../sisa_tagihan',
                        readOnly: true,
                        value: 0,
                        width: 175,
                        x: 805,
                        y: 382
                        //x: 805,
                        //y: 412
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturPembelianWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.uang_muka.on('change', this.updateTotal, this);
        this.disc.on('change', this.updateTotal, this);
        this.supplier.on('change', this.onSuppChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztReturPembelianDetil.removeAll();
    },
    updateTotal: function () {
        jun.rztReturPembelianDetil.refreshData();
    },
    onSuppChange: function () {
        var supplier_id = this.supplier.getValue();
        if (supplier_id == '' || supplier_id == null) {
            return;
        }
        var supplier = jun.getSupplier(supplier_id);
        this.alamat.setValue(supplier.data.alamat);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturPembelian/create/';
        Ext.getCmp('form-ReturPembelian').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztReturPembelianDetil.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztReturPembelian.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ReturPembelian').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});