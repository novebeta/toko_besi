jun.Commentsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Commentsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CommentsStoreId',
            url: 'Comments',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id'},
{name:'type'},
{name:'type_no'},
{name:'date_'},
{name:'memo_'},
                
            ]
        }, cfg));
    }
});
jun.rztComments = new jun.Commentsstore();
//jun.rztComments.load();
