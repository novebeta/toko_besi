jun.DetilPenjualanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "DetilPenjualan",
    id: 'docs-jun.DetilPenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Barcode',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'SAT',
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        {
            header: 'Harga',
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Jml',
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc(%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        //{
        //    header: 'Disc 2(%)',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'disc2',
        //    width: 100,
        //    align: "right",
        //    renderer: Ext.util.Format.numberRenderer("0,0.00")
        //},
        {
            header: 'Pot(Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'pot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztDetilPenjualan;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            colspan: 7,
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                                '<span style="float:left;width: 15%;">{barcode}</span><span style="float:left;width: 75%;">{barang_name}</span><span>{sisa}</span>',
                                "</div></tpl>"),
                            listWidth: 750,
                            store: jun.rztBarang,
                            valueField: 'barang_id',
                            displayField: 'barang_name',
                            forceSelection: true,
                            hideTrigger: true,
                            style: 'margin-bottom:2px',
                            ref: '../../barang',
                            width: 399
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'hrgid',
                            ref: '../../hrg',
                            width: 100,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disk :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'disc1id',
                            ref: '../../disc1',
                            width: 45,
                            minValue: 0,
                            value: 0,
                            maxValue: 75,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Potongan :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'potid',
                            ref: '../../pot',
                            value: 0,
                            minValue: 0,
                            width: 100,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Jumlah :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'jmlid',
                            ref: '../../jml',
                            width: 50,
                            value: 1,
                            minValue: 0
                        }
                    ]
                },
                //{
                //    xtype: 'buttongroup',
                //    columns: 4,
                //    defaults: {
                //        scale: 'small'
                //    },
                //    items: [
                //        {
                //            text: 'Disk 1 :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'disc1id',
                //            ref: '../../disc1',
                //            width: 45,
                //            minValue: 0,
                //            value: 0,
                //            maxValue: 100
                //        },
                //        {
                //            text: '\xA0Disc 2 :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'disc2id',
                //            ref: '../../disc2',
                //            width: 45,
                //            value: 0,
                //            minValue: 0,
                //            maxValue: 100
                //        },
                //        {
                //            text: 'Potongan :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'potid',
                //            ref: '../../pot',
                //            value: 0,
                //            colspan: 3,
                //            minValue: 0
                //        }
                //    ]
                //},
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Tambah',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Ubah',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Hapus',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.DetilPenjualanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.barang.on('select', this.onChangeBarang, this);
        //this.sat.on('select', this.onChangeSat, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
//        this.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Ubah') {
            this.barang.setValue(record.data.barang_id);
            this.onChangeBarang();
            this.hrg.setValue(record.data.price);
            this.disc1.setValue(record.data.disc1);
            this.jml.setValue(record.data.jml);
            this.pot.setValue(record.data.pot);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Ubah");
            this.btnDisable(false);
        }
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        this.hrg.setValue(barang.data.price);
        this.disc1.setValue(barang.data.disc);
        this.pot.setValue(barang.data.disc_rp);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztDetilPenjualan.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
//                return
//            }
//        }
//        var sat = this.sat.getValue();
//        if (sat == "") {
//            Ext.MessageBox.alert("Error", "Satuan harus dipilih.");
//            return
//        }
        var barang = jun.getBarang(barang_id);
        var sat = barang.data.satuan;
//        var pcs;
        var price = parseFloat(this.hrg.getValue());
        var jml = parseFloat(this.jml.getValue());
        var disc = parseFloat(this.disc1.getValue());
        var disc2 = 0;
        var disc_rp = parseInt(this.pot.getValue());
        var bruto = Math.round(price * jml);
//        var totdisc1 = bruto * (disc1 / 100);
//        var totdisc2 = bruto * (disc2 / 100);
//        var totalpot = (totdisc1 + totdisc2) + pot;
        var nominal = jun.calculateDisc(jml, price, disc, disc2, disc_rp);
        var totalpot = bruto - nominal;
        //var hpp = Math.round(jml * barang.data.price_buy);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('sat', sat);
            record.set('jml', jml);
            record.set('price', price);
            record.set('disc1', disc);
            record.set('disc2', disc2);
            record.set('pot', disc_rp);
            record.set('totalpot', totalpot);
            record.set('nominal', nominal);
            //record.set('pcs', pcs);
            record.set('bruto', bruto);
            //record.set('hpp', hpp);
            record.commit();
        } else {
            var c = jun.rztDetilPenjualan.recordType,
                d = new c({
                    barang_id: barang_id,
                    sat: sat,
                    jml: jml,
                    price: price,
                    disc1: disc,
                    disc2: disc2,
                    pot: disc_rp,
                    totalpot: totalpot,
                    nominal: nominal,
                    //pcs: pcs,
                    bruto: bruto
                    //hpp: hpp
                });
            jun.rztDetilPenjualan.add(d);
        }
        this.store.refreshData();
        this.barang.reset();
        //this.sat.reset();
        this.jml.reset();
        this.hrg.reset();
        this.disc1.reset();
        //this.disc2.reset();
        this.pot.reset();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.detil_penjualan;
        var form = new jun.DetilPenjualanWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
        this.store.refreshData();
    }
});
jun.DetilReturPenjualanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "DetilReturPenjualan",
    id: 'docs-jun.DetilReturPenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Barcode',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'SAT',
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        {
            header: 'Harga',
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Jml',
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc(%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        //{
        //    header: 'Disc 2(%)',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'disc2',
        //    width: 100,
        //    align: "right",
        //    renderer: Ext.util.Format.numberRenderer("0,0.00")
        //},
        {
            header: 'Pot(Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'pot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztDetilPenjualan;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            colspan: 7,
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="font-weight: bold">',
                                '<span style="float:left;width: 15%;">{barcode}</span><span style="float:left;width: 75%;">{barang_name}</span><span>{sisa}</span>',
                                "</div></tpl>"),
                            listWidth: 750,
                            store: jun.rztBarang,
                            valueField: 'barang_id',
                            displayField: 'barang_name',
                            forceSelection: true,
                            hideTrigger: true,
                            style: 'margin-bottom:2px',
                            ref: '../../barang',
                            width: 399
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'hrgid',
                            ref: '../../hrg',
                            width: 100,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disk :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'disc1id',
                            ref: '../../disc1',
                            width: 45,
                            minValue: 0,
                            value: 0,
                            maxValue: 75,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Potongan :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'potid',
                            ref: '../../pot',
                            value: 0,
                            minValue: 0,
                            width: 100,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Jumlah :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'jmlid',
                            ref: '../../jml',
                            width: 50,
                            value: 1,
                            minValue: 1
                        }
                    ]
                },
                //{
                //    xtype: 'buttongroup',
                //    columns: 4,
                //    defaults: {
                //        scale: 'small'
                //    },
                //    items: [
                //        {
                //            text: 'Disk 1 :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'disc1id',
                //            ref: '../../disc1',
                //            width: 45,
                //            minValue: 0,
                //            value: 0,
                //            maxValue: 100
                //        },
                //        {
                //            text: '\xA0Disc 2 :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'disc2id',
                //            ref: '../../disc2',
                //            width: 45,
                //            value: 0,
                //            minValue: 0,
                //            maxValue: 100
                //        },
                //        {
                //            text: 'Potongan :'
                //        },
                //        {
                //            xtype: 'numericfield',
                //            id: 'potid',
                //            ref: '../../pot',
                //            value: 0,
                //            colspan: 3,
                //            minValue: 0
                //        }
                //    ]
                //},
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Tambah',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Ubah',
                            height: 44,
                            ref: '../   ../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Hapus',
                            height: 44,
                            ref: '../   ../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.DetilReturPenjualanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.barang.on('select', this.onChangeBarang, this);
        //this.sat.on('select', this.onChangeSat, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
//        this.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Ubah') {
            this.barang.setValue(record.data.barang_id);
            this.onChangeBarang();
            this.hrg.setValue(record.data.price);
            this.disc1.setValue(record.data.disc1);
            this.jml.setValue(record.data.jml);
            this.pot.setValue(record.data.pot);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Ubah");
            this.btnDisable(false);
        }
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        this.hrg.setValue(barang.data.price);
        this.disc1.setValue(barang.data.disc);
        this.pot.setValue(barang.data.disc_rp);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztDetilReturPenjualan.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
//                return
//            }
//        }
//        var sat = this.sat.getValue();
//        if (sat == "") {
//            Ext.MessageBox.alert("Error", "Satuan harus dipilih.");
//            return
//        }
        var barang = jun.getBarang(barang_id);
        var sat = barang.data.satuan;
//        var pcs;
        var price = parseFloat(this.hrg.getValue());
        var jml = parseFloat(this.jml.getValue());
        var disc = parseFloat(this.disc1.getValue());
        var disc2 = 0;
        var disc_rp = parseInt(this.pot.getValue());
        var bruto = Math.round(price * jml);
//        var totdisc1 = bruto * (disc1 / 100);
//        var totdisc2 = bruto * (disc2 / 100);
//        var totalpot = (totdisc1 + totdisc2) + pot;
        var nominal = jun.calculateDisc(jml, price, disc, disc2, disc_rp);
        var totalpot = bruto - nominal;
        //var hpp = Math.round(jml * barang.data.price_buy);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('sat', sat);
            record.set('jml', jml);
            record.set('price', price);
            record.set('disc1', disc);
            record.set('disc2', disc2);
            record.set('pot', disc_rp);
            record.set('totalpot', totalpot);
            record.set('nominal', nominal);
            //record.set('pcs', pcs);
            record.set('bruto', bruto);
            //record.set('hpp', hpp);
            record.commit();
        } else {
            var c = jun.rztDetilPenjualan.recordType,
                d = new c({
                    barang_id: barang_id,
                    sat: sat,
                    jml: jml,
                    price: price,
                    disc1: disc,
                    disc2: disc2,
                    pot: disc_rp,
                    totalpot: totalpot,
                    nominal: nominal,
                    //pcs: pcs,
                    bruto: bruto
                    //hpp: hpp
                });
            jun.rztDetilPenjualan.add(d);
        }
        this.store.refreshData();
        this.barang.reset();
        //this.sat.reset();
        this.jml.reset();
        this.hrg.reset();
        this.disc1.reset();
        //this.disc2.reset();
        this.pot.reset();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.detil_ReturPenjualan;
        var form = new jun.DetilReturPenjualanWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
        this.store.refreshData();
    }
});
