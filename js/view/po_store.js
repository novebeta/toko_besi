jun.Postore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Postore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PoStoreId',
            url: 'Po',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_id'},
                {name: 'no_po'},
                {name: 'tgl'},
                {name: 'tgl_butuh'},
                {name: 'supplier_id'},
                {name: 'id_user'},
                {name: 'done'}
            ]
        }, cfg));
    }
});
jun.rztPo = new jun.Postore();
//jun.rztPo.load();
