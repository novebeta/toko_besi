jun.GlTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (a) {
        a = a || {}, jun.GlTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: "GlTransStoreId",
            url: 'GlTrans',
            autoLoad: !1,
            autoSave: !1,
            root: "results",
            totalProperty: "total",
            fields: [{
                         name: "counter"
                     }, {
                         name: "account"
                     }, {
                         name: "debit",
                         type: "float"
                     }, {
                         name: "kredit",
                         type: "float"
                     }]
        }, a));
    },
    refreshData: function () {
        Ext.getCmp("tot_debit_id").setValue(this.sum("debit")), Ext.getCmp("tot_kredit_id").setValue(this.sum("kredit"));
    }
}), jun.rztGlTrans = new jun.GlTransstore();
jun.JurnalUmumStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JurnalUmumStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JurnalUmumStoreId',
            url: 'GlTrans/ListJurnalUmum',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'type_no'},
                {name: 'tran_date'},
                {name: 'reference'},
                {name: 'tot_debit'},
                {name: 'tot_kredit'}
            ]
        }, cfg));
    }
});
jun.rztJurnalUmum = new jun.JurnalUmumStore();
