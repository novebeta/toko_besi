jun.TerimaDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TerimaDetil",
    id: 'docs-jun.TerimaDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'Jml Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Sat Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 100
        },
        {
            header: 'Jml Stok',
            sortable: true,
            resizable: true,
            dataIndex: 'pcs',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Sat Stok',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderSatBarang
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Jml Beli :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../jml',
                    width: 75,
                    style: 'margin-bottom:2px',
                    minValue: 0
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Jml Stok :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../pcs',
                    width: 75,
                    style: 'margin-bottom:2px',
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    hidden:true,
                    ref: '../btnDelete'
                }
            ]
        };
//        this.store.baseParams = {mode: "grid"};
//        this.store.reload();
//        this.store.baseParams = {};
        jun.TerimaDetilGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    saveDetail: function(){
        var jml = parseFloat(this.jml.getValue());
        var pcs = parseFloat(this.pcs.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('jml', jml);
            record.set('pcs', pcs);
            record.commit();
        } else {
            var c = jun.rztTerimaDetil.recordType,
                d = new c({
                    jml: jml,
                    pcs: pcs
                });
            jun.rztTerimaDetil.add(d);
        }
        this.pcs.reset();
        this.jml.reset();
        this.btnEdit.setText("Edit");
        this.btnDisable(false);
    },
    loadForm: function () {
        var pcs = parseFloat(this.pcs.getValue());
        if (pcs == 0) {
            var msg = Ext.MessageBox.confirm('Pertanyaan', 'Anda menginputkan jumlah stok sebesar 0 unit.<br/>'+
                                                 'Benar memang tidak ada barang yang diterima?', function(btn){

                if (btn == 'yes') {
                    this.saveDetail();
                }
            }, this);
        }else{
            this.saveDetail();
        }
    },
    btnDisable: function (s) {
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.jml.setValue(record.data.jml);
            this.pcs.setValue(record.data.pcs);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item");
            return;
        }
        Ext.MessageBox.prompt('Jumlah Diterima', 'Masukkan jumlah barang diterima:', function (btn, text) {
            if (text == "" || btn == 'cancel') return;
            var selectedz = this.sm.getSelected();
            var idz = selectedz.json.barang_id;
            var num = /^-|\d+/;
            if (num.test(text)) {
                selectedz.set('jml', text);
                selectedz.commit();
            } else {
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: 'Input harus angka.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        }, this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
