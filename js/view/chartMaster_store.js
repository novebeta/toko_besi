jun.ChartMasterstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ChartMasterstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ChartMasterStoreId',
            url: 'ChartMaster',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'account_code'},
                {name: 'account_name'},
                {name: 'status'},
                {name: 'description'},
                {name: 'kategori'},
                {name: 'header'}
            ]
        }, cfg));
    },
    FilterData: function () {
        this.filter([
            {
                property: "status",
                value: "1"
            }
        ])
    },
    FilterDataHeader: function () {
        this.filter([
            {
                property: "header",
                value: "1"
            }
        ])
    },
    FilterDataNotHeader: function () {
        this.filter([
            {
                property: "header",
                value: "0"
            }
        ])
    }
});
jun.rztChartMaster = new jun.ChartMasterstore();
jun.rztChartMasterLib = new jun.ChartMasterstore();
jun.rztChartMasterComp = new jun.ChartMasterstore({baseParams: {mode: "detil"}});
jun.rztChartMasterHeader = new jun.ChartMasterstore({baseParams:{mode: "header"}});
jun.rztChartMasterNonKasBank = new jun.ChartMasterstore({baseParams: {mode: "nonkasbank"}});
//jun.rztChartMaster.load();
