jun.PembelianGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembelian",
    id: 'docs-jun.PembelianGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No Faktur',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur_beli',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Nama Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Ref. Dok Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_supp',
            width: 100
        },
        {
            header: 'Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'tempo',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
        /*
         {
         header:'uang_muka',
         sortable:true,
         resizable:true,
         dataIndex:'uang_muka',
         width:100
         },
         {
         header:'no_bg_cek',
         sortable:true,
         resizable:true,
         dataIndex:'no_bg_cek',
         width:100
         },
         {
         header:'sisa_tagihan',
         sortable:true,
         resizable:true,
         dataIndex:'sisa_tagihan',
         width:100
         },
         {
         header:'lunas',
         sortable:true,
         resizable:true,
         dataIndex:'lunas',
         width:100
         },
         {
         header:'final',
         sortable:true,
         resizable:true,
         dataIndex:'final',
         width:100
         },
         {
         header:'id_user',
         sortable:true,
         resizable:true,
         dataIndex:'id_user',
         width:100
         },
         {
         header:'parent',
         sortable:true,
         resizable:true,
         dataIndex:'parent',
         width:100
         },
         {
         header:'bruto',
         sortable:true,
         resizable:true,
         dataIndex:'bruto',
         width:100
         },
         {
         header:'disc',
         sortable:true,
         resizable:true,
         dataIndex:'disc',
         width:100
         },
         {
         header:'tgl_tempo',
         sortable:true,
         resizable:true,
         dataIndex:'tgl_tempo',
         width:100
         },
         {
         header:'terima_id',
         sortable:true,
         resizable:true,
         dataIndex:'terima_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBarang.getTotalCount() === 0) {
            jun.rztBarang.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        jun.rztPembelian.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.mode = 'grid';
                    b.params.lunas = Ext.getCmp('statuspembeliangridid').getValue();
                }
            }
        });
        this.store = jun.rztPembelian;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                //{
                //    xtype: 'button',
                //    text: 'Tambah',
                //    ref: '../btnAdd'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Lihat Pembelian',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Status :'
                },
                new jun.comboOpenClose({
                    id: 'statuspembeliangridid',
                    ref: '../status'
                })
                //{
                //    xtype: 'button',
                //    text: 'Hapus',
                //    ref: '../btnDelete'
                //}
            ]
        };
        //this.store.baseParams = {mode: "grid"};
        //this.store.reload();
        //this.store.baseParams = {};
        jun.PembelianGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.status.on('select', function () {this.store.reload();}, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PembelianWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pembelian_id;
        var form = new jun.PembelianWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPembelianDetil.baseParams = {
            pembelian_id: idz
        };
        jun.rztPembelianDetil.load();
        jun.rztPembelianDetil.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Pembelian/delete/id/' + record.json.pembelian_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPembelian.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
