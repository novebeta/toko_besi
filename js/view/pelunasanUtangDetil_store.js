jun.PelunasanUtangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanUtangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanUtangDetilStoreId',
            url: 'PelunasanUtangDetil',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_Utang_detil_id'},
                {name: 'kas_dibayar', type: 'float'},
                {name: 'pelunasan_utang_id'},
                {name: 'pembelian_id'},
                {name: 'tgl'},
                {name: 'no_faktur_beli'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanUtangid").setValue(subtotal);
    }
});
jun.rztPelunasanUtangDetil = new jun.PelunasanUtangDetilstore();
jun.FakturUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FakturUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FakturUtangStoreId',
            url: 'PelunasanUtangDetil/faktur',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembelian_id'},
                {name: 'tgl'},
                {name: 'no_faktur_beli'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztFakturUtang = new jun.FakturUtangstore();

jun.UtangSalesDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.UtangSalesDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UtangSalesDetilstoreId',
            url: 'PelunasanUtangDetil/UtangSales',
            autoLoad: !1,
            autoSave: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penjualan_id'},
                {name: 'tgl'},
                {name: 'no_faktur'},
                {name: 'konsumen_code'},
                {name: 'konsumen_name'},
                {name: 'total', type: 'float'},
                {name: 'sisa', type: 'float'},
                {name: 'cetak', type: 'boolean'}
            ]
        }, cfg));
    }
});
jun.rztUtangPerSales = new jun.UtangSalesDetilstore();