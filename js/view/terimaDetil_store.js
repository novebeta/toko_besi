jun.TerimaDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaDetilStoreId',
            url: 'TerimaDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_detil_id'},
                {name: 'barang_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'pcs', type: 'float'},
                {name: 'terima_id'}
            ]
        }, cfg));
    }
});
jun.rztTerimaDetil = new jun.TerimaDetilstore();
//jun.rztTerimaDetil.load();
