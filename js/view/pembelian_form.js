//TODO : onChangeTempo
jun.PembelianWin = Ext.extend(Ext.Window, {
    title: 'Pembelian',
    modez: 1,
    width: 1015,
    height: 545,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Pembelian',
                labelWidth: 100,
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'no_faktur_beli',
                        hideLabel: false,
                        name: 'no_faktur_beli',
                        ref: '../no_faktur_beli',
                        x: 105,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true,
                        listeners: {
                            render: function (c) {
                                Ext.QuickTips.register({
                                    target: c,
                                    text: 'No faktur pembelian otomatis'
                                });
                            }
                        }
                    },
                    {
                        xtype: "label",
                        text: "No Faktur Suplier:",
                        x: 310,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'no_faktur_supp',
                        name: 'no_faktur_supp',
                        ref: '../no_faktur_supp',
                        allowBlank: false,
                        x: 475,
                        y: 2,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Pemasok:",
                        x: 310,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'orang_id',
                        displayField: 'nama',
                        width: 175,
                        readOnly: true,
                        x: 475,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 310,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "52",
                        width: 175,
                        readOnly: true,
                        x: 475,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl Faktur:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        allowBlank: false,
                        width: 175,
                        x: 105,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tempo (Hari):",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Tempo',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tempo',
                        ref: '../tempo',
                        value: 0,
                        maxLength: 6,
                        width: 175,
                        x: 105,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl Jatuh Tempo:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_tempo',
                        fieldLabel: 'tgl_tempo',
                        name: 'tgl_tempo',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        x: 105,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "No LPB:",
                        x: 725,
                        y: 5
                    },
                    {
                        xtype: "textfield",
                        ref: '../lpb',
                        name: 'no_lpb',
                        width: 175,
                        readOnly: true,
                        x: 805,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "No PO:",
                        x: 725,
                        y: 35
                    },
                    {
                        xtype: "textfield",
                        text: "",
                        ref: '../po',
                        width: 175,
                        readOnly: true,
                        name: 'no_po',
                        x: 805,
                        y: 32
                    },
                    new jun.PembelianDetilGrid({
                        x: 5,
                        y: 125,
                        height: 250,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 5,
                        y: 385
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'sub_total',
                        hideLabel: false,
                        name: 'sub_total',
                        id: 'sub_totalid',
                        value: 0,
                        ref: '../sub_total',
                        readOnly: true,
                        width: 175,
                        x: 105,
                        y: 382
                    },
                    {
                        xtype: "label",
                        text: "Potongan:",
                        x: 5,
                        y: 415
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'disc',
                        hideLabel: false,
                        name: 'disc',
                        id: 'discid',
                        value: 0,
                        ref: '../disc',
                        width: 175,
                        x: 105,
                        y: 412
                    },
                    {
                        xtype: "label",
                        text: "Biaya Bongkar:",
                        x: 5,
                        y: 445
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'biaya_bongkar',
                        hideLabel: false,
                        name: 'biaya_bongkar',
                        id: 'biaya_bongkarid',
                        ref: '../biaya_bongkar',
                        value: 0,
                        width: 175,
                        x: 105,
                        y: 442
                    },
                    {
                        xtype: "label",
                        text: "Biaya Angkut:",
                        x: 310,
                        y: 385
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'biaya_angkut',
                        hideLabel: false,
                        name: 'biaya_angkut',
                        id: 'biaya_angkutid',
                        ref: '../biaya_angkut',
                        value: 0,
                        width: 175,
                        x: 475,
                        y: 382
                    },
                    {
                        xtype: "label",
                        text: "Allowance:",
                        x: 310,
                        y: 415
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'allowance',
                        hideLabel: false,
                        name: 'allowance',
                        id: 'allowanceid',
                        ref: '../allowance',
                        value: 0,
                        width: 175,
                        x: 475,
                        y: 412
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 310,
                        y: 445
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        name: 'total',
                        id: 'totalid',
                        value: 0,
                        ref: '../total',
                        readOnly: true,
                        width: 175,
                        x: 475,
                        y: 442
                    },
                    {
                        xtype: "label",
                        text: "Bank:",
                        x: 725,
                        y: 385
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'id_bank',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'id_bank',
                        valueField: 'id_bank',
                        displayField: 'nama_akun',
                        id: 'id_banktid',
                        ref: '../id_bank',
                        allowBlank: false,
                        width: 175,
                        x: 805,
                        y: 385
                    },
                    {
                        xtype: "label",
                        text: "Uang Muka:",
                        x: 725,
                        y: 415
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'uang_muka',
                        hideLabel: false,
                        name: 'uang_muka',
                        id: 'uang_mukaid',
                        ref: '../uang_muka',
                        value: 0,
                        width: 175,
                        x: 805,
                        y: 412
                    },
                    {
                        xtype: "label",
                        text: "Sisa Tagihan:",
                        x: 725,
                        y: 445
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'sisa_tagihan',
                        hideLabel: false,
                        name: 'sisa_tagihan',
                        id: 'sisa_tagihanid',
                        ref: '../sisa_tagihan',
                        readOnly: true,
                        value: 0,
                        width: 175,
                        x: 805,
                        y: 442
                    },
                    {
                        xtype: "hidden",
                        name: "terima_id"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PembelianWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.biaya_bongkar.on('change', this.updateTotal, this);
        this.allowance.on('change', this.updateTotal, this);
        this.biaya_angkut.on('change', this.updateTotal, this);
        this.disc.on('change', this.updateTotal, this);
        this.uang_muka.on('change', this.updateTotal, this);
        this.tempo.on('change', this.updateTglTempo, this);
        this.tgl.on('change', this.updateTglTempo, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onActivate: function () {
        this.updateTglTempo();
    },
    updateTglTempo: function () {
        var tgl = this.tgl.getValue();
        if (tgl == '') return;
        var tempo = parseFloat(this.tempo.getValue());
        if (tempo == 'NaN') return;
        var tgl_tempo = tgl.add(Date.DAY, tempo);
        this.tgl_tempo.setValue(tgl_tempo);
    },
    onWinClose: function () {
        jun.rztPembelianDetil.removeAll();
    },
    updateTotal: function () {
        jun.rztPembelianDetil.refreshData();
    },
    onSuppChange: function () {
        var supplier_id = this.supplier.getValue();
        var supplier = jun.getSupplier(supplier_id);
        this.alamat.setValue(supplier.data.alamat);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Pembelian/create/';
        Ext.getCmp('form-Pembelian').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPembelianDetil.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPembelian.reload();
                jun.rztTerima.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Pembelian').getForm().reset();
                    this.btnDisabled(false);
                }
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
//    var dt = new Date('10/29/2006').add(Date.DAY, 5);
});