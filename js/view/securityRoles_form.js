jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 700,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Kode Rekening",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "100"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kas dan Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Karyawan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kategori Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "104"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Suplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Konsumen",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "Saldo Awal Rekening",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "200"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Saldo Awal Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "201"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Penjualan Tunai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Penjualan Kredit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Retur Penjualan Tunai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "204"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Retur Penjualan Kredit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "205"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Pelunasan Piutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "206"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Purchase Order",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "207"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Penerimaan Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "208"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Pembelian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "209"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Retur Pembelian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "210"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Pelunasan Utang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Persediaan dan Jurnal Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Pengelolaan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "300"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Selisih",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Jurnal Umum",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Generate Laba Rugi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Closing",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Kas Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Kas Masuk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "500"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kas Keluar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "501"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Mutasi Kas",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "502"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kas Kecil Masuk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "503"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kas Kecil Keluar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "504"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Laporan Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Penjualan Tunai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "600"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Penjualan Kredit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "601"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Penjualan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "602"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Daftar Piutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "603"
                            },
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Piutang Per Salesman",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "604"
                            //},
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Laba Kotor Salesman",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "605"
                            //},
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Biaya Per Salesman",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "606"
                            //},
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Botol Per Salesman",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "607"
                            //},
                            {
                                columnWidth: .33,
                                boxLabel: "Barang Sedang Dipesan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "608"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Barang Mencapai Titik Minimal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "609"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Penerimaan Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "610"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Pembelian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "611"
                            },
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Setoran Kas",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "612"
                            //},
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Nota Debet",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "613"
                            //},
                            {
                                columnWidth: .33,
                                boxLabel: "Utang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "614"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Mutasi Kas",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "615"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Jurnal Umum",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "616"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Pengelolaan Persediaan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "617"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Selisih Persediaan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "618"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Laba Rugi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "619"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Neraca",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "620"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Ledger",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "621"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kartu Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "622"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Mutasi Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "623"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kartu Hutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "624"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Kartu Piutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "625"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cetak Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "626"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cetak Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "627"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                timeOut: 1e3,
                scope: this,
                success: function (a, b) {
                    jun.rztSecurityRoles.reload();
                    jun.sidebar.getRootNode().reload();
                    var c = Ext.decode(b.response.responseText);
                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});