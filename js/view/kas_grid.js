jun.KasGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Kas Masuk",
    id: 'docs-jun.KasGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No BKM',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "xdatefield"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztKas;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kas Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Kas Masuk',
                    ref: '../btnEdit'
                }
            ]
        };
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKaryawan.getTotalCount() === 0) {
            jun.rztKaryawan.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        this.store.baseParams.mode = "grid";
        this.store.reload();
//        this.store.baseParams = {};
        jun.KasGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({
            modez: 0
        });
        form.arus.setValue(0);
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        jun.rztKasDtl.baseParams = {kas_id: idz, mode: 'in'};
        jun.rztKasDtl.load();
        jun.rztKasDtl.baseParams = {};
        var form = new jun.KasWin({modez: 1, id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.onKelompokChange();
        Ext.getCmp('who_idid').setValue(
            this.record.data.tipe_kas == '3' ? this.record.data.who : this.record.data.who_id);
        form.arus.setValue(0);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Kas/delete/id/' + record.json.kas_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKas.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KasGridOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Kas Keluar",
    id: 'docs-jun.KasGridOut',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No BKK',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "xdatefield"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        }
//        {
//            header: 'cara_bayar',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'cara_bayar',
//            width: 100
//        }
        /*
         {
         header:'who',
         sortable:true,
         resizable:true,
         dataIndex:'who',
         width:100
         },
         {
         header:'id_bank',
         sortable:true,
         resizable:true,
         dataIndex:'id_bank',
         width:100
         },
         {
         header:'tipe_kas',
         sortable:true,
         resizable:true,
         dataIndex:'tipe_kas',
         width:100
         },
         {
         header:'who_id',
         sortable:true,
         resizable:true,
         dataIndex:'who_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztKasKeluar;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kas Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Kas Keluar',
                    ref: '../btnEdit'
                }
//                ,
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'button',
//                    text: 'Hapus',
//                    ref: '../btnDelete'
//                }
            ]
        };
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKaryawan.getTotalCount() === 0) {
            jun.rztKaryawan.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        this.store.baseParams.mode = "grid";
        this.store.reload();
//        this.store.baseParams = {};
        jun.KasGridOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({
            modez: 0
        });
        form.arus.setValue(1);
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        jun.rztKasDtl.baseParams = {kas_id: idz, mode: 'out'};
        jun.rztKasDtl.load();
        jun.rztKasDtl.baseParams = {};
        var form = new jun.KasWin({modez: 1, id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.onKelompokChange();
        Ext.getCmp('who_idid').setValue(
            this.record.data.tipe_kas == '3' ? this.record.data.who : this.record.data.who_id);
        form.arus.setValue(1);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Kas/delete/id/' + record.json.kas_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKas.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
jun.KasGridKecil = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Kas Kecil Masuk",
    id: 'docs-jun.KasGridKecil',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No BKM',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "xdatefield"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztKasKecil;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kas Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Kas Masuk',
                    ref: '../btnEdit'
                }
            ]
        };
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKaryawan.getTotalCount() === 0) {
            jun.rztKaryawan.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        this.store.baseParams.mode = "grid";
        this.store.reload();
//        this.store.baseParams = {};
        jun.KasGridKecil.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({
            modez: 0,
            tipe: 'kecil'
        });
        form.arus.setValue(0);
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        jun.rztKasDtl.baseParams = {kas_id: idz, mode: 'in'};
        jun.rztKasDtl.load();
        jun.rztKasDtl.baseParams = {};
        var form = new jun.KasWin({modez: 1, id: idz, tipe: 'kecil'});
        form.formz.getForm().loadRecord(this.record);
        form.onKelompokChange();
        Ext.getCmp('who_idid').setValue(
            this.record.data.tipe_kas == '3' ? this.record.data.who : this.record.data.who_id);
        form.arus.setValue(0);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Kas/delete/id/' + record.json.kas_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKas.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KasGridOutKecil = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Kas Kecil Keluar",
    id: 'docs-jun.KasGridOutKecil',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No BKK',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "xdatefield"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        }
//        {
//            header: 'cara_bayar',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'cara_bayar',
//            width: 100
//        }
        /*
         {
         header:'who',
         sortable:true,
         resizable:true,
         dataIndex:'who',
         width:100
         },
         {
         header:'id_bank',
         sortable:true,
         resizable:true,
         dataIndex:'id_bank',
         width:100
         },
         {
         header:'tipe_kas',
         sortable:true,
         resizable:true,
         dataIndex:'tipe_kas',
         width:100
         },
         {
         header:'who_id',
         sortable:true,
         resizable:true,
         dataIndex:'who_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztKasKeluarKecil;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kas Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Kas Keluar',
                    ref: '../btnEdit'
                }
//                ,
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'button',
//                    text: 'Hapus',
//                    ref: '../btnDelete'
//                }
            ]
        };
        if (jun.rztKonsumenCmp.getTotalCount() === 0) {
            jun.rztKonsumenCmp.load();
        }
        if (jun.rztKaryawan.getTotalCount() === 0) {
            jun.rztKaryawan.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztChartMasterNonKasBank.getTotalCount() === 0) {
            jun.rztChartMasterNonKasBank.load();
        }
        this.store.baseParams.mode = "grid";
        this.store.reload();
//        this.store.baseParams = {};
        jun.KasGridOutKecil.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({
            modez: 0,
            tipe: 'kecil'
        });
        form.arus.setValue(1);
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        jun.rztKasDtl.baseParams = {kas_id: idz, mode: 'out'};
        jun.rztKasDtl.load();
        jun.rztKasDtl.baseParams = {};
        var form = new jun.KasWin({modez: 1, id: idz, tipe: 'kecil'});
        form.formz.getForm().loadRecord(this.record);
        form.onKelompokChange();
        Ext.getCmp('who_idid').setValue(
            this.record.data.tipe_kas == '3' ? this.record.data.who : this.record.data.who_id);
        form.arus.setValue(1);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Kas/delete/id/' + record.json.kas_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKas.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});

