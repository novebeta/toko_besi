jun.BarangWin = Ext.extend(Ext.Window, {
    title: 'Master Barang',
    modez: 1,
    width: 400,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Barang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Barcode',
                        hideLabel: false,
                        //hidden:true,
                        name: 'barcode',
                        id: 'barcodeid',
                        ref: '../barcode',
                        maxLength: 13,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Barang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'barang_name',
                        id: 'barang_nameid',
                        ref: '../barang_name',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kategori',
                        store: jun.rztKategoriBarangCmp,
                        hiddenName: 'kategori_barang_id',
                        valueField: 'kategori_barang_id',
                        forceSelection: true,
                        displayField: 'nama_kategori',
                        ref: '../kategori_barang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Merk',
                        hideLabel: false,
                        //hidden:true,
                        name: 'merk',
                        id: 'merkid',
                        ref: '../merk',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Warna',
                        hideLabel: false,
                        //hidden:true,
                        name: 'warna',
                        id: 'warnaid',
                        ref: '../warna',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'PPN',
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'ppn',
                        ref: '../ppn'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga Cost',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'price_buy',
                        id: 'price_buyid',
                        ref: '../price_buy',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga Jual',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'price',
                        id: 'priceid',
                        ref: '../price',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Disc(%)',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Potongan',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'disc_rp',
                        id: 'disc_rpid',
                        ref: '../disc_rp',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Stok Minimal',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'stok_minimal',
                        id: 'stok_minimalid',
                        ref: '../stok_minimal',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Satuan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'satuan',
                        id: 'satuanid',
                        ref: '../satuan',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    new jun.comboActive({
                        fieldLabel: 'Status',
                        name: 'status',
                        anchor: '100%'
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Simpan',
                hidden: false,
                ref: '../btnSave'
            }, {
                xtype: 'button',
                text: 'Simpan & Tutup',
                ref: '../btnSaveClose'
            }, {
                xtype: 'button',
                text: 'Batal',
                ref: '../btnCancel'
            }]
        };
        jun.BarangWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            //this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Barang/update/id/' + this.id;
        } else {
            urlz = 'Barang/create/';
        }
        Ext.getCmp('form-Barang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarang.reload();
                jun.rztBarangMaster.reload();
                jun.rztBarangLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Barang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});