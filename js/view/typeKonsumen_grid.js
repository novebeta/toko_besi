jun.TypeKonsumenGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Tipe Konsumen",
    id: 'docs-jun.TypeKonsumenGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [{
        header: 'Kode Tipe Konsumen',
        sortable: true,
        resizable: true,
        dataIndex: 'type_konsumen_code',
        width: 100,
        filter: {
            xtype: "textfield"
        }
    }, {
        header: 'Nama Tipe Konsumen',
        sortable: true,
        resizable: true,
        dataIndex: 'type_konsumen_name',
        width: 100,
        filter: {
            xtype: "textfield"
        }
    }, {
        header: 'Status',
        sortable: true,
        resizable: true,
        dataIndex: 'status',
        width: 100,
        filter: {
            xtype: "textfield"
        },
        renderer: jun.renderActive
    }
    ],
    initComponent: function() {
        this.store = jun.rztTypeKonsumen;
        this.bbar = {
            items: [{
                xtype: 'paging',
                store: this.store,
                displayInfo: true,
                pageSize: 20
            }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah Tipe',
                ref: '../btnAdd'
            }, {
                xtype: 'tbseparator'
            }, {
                xtype: 'button',
                text: 'Ubah Tipe',
                ref: '../btnEdit'
            }]
        };
        this.store.baseParams = {
            mode: "grid"
        };
        this.store.reload();
        this.store.baseParams = {};
        jun.TypeKonsumenGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function(sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },
    loadForm: function() {
        var form = new jun.TypeKonsumenWin({
            modez: 0
        });
        form.show();
    },
    loadEditForm: function() {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.type_konsumen_id;
        var form = new jun.TypeKonsumenWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);        
    },
    deleteRec: function() {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function(btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'TypeKonsumen/delete/id/' + record.json.type_konsumen_id,
            method: 'POST',
            success: function(f, a) {
                jun.rztTypeKonsumen.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function(f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})