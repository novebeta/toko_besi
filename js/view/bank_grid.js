jun.BankGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas dan Bank",
    id: 'docs-jun.BankGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header:'Kode Rekening',
            sortable:true,
            resizable:true,
            dataIndex:'account_code',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Kas/Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_akun',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No Rekening Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'no_rekening',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_bank',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Alamat Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat_bank',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Telepon',
            sortable: true,
            resizable: true,
            dataIndex: 'no_tlp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100,
            renderer: jun.renderActive,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztBank;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Kas/Bank',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Kas/Bank',
                    ref: '../btnEdit'
                }
            ]
        };
        if (jun.rztChartMasterComp.getTotalCount() === 0) {
            jun.rztChartMasterComp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BankGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BankWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.id_bank;
        var form = new jun.BankWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Bank/delete/id/' + record.json.id_bank,
            method: 'POST',
            success: function (f, a) {
                jun.rztBank.reload();
                jun.rztBankCmp.reload();
                jun.rztBankLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
