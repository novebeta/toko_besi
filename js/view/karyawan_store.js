jun.Karyawanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Karyawanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KaryawanStoreId',
            url: 'Karyawan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'orang_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'phone'},
                {name: 'bagian'},
                {name: 'type_'}
            ]
        }, cfg));
    }
});
jun.rztKaryawan = new jun.Karyawanstore();
jun.rztKaryawanCmp = new jun.Karyawanstore();
jun.rztKaryawanLib = new jun.Karyawanstore();
//jun.rztKaryawan.load();
