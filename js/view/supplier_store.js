jun.Supplierstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {}, jun.Supplierstore.superclass.constructor.call(this, Ext.apply({
            storeId: "SupplierStoreId",
            url: "Supplier",
            root: "results",
            totalProperty: "total",
            fields: [
                {name: "orang_id"},
                {name: "kode"},
                {name: "nama"},
                {name: "phone"},
                {name: "alamat"}
            ]
        }, cfg))
    }
});
jun.rztSupplierMaster = new jun.Supplierstore;
jun.rztSupplierLib = new jun.Supplierstore;
jun.rztSupplierCmp = new jun.Supplierstore;