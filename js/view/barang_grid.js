jun.BarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Barang",
    id: 'docs-jun.BarangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Barcode',
            sortable: true,
            resizable: true,
            dataIndex: 'barcode',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_name',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Stok Minimal',
            sortable: true,
            resizable: true,
            dataIndex: 'stok_minimal',
            width: 100, align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            filter: {xtype: "numericfield"}
        }
    ],
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztKategoriBarangCmp.getTotalCount() === 0) {
            jun.rztKategoriBarangCmp.load();
        }
        this.store = jun.rztBarangMaster;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Barang',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Barang',
                    ref: '../btnEdit'
                },
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Cetak Barang',
                //    ref: '../btnCetak'
                //},
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportCetakMaster",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "format",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnCetak.on('Click', this.onbtnCetakclick, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onbtnCetakclick:function(){
        Ext.getCmp("form-ReportCetakMaster").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCetakMaster").getForm().url = "Report/CetakBarang";
        this.format.setValue('excel');
        var form = Ext.getCmp('form-ReportCetakMaster').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BarangWin({
            modez: 0
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "" || selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.barang_id;
        var form = new jun.BarangWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Barang/delete/id/' + record.json.barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})