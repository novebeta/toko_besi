jun.KasDtlstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KasDtlstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasDtlStoreId',
            url: 'KasDtl',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kas_dtl_id'},
                {name: 'no_faktur'},
                {name: 'nilai_faktur', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'kas_id'},
                {name: 'account_code'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        Ext.getCmp('totalkas').setValue(this.sum("total"));
    }
});
jun.rztKasDtl = new jun.KasDtlstore();
//jun.rztKasDtl.load();
