jun.PembelianDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PembelianDetil",
    id: 'docs-jun.PembelianDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarcode
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaBarang
        },
        {
            header: 'Sat beli',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 100
        },
        {
            header: 'Jml Beli',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc 1(%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc 2(%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc2',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Pot(Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'pot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Jml Stock',
            sortable: true,
            resizable: true,
            dataIndex: 'pcs',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Sat Stok',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderSatBarang
        }
    ],
    initComponent: function () {
        this.store = jun.rztPembelianDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    text: 'Harga :'
                },
                {
                    xtype: 'numericfield',
                    text: 'Ubah Harga',
                    value: 0,
                    ref: '../price'
                },
                {
                    text: 'Disk 1 :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../disc1',
                    width: 45,
                    minValue: 0,
                    value: 0,
                    maxValue: 100
                },
                {
                    text: '\xA0Disc 2 :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../disc2',
                    width: 45,
                    value: 0,
                    minValue: 0,
                    maxValue: 100
                },
                {
                    text: 'Potongan :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../pot',
                    value: 0,
                    colspan: 3,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.PembelianDetilGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.store.on('update', this.onStoreChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onStoreChange: function () {
        jun.rztPembelianDetil.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PembelianDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Detil Pembelian");
            return;
        }
        if (this.btnEdit.getText() == "Ubah") {
            this.price.setValue(record.data.price);
            this.disc1.setValue(record.data.disc1);
            this.disc2.setValue(record.data.disc2);
            this.pot.setValue(record.data.pot);
            this.btnEdit.setText("Save");
            this.sm.lock();
        } else {
            var price = parseFloat(this.price.getValue());
            var disc1 = parseFloat(this.disc1.getValue());
            var disc2 = parseFloat(this.disc2.getValue());
            var pot = parseInt(this.pot.getValue());
            var nominal = jun.calculateDisc(record.data.jml, price, disc1, disc2, pot);
            var bruto = Math.round(record.data.jml * price);
            var totalpot = bruto - nominal;
            record.set('price', price);
            record.set('disc1', disc1);
            record.set('disc2', disc2);
            record.set('pot', pot);
            record.set('bruto', bruto);
            record.set('totalpot', totalpot);
            record.set('nominal', nominal);
            record.commit();
            this.price.reset();
            this.disc1.reset();
            this.disc2.reset();
            this.pot.reset();
            this.btnEdit.setText("Ubah");
            this.sm.unlock();
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
