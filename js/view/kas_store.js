jun.Kasstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kasstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasStoreId',
            url: 'Kas',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kas_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'total'},
                {name: 'note'},
                {name: 'cara_bayar'},
                {name: 'who'},
                {name: 'id_bank'},
                {name: 'tipe_kas'},
                {name: 'who_id'},
                {name: 'arus'}
            ]
        }, cfg));
    }
});
jun.rztKas = new jun.Kasstore({baseParams: {arus: "masuk"}});
jun.rztKasKeluar = new jun.Kasstore({baseParams: {arus: "keluar"}});
jun.rztKasKecil = new jun.Kasstore({baseParams: {arus: "masukkecil"}});
jun.rztKasKeluarKecil = new jun.Kasstore({baseParams: {arus: "keluarkecil"}});
//jun.rztKas.load();
