jun.ChartMasterWin = Ext.extend(Ext.Window, {
    title: 'Kode Rekening',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ChartMaster',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Rekening',
                        hideLabel: false,
                        //hidden:true,
                        name: 'account_code',
                        id: 'account_codeid',
                        ref: '../account_code',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Rekening',
                        hideLabel: false,
                        //hidden:true,
                        name: 'account_name',
                        id: 'account_nameid',
                        ref: '../account_name',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Header',
                        hideLabel: false,
                        value: 1,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'header',
                        id: 'headerid',
                        ref: '../h'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kategori',
                        store: jun.rztChartMasterHeader,
                        forceSelection: true,
                        hiddenName: 'kategori',
                        valueField: 'account_code',
                        ref: '../katagori',
                        displayField: 'account_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Deskripsi',
                        hideLabel: false,
                        height: 80,
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    new jun.comboActive({fieldLabel: "Status", hideLabel: !1, width: 100, height: 20, name: "status", id: "statusid", ref: "../status", hiddenName: "status"})

                /**/
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ChartMasterWin.superclass.initComponent.call(this);
//        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.katagori.on('expand', this.onFocuskatagori, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
//    onFocuskatagori: function() {
//        jun.rztChartMasterComp.FilterDataHeader();
//    },
//    onWinClose: function() {
//        jun.rztChartMasterComp.clearFilter();
//    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'ChartMaster/update/id/' + this.id;
        } else {
            urlz = 'ChartMaster/create/';
        }
        Ext.getCmp('form-ChartMaster').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztChartMaster.reload();
                jun.rztChartMasterLib.reload();
                jun.rztChartMasterComp.reload();
                jun.rztChartMasterNonKasBank.reload();
                jun.rztChartMasterHeader.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ChartMaster').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});