jun.KonsumenGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Konsumen",
    id: 'docs-jun.KonsumenGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Konsumen',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Konsumen',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Phone',
            sortable: true,
            resizable: true,
            dataIndex: 'phone',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonsumenMaster;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Konsumen',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Konsumen',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Cetak Konsumen',
                    ref: '../btnCetak'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.KonsumenGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnCetak.on('Click', this.onbtnCetak, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KonsumenWin({
            modez: 0
        });
        form.show();
    },
    onbtnCetak: function () {
        var plugins = this.headerFilters.filterFields;
        var form = new jun.LapMasterKonsumen({
            modez: 0
        });
        form.show();
        form.konsumen_code.setValue(plugins.konsumen_code.getValue());
        form.konsumen_name.setValue(plugins.konsumen_name.getValue());
        form.phone.setValue(plugins.phone.getValue());
        form.hp.setValue(plugins.hp.getValue());
        form.hp2.setValue(plugins.hp2.getValue());
        form.tempo.setValue(plugins.tempo.getValue());
        form.status.setValue(plugins.status.getValue());
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == "") {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.orang_id;
        var form = new jun.KonsumenWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Konsumen/delete/id/' + record.json.orang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKonsumenCmp.reload();
                jun.rztKonsumenLib.reload();
                jun.rztKonsumenMaster.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});