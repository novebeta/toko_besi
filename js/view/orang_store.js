jun.Orangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Orangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OrangStoreId',
            url: 'Orang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'orang_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'phone'},
                {name: 'bagian'},
                {name: 'type_'},
            ]
        }, cfg));
    }
});
jun.rztOrang = new jun.Orangstore();
//jun.rztOrang.load();
