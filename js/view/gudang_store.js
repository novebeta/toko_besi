jun.Gudangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Gudangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GudangStoreId',
            url: 'Gudang',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'gudang_id'},
{name:'gudang_code'},
{name:'gudang_name'},
{name:'delivery_address'},
{name:'phone'},
{name:'phone2'},
{name:'fax'},
{name:'email'},
{name:'contact'},
{name:'status'}
            ]
        }, cfg));
    }, FilterData: function() {
        this.filter([{property: "status", value: "1"}])
    }
});
jun.rztGudang = new jun.Gudangstore();
//jun.rztGudang.load();
