<?php
require_once('settings.php');
/**
 *DIRECTORY_SEPARATOR
 */
define('DS', DIRECTORY_SEPARATOR);
define('SUPPLIER', 0);
define('KONSUMEN', 1);
define('KARYAWAN', 2);
/**
 *
 */
define('KAS_MASUK', 0);
/**
 *KAS_KELUAR
 */
define('KAS_KELUAR', 1);
/**
 *PENJUALAN
 */
define('PENJUALAN', 2);
/**
 *
 */
define('DOS', 3);
/**
 *
 */
define('RETURJUAL', 4);
/**
 *
 */
define('PELUNASANPIUTANG', 5);
/**
 *
 */
define('BIAYASALES', 6);
/**
 *
 */
define('DOF', 7);
/**
 *
 */
define('RETURBOTOL', 8);
/**
 *
 */
define('SETORKASSALES', 9);
define('PENJUALAN_KREDIT', 10);
define('RETUR_JUAL_KREDIT', 11);
/**
 *
 */
define('BOTOL', 200);
/**
 *
 */
define('KELOLASTOK', 300);
/**
 *
 */
define('SELISIHSTOK', 310);
define('PO', 400);
define('TERIMABARANG', 401);
define('PEMBELIAN', 402);
define('RETURPEMBELIAN', 403);
define('NOTADEBET', 404);
define('PELUNASANUTANG', 405);
/**
 *
 */
define('BANKTRANSFER', 14);
/**
 *
 */
defined('VOID') or define('VOID', 15);
/**
 *
 */
define('SALDO_AWAL', 16);
define('DIR_BACKUP', 'D:\xampp\htdocs\toko_besi\backup');
define('MYSQLDUMP', 'D:\xampp\mysql\bin\mysqldump.exe');
/**
 *
 */
define('JURNAL_UMUM', 18);
define('LABARUGI', 13);
/**
 *Extjs form pelunasan piutang
 */
define('DEFKASPELPIU', 11);
define('CHARLENGTHRECEIPT', 80);

define('SALES_TANPA_KIRIM', 0);
define('SALES_BELUM_KIRIM', 1);
define('SALES_SUDAH_KIRIM', 2);

/**
 * @global array $systypes_array
 */
global $systypes_array;
$systypes_array = array(
    KAS_MASUK => "Kas Masuk",
    KAS_KELUAR => "Kas Keluar",
    PENJUALAN => "Penjualan",
    DOS => "DO Sementara",
    RETURJUAL => "Retur Penjualan",
    PELUNASANPIUTANG => "Pelunasan Piutang",
    BIAYASALES => "Biaya Sales",
    BOTOL => "Botol",
    BANKTRANSFER => "Mutasi Antar Kas/Bank",
    VOID => "Void Dokumen",
    SALDO_AWAL => "Saldo Awal",
    JURNAL_UMUM => 'Jurnal Umum',
    KELOLASTOK => 'Pengelolaan Persediaan',
    SELISIHSTOK => 'Selisih Persediaan',
    PO => 'Purchase Order',
    TERIMABARANG => 'Penerimaan Barang',
    NOTADEBET => 'Nota Debet',
    PEMBELIAN => 'Pembelian',
    RETURPEMBELIAN => 'Retur Pembelian',
    PELUNASANUTANG => 'Pelunasan Utang',
    SETORKASSALES => 'Setor Kas Sales'
);
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

/**
 * This is the shortcut to html_entity_decode
 */
function h_e_d($text, $flags = ENT_QUOTES, $encoding = 'UTF-8')
{
    return html_entity_decode($text, $flags, $encoding);
}

/**
 * @return CDbTransaction
 */
function dbTrans()
{
    return Yii::app()->db->beginTransaction();
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

/**
 * @param $string
 * @return string
 */
function Encrypt($string)
{
    return base64_encode(Yii::app()->getSecurityManager()->encrypt($string));
}

/**
 * @param $string
 * @return string
 */
function Decrypt($string)
{
    return Yii::app()->getSecurityManager()->decrypt(base64_decode($string));
}

/**
 * @param int $length
 * @return string
 */
function generatePassword($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

/**
 * @param $date
 * @param $format
 * @return string
 */
function date2longperiode($date, $format)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->format($format, $timestamp);
}

/**
 * @param $month
 * @param $year
 * @return array
 */
function period2date($month, $year)
{
    $timestamp = DateTime::createFromFormat('d/m/Y', "01/$month/$year");
    $start = $timestamp->format('Y-m-d');
    $end = $timestamp->format('Y-m-t');
    return array('start' => $start, 'end' => $end);
}

/**
 * @param $number
 * @return mixed
 */
function get_number($number)
{
    if ($number == null) {
        return null;
    }
    return str_replace(",", "", $number);
}

/**
 * @param $number
 * @return int
 */
function is_angka($number)
{
    if ($number == null) {
        return false;
    }
    if (is_array($number)) {
        return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number);
}

/**
 * @param $date
 * @return string
 */
function sql2date($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

/**
 * @param $date
 * @return string
 */
function date2sql($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, $format);
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', $timestamp);
}

/**
 * @param $date
 * @return string
 */
function sql2long_date($date)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->formatDateTime($timestamp, 'long', false);
}

/**
 * @return string
 */
function get_date_tomorrow()
{
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', time() + (1 * 24 * 60 * 60));
}

/**
 * @return string
 */
function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

/**
 * @param string $format
 * @return string
 */
function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

/**
 * @param string $formatDate
 * @return string
 */
function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

/**
 * @param $value
 * @param int $decimal
 * @return string
 */
function percent_format($value, $decimal = 0)
{
    return number_format($value, $decimal) . '%';
}

/**
 * @param $value
 * @param int $decimal
 * @return string
 */
function curr_format($value, $decimal = 0)
{
    return "Rp" . number_format($value * 100, $decimal);
}

/**
 * @param $value
 * @param int $decimal
 * @return string
 */
function acc_format($value, $decimal = 0)
{
    $normalize = $value < 0 ? -$value : $value;
    $print = number_format($normalize, $decimal);
    return $value < 0 ? "($print)" : $print;
}

function format_number_report($num, $digit = 0)
{
    if(!is_angka($num)){
        return $num;
    }
    return (isset($_POST['format']) && $_POST['format'] == 'excel') ? $num : number_format($num, $digit);
}