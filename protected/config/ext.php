<?php
return CMap::mergeArray(
    require(dirname(__FILE__) . '/database.php'),
    array(
        'params' => array(
            'corp' => array(
                'nama' => '',
                'address' => array(
                    '',
                    ''
                ),
                'faktur_footer'=> array(
                    'Barang yang sudah dibeli tidak dapat ditukar atau dikembalikan',
                    'Pembayaran selain tunai dianggap lunas pada saat dana cair.'
                ),
            ),
        ),
    )
);