<?php
return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:host=127.0.0.1;dbname=toko_besi;port=3306',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => 'root',
                'tablePrefix' => 'psn_',
                'charset' => 'utf8',
            ),
        ),
    )
);
