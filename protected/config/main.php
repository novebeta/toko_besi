<?php
$basePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..';
//YiiBase::setPathOfAlias('acl', $basePath.DIRECTORY_SEPARATOR.'modules'.
//    DIRECTORY_SEPARATOR.'acl/');
return array(
    'basePath' => $basePath,
    'name' => '',
    'preload' => array('log'),
    'sourceLanguage' => 'id',
    'import' => array(
        'application.models.*',
        'ext.giix-components.*',
        'application.extensions.PHPExcel',
        'application.vendors.*',
        'ext.yiireport.*',
        'application.components.*',
//        'acl.components.*',
//        'acl.models.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array('ext.giix-core',
            ),
        ),      
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
        'user' => array(
            'loginUrl' => array('site/login'),
            'allowAutoLogin' => true,),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
//                '/' => 'site/index',
//                '<action:(contact|login|logout)>/*' => 'site/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'session' => array(
            'sessionName' => 'primasari',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'psn_session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 10800
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'filter' => 'CLogFilter',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 10
                ),
            ),
        ),
//        'excel' => array(
//            'class' => 'application.extensions.PHPExcel',
//        ),
        'errorHandler' => array(
            'errorAction' => '',),
        
//        'ePdf' => array(
//            'class' => 'ext.yii-pdf.EYiiPdf',
//            'params' => array(
//                'mpdf' => array(
//                    'librarySourcePath' => 'application.vendors.mpdf.*',
//                    'constants' => array(
//                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
//                    ),
//                    'class' => 'mPDF',
//                ),
//                'HTML2PDF' => array(
//                    'librarySourcePath' => 'application.vendors.html2pdf.*',
//                    'classFile' => 'html2pdf.class.php',
//                )
//            ),
//        ),
        
        
    ),
    'params' => array(
        'adminEmail' => 'webmaster@example.com',
        'system_title' => 'PT. TOKO BESI',
        'system_subtitle' => 'Jl Kebon Agung No 7 Sendang Adi Mlati, Sleman - DI Yogyakarta',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
);
