<?php
class EpsonStatic
{
    static function Init()
    {
        return '\x1B\x40';
    }
    static function ResetStyles()
    {
        return '\x1B\x21\x00';
    }
    static function MakeStyle($font, $bold = false, $underline = false, $double_width = false, $double_height = false)
    {
        $hex = 0;
        if ($font) {
            $hex += $font;
        }
        if ($bold) {
            $hex += 8;
        }
        if ($underline) {
            $hex += 80;
        }
        if ($double_width) {
            $hex += 20;
        }
        if ($double_height) {
            $hex += 10;
        }
        if ($hex < 10) {
            $hex = '0' . $hex;
        }
        return '\x1B\x21\x' . $hex;
    }
    static function Cut()
    {
        return '\x1D\x56\x41';
    }
}