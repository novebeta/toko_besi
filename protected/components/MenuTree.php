<?php
class MenuTree
{
    var $security_role;
    var $menu_users = array(
        'text' => 'User Manajement',
        'id' => 'jun.UsersGrid',
        'leaf' => true
    );
    var $security = array(
        'text' => 'Security Roles',
        'id' => 'jun.SecurityRolesGrid',
        'leaf' => true
    );
    function __construct($id)
    {
        //$this->security_role_id = $id;
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function get_menu_master()
    {
        $root = array();
        $child = array();
        if (in_array(100, $this->security_role)) {
            $child[] = array(
                'text' => 'Kode Rekening',
                'id' => 'jun.ChartMasterGrid',
                'leaf' => true
            );
        }
        if (in_array(101, $this->security_role)) {
            $child[] = array(
                'text' => 'Kas dan Bank',
                'id' => 'jun.BankGrid',
                'leaf' => true
            );
        }
        if (in_array(102, $this->security_role)) {
            $child[] = array(
                'text' => 'Karyawan',
                'id' => 'jun.KaryawanGrid',
                'leaf' => true
            );
        }
        if (in_array(103, $this->security_role)) {
            $child[] = array(
                'text' => 'Kategori Barang',
                'id' => 'jun.KategoriBarangGrid',
                'leaf' => true
            );
        }
        if (in_array(104, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang',
                'id' => 'jun.BarangGrid',
                'leaf' => true
            );
        }
        if (in_array(105, $this->security_role)) {
            $child[] = array(
                'text' => 'Suplier',
                'id' => 'jun.SupplierGrid',
                'leaf' => true
            );
        }
        if (in_array(106, $this->security_role)) {
            $child[] = array(
                'text' => 'Konsumen',
                'id' => 'jun.KonsumenGrid',
                'leaf' => true
            );
        }
        if (!empty($child)) {
            $root = array(
                'text' => 'Master',
                'expanded' => false,
                'children' => $child
            );
        }
        return $root;
    }
    function get_menu_transaksi()
    {
        $child = array();
        $root = array();
        if (in_array(200, $this->security_role)) {
            $child[] = array(
                'text' => 'Saldo Awal Rekening',
                'id' => 'jun.SaldoAwalWin',
                'leaf' => true
            );
        }
        if (in_array(201, $this->security_role)) {
            $child[] = array(
                'text' => 'Saldo Awal Barang',
                'id' => 'jun.SaldoAwalBarangWin',
                'leaf' => true
            );
        }
        if (in_array(202, $this->security_role)) {
            $child[] = array(
                'text' => 'Penjualan Tunai',
                'id' => 'jun.PenjualanGrid',
                'leaf' => true
            );
        }
        if (in_array(203, $this->security_role)) {
            $child[] = array(
                'text' => 'Penjualan Kredit',
                'id' => 'jun.PenjualanKreditGrid',
                'leaf' => true
            );
        }
        if (in_array(204, $this->security_role)) {
            $child[] = array(
                'text' => 'Retur Penjualan Tunai',
                'id' => 'jun.ReturPenjualanGrid',
                'leaf' => true
            );
        }
        if (in_array(205, $this->security_role)) {
            $child[] = array(
                'text' => 'Retur Penjualan Kredit',
                'id' => 'jun.ReturPenjualanKreditGrid',
                'leaf' => true
            );
        }
        if (in_array(206, $this->security_role)) {
            $child[] = array(
                'text' => 'Pelunasan Piutang',
                'id' => 'jun.PelunasanPiutangGrid',
                'leaf' => true
            );
        }
        if (in_array(207, $this->security_role)) {
            $child[] = array(
                'text' => 'Purchase Order',
                'id' => 'jun.PoGrid',
                'leaf' => true
            );
        }
        if (in_array(208, $this->security_role)) {
            $child[] = array(
                'text' => 'Penerimaan Barang',
                'id' => 'jun.TerimaGrid',
                'leaf' => true
            );
        }
        if (in_array(209, $this->security_role)) {
            $child[] = array(
                'text' => 'ReportPembelian',
                'id' => 'jun.PembelianGrid',
                'leaf' => true
            );
        }
        if (in_array(210, $this->security_role)) {
            $child[] = array(
                'text' => 'Retur Pembelian',
                'id' => 'jun.ReturPembelianGrid',
                'leaf' => true
            );
        }
        if (in_array(211, $this->security_role)) {
            $child[] = array(
                'text' => 'Pelunasan Utang',
                'id' => 'jun.PelunasanUtangGrid',
                'leaf' => true
            );
        }
        if (!empty($child)) {
            $root = array(
                'text' => 'Transaksi',
                'expanded' => false,
                'children' => $child
            );
        }
        return $root;
    }
    function get_menu_persediaan()
    {
        $child = array();
        $childPersediaan = array();
        $childJurnal = array();
        if (in_array(300, $this->security_role)) {
            $childPersediaan[] = array(
                'text' => 'Pengelolaan',
                'id' => 'jun.KelolaStokGrid',
                'leaf' => true
            );
        }
        if (in_array(301, $this->security_role)) {
            $childPersediaan[] = array(
                'text' => 'Selisih',
                'id' => 'jun.SelisihStokGrid',
                'leaf' => true
            );
        }
        if (!empty($childPersediaan)) {
            $child = array(
                'text' => 'Persediaan',
                'expanded' => false,
                'children' => $childPersediaan
            );
        }
        return $child;
    }
    function get_menu_jurnal()
    {
        $child = array();
        $childJurnal = array();
        if (in_array(302, $this->security_role)) {
            $childJurnal[] = array(
                'text' => 'Jurnal Umum',
                'id' => 'jun.JurnalUmum',
                'leaf' => true
            );
        }
        if (in_array(303, $this->security_role)) {
            $childJurnal[] = array(
                'text' => 'Generate Laba Rugi',
                'id' => 'jun.GenerateLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(304, $this->security_role)) {
            $childJurnal[] = array(
                'text' => 'Closing',
                'id' => 'jun.ClosingYear',
                'leaf' => true
            );
        }
        if (!empty($childJurnal)) {
            $child = array(
                'text' => 'Jurnal',
                'expanded' => false,
                'children' => $childJurnal
            );
        }
        return $child;
    }
    function get_menu_kas()
    {
        $childKas = array();
        $child = array();
        if (in_array(500, $this->security_role)) {
            $childKas[] = array(
                'text' => 'Kas Masuk',
                'id' => 'jun.KasGrid',
                'leaf' => true
            );
        }
        if (in_array(501, $this->security_role)) {
            $childKas[] = array(
                'text' => 'Kas Keluar',
                'id' => 'jun.KasGridOut',
                'leaf' => true
            );
        }
        if (in_array(502, $this->security_role)) {
            $childKas[] = array(
                'text' => 'Mutasi Kas',
                'id' => 'jun.TranferBankWin',
                'leaf' => true
            );
        }
        if (in_array(503, $this->security_role)) {
            $childKas[] = array(
                'text' => 'Kas Kecil Masuk',
                'id' => 'jun.KasGridKecil',
                'leaf' => true
            );
        }
        if (in_array(504, $this->security_role)) {
            $childKas[] = array(
                'text' => 'Kas Kecil Keluar',
                'id' => 'jun.KasGridOutKecil',
                'leaf' => true
            );
        }
        if (!empty($childKas)) {
            $child = array(
                'text' => 'Kas',
                'expanded' => false,
                'children' => $childKas
            );
        }
        return $child;
    }
    function get_laporan()
    {
        $childLap = array();
        $child = array();
        if (in_array(600, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Penjualan Tunai',
                'id' => 'jun.LapPenjualanTunai',
                'leaf' => true
            );
        }

        if (in_array(601, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Penjualan Kredit',
                'id' => 'jun.LapPenjualanKredit',
                'leaf' => true
            );
        }

        if (in_array(602, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Penjualan',
                'id' => 'jun.LapPenjualan',
                'leaf' => true
            );
        }


        if (in_array(603, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Daftar Piutang',
                'id' => 'jun.LapDaftarPiutang',
                'leaf' => true
            );
        }
//        if (in_array(604, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Piutang Per Salesman',
//                'id' => 'jun.LapPiutangSales',
//                'leaf' => true
//            );
//        }
//        if (in_array(605, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Laba Kotor Salesman',
//                'id' => 'jun.ReportLabaKotorSales',
//                'leaf' => true
//            );
//        }
//        if (in_array(606, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Biaya Per Salesman',
//                'id' => 'jun.LapBiayaSales',
//                'leaf' => true
//            );
//        }
//        if (in_array(607, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Botol Per Salesman',
//                'id' => 'jun.LapBotolSales',
//                'leaf' => true
//            );
//        }
        if (in_array(608, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Barang Sedang Dipesan',
                'id' => 'jun.ReportBarangSedangPesan',
                'leaf' => true
            );
        }
        if (in_array(609, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Barang Mencapai Titik Minimal',
                'id' => 'jun.ReportBarangHabis',
                'leaf' => true
            );
        }
        if (in_array(610, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Penerimaan Barang',
                'id' => 'jun.ReportTerimaBarang',
                'leaf' => true
            );
        }
        if (in_array(611, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Pembelian',
                'id' => 'jun.ReportPembelian',
                'leaf' => true
            );
        }
//        if (in_array(612, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Setoran Kas',
//                'id' => 'jun.ReportSetorKasPeriode',
//                'leaf' => true
//            );
//        }
//        if (in_array(613, $this->security_role)) {
//            $childLap[] = array(
//                'text' => 'Nota Debet',
//                'id' => 'jun.ReportNotaDebet',
//                'leaf' => true
//            );
//        }
        if (in_array(614, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Utang',
                'id' => 'jun.ReportUtang',
                'leaf' => true
            );
        }
        if (in_array(615, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Mutasi Kas',
                'id' => 'jun.ReportMutasiKas',
                'leaf' => true
            );
        }
        if (in_array(616, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Jurnal Umum',
                'id' => 'jun.ReportJurnalUmum',
                'leaf' => true
            );
        }
        if (in_array(617, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Pengelolaan Persediaan',
                'id' => 'jun.ReportKelolaStok',
                'leaf' => true
            );
        }
        if (in_array(618, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Selisih Persediaan',
                'id' => 'jun.ReportSelisihStok',
                'leaf' => true
            );
        }

        if (in_array(619, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Laba Kotor',
                'id' => 'jun.ReportLabaKotor',
                'leaf' => true
            );
        }

        if (in_array(619, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Laba Rugi',
                'id' => 'jun.ReportLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(620, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Neraca',
                'id' => 'jun.Neraca',
                'leaf' => true
            );
        }
        if (in_array(621, $this->security_role)) {
            $childLap[] = array(
                'text' => 'General Ledger',
                'id' => 'jun.ReportGeneralLedger',
                'leaf' => true
            );
        }
        if (in_array(622, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Kartu Stok',
                'id' => 'jun.ReportInventoryCard',
                'leaf' => true
            );
        }
        if (in_array(623, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Mutasi Stok',
                'id' => 'jun.ReportInventoryMovements',
                'leaf' => true
            );
        }
        if (in_array(624, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Kartu Hutang',
                'id' => 'jun.ReportKartuHutang',
                'leaf' => true
            );
        }
        if (in_array(625, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Kartu Piutang',
                'id' => 'jun.ReportKartuPiutang',
                'leaf' => true
            );
        }
        if (in_array(626, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Cetak Barang',
                'id' => 'jun.ReportCetakBarang',
                'leaf' => true
            );
        }
        if (in_array(627, $this->security_role)) {
            $childLap[] = array(
                'text' => 'Cetak Supplier',
                'id' => 'jun.ReportSupplier',
                'leaf' => true
            );
        }
        if (!empty($childLap)) {
            $child = array(
                'text' => 'Laporan',
                'expanded' => false,
                'children' => $childLap
            );
        }
        return $child;
    }
    function get_menu_administration()
    {
        $child = array();
        $root = array();
        if (in_array(400, $this->security_role)) {
            $child[] = array(
                'text' => 'User Management',
                'id' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'Security Roles',
                'id' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role)) {
            $child[] = array(
                'text' => 'Backup / Restore',
                'id' => 'jun.BackupRestoreWin',
                'leaf' => true
            );
        }
        if (!empty($child)) {
            $root = array(
                'text' => 'Administrasi',
                'expanded' => false,
                'children' => $child
            );
        }
        return $root;
    }
    public function get_menu()
    {
        $data = array();
        $master_arr = self::get_menu_master();
        $trans_arr = self::get_menu_transaksi();
        $persediaan_arr = self::get_menu_persediaan();
        $jurnal_arr = self::get_menu_jurnal();
        $menu_kas_arr = self::get_menu_kas();
        $lap_arr = self::get_laporan();
        $administration_arr = self::get_menu_administration();
        if (!empty($master_arr)) {
            $data[] = $master_arr;
        }
        if (!empty($trans_arr)) {
            $data[] = $trans_arr;
        }
        if (!empty($persediaan_arr)) {
            $data[] = $persediaan_arr;
        }
        if (!empty($jurnal_arr)) {
            $data[] = $jurnal_arr;
        }
        if (!empty($menu_kas_arr)) {
            $data[] = $menu_kas_arr;
        }
        if (!empty($lap_arr)) {
            $data[] = $lap_arr;
        }
        if (!empty($administration_arr)) {
            $data[] = $administration_arr;
        }
        $username = Yii::app()->user->name;
        if (in_array(000, $this->security_role)) {
            $data[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(001, $this->security_role)) {
            $data[] = array(
                'text' => "Logout ($username)",
                'id' => 'logout',
                'leaf' => true
            );
        }
        return CJSON::encode($data);
    }
}
