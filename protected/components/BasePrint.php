<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/12/14
 * Time: 11:30 AM
 */
class BasePrint
{
    function fillWithChar($char, $l = CHARLENGTHRECEIPT)
    {
//        $res = "";
//        for ($i = 0; $i < $l; $i++) {
//            $res .= $char;
//        }
        return str_repeat($char, $l);
//        return $res;
    }
    function setCenter($msg, $l = CHARLENGTHRECEIPT)
    {
        $lmsg = strlen($msg);
        if ($lmsg > $l) {
            return substr($msg, 0, $l);
        }
        return str_pad($msg, $l, " ", STR_PAD_BOTH);
    }
    function addHeaderSales($msg1, $msg2, $l = 15)
    {
        $lmsg1 = strlen($msg1);
        if ($lmsg1 > $l) {
            $msg1 = substr($msg1, 0, $l);
            $lmsg1 = 15;
        }
        $res = $msg1 . self::fillWithChar(" ", $l - $lmsg1);
        $res .= ":" . $msg2;
        return $res;
    }
    function addLeftRight($msg1, $msg2, $l)
    {
        $lmsg1 = strlen($msg1);
        $lmsg2 = strlen($msg2);
        $sisa = $l - ($lmsg1 + $lmsg2);
        return $msg1 . self::fillWithChar(" ", $sisa) . $msg2;
    }
    function addLeftRightWithMax($msg1, $msg2, $msg1_max, $msg2_max, $space = "")
    {
        $lmsg1 = substr($msg1, 0, $msg1_max);
        $lmsg2 = substr($msg2, 0, $msg2_max);
        return str_pad($lmsg1, $msg1_max, " ", STR_PAD_RIGHT) . $space . str_pad($lmsg2, $msg2_max, " ", STR_PAD_LEFT);
    }
    function addDetailsItemSales($no, $kode, $nama, $sat, $jml, $harga, $pot, $nom)
    {
        $details =
            substr(str_pad($no, 2, " ", STR_PAD_LEFT), 0, 2) . " " .
            substr(str_pad($kode, 13, " ", STR_PAD_RIGHT), 0, 13) . " " .
            substr(str_pad($nama, 29, " ", STR_PAD_RIGHT), 0, 29) . " " .
            substr(str_pad($sat, 3, " ", STR_PAD_RIGHT), 0, 3) . " " .
            substr(str_pad(number_format($jml, 2), 9, " ", STR_PAD_LEFT), 0, 9) . " " .
            substr(str_pad(number_format($harga, 0), 10, " ", STR_PAD_LEFT), 0, 10) . " " .
            substr(str_pad(number_format($pot, 0), 10, " ", STR_PAD_LEFT), 0, 10) . " " .
            substr(str_pad(number_format($nom, 0), 10, " ", STR_PAD_LEFT), 0, 10);
        return $details;
    }
    function addDetailsItemDO($no, $kode, $nama, $sat, $jml)
    {
        $details =
            substr(str_pad($no, 2, " ", STR_PAD_LEFT), 0, 2) . " " .
            substr(str_pad($kode, 13, " ", STR_PAD_RIGHT), 0, 13) . " " .
            substr(str_pad($nama, 29, " ", STR_PAD_RIGHT), 0, 29) . " " .
            substr(str_pad($sat, 3, " ", STR_PAD_RIGHT), 0, 3) . " " .
            substr(str_pad(number_format($jml, 2), 9, " ", STR_PAD_LEFT), 0, 9) . " ";
        return $details;
    }
    function addItemCodeReceipt($itemKode, $qty, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 39
        $mitemKode = 34;
        $lqty = strlen($qty); //max 7
        $mqty = 7;
        $lsubtotal = strlen($subtotal); //max 17
        $msubtotal = 17;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
//            $litemKode = $mitemKode;
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mitemKode);
//            $lqty = $mqty;
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
//            $lsubtotal = $msubtotal;
        }
        $msg1 = self::addLeftRight($itemKode, $qty, $mitemKode + $mqty);
        return self::addLeftRight($msg1, $subtotal, $l);
    }
    function addItemNameReceipt($itemName, $l, $prefix = 5)
    {
        $itemName = self::fillWithChar(" ", $prefix) . $itemName;
        if (strlen($itemName) > $l) {
            return substr($itemName, 0, $l);
        }
        return $itemName;
    }
    function addItemDiscReceipt($disc, $subtotal, $prefix = 6, $l = CHARLENGTHRECEIPT)
    {
        $lbldisc = self::fillWithChar(" ", $prefix) . "Disc: $disc";
        return self::addLeftRight($lbldisc, $subtotal, $l);
    }
} 