<?php
/**
 *Rekening Kas Sementara
 */
define('KASSEMENTARA', '1011120');
/**
 *Rekening Piutang Dagang
 */
define('PIUTANGDAGANG', '1011310');
define('UTANGDAGANG', '1021100');
/**
 *Rekening PPN
 */
define('PPN', '1021300');
define('BONGKAR', '1054000');        
define('ONGKIRJUAL', '1044000');
define('ANGKUT', '1053000');
define('ALLOWANCE', '1052000');
define('POTONGAN_PEMBELIAN', '1055000');
define('KATEGORIPENDAPATAN', '1040000');
define('KATEGORIHPP', '1050000');
define('KATEGORIBEBANADMINISTRASI', '1061000');
define('KATEGORIBEBANPEMASARAN', '1062000');
define('KATEGORIPENDAPATANLAIN', '1070000');
define('KATEGORIBEBANLAIN', '1080000');
define('PAJAKPENGHASILAN', '1091000');
/**
 *Rekening Penjualan Sales
 */
define('PENJUALANSALES', '1041000');
/**
 *Rekening Retur Penjualan Sales
 */
define('RETURPENJUALANSALES', '1042000');
/**
 *Rekening Retur Penjualan Sales
 */
define('POTONGPENJUALANSALES', '1043000');
/**
 *Rekening Retur Penjualan Sales
 */
define('PENJUALANAGEN', '1041000');
/**
 *Rekening Retur Penjualan Sales
 */
define('RETURPENJUALANAGEN', '1042000');
/**
 *Rekening Retur Penjualan Sales
 */
define('POTONGPENJUALANAGEN', '1043000');
/**
 *Rekening Persedian Botol
 */
define('PIUTANGBOTOL', '1011310');
/**
 *Rekening Selisih Botol
 */
define('PENDAPATANLAIN', '1071000');
/**
 *Rekening Beban Kelilingan
 *Sample : Makan
 */
define('BEBANKELILINGAN', '1063910');
/**
 *Rekening Beban Transportasi
 * Sample : BBM, Parkir
 */
define('BEBANTRANSPORTASI', '1062120');
/**
 *Rekening Kas Besar
 */
define('KASBESAR', '1011120');
/**
 *Rekening Persedian Barang Dagang
 */
define('PERSEDIANBARANGDAGANG', '1011510');
/**
 *Rekening Selisih Persediaan
 */
define('SELISIHPERSEDIAAN', '1059000');
/**
 *Rekening HPP
 */
define('HPP', '1051000');
define('GUDANG_PST', 1);
define('GUDANG_SMT', 2);
define('GRUPKAS', '1011100');
define('GRUPBANK', '1011200');
define('COA_LABA_RUGI', '1033200');
define('COA_LABA_DITAHAN', '1032000');