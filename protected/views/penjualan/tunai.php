<?
/** @var $jual Penjualan */
/** @var $konsumen Konsumen */
$detail_count = 1;
$konsumen = Konsumen::model()->findByPk($jual->konsumen_id);
$nama = $alamat = $phone = '';
if ($konsumen == null) {
    $nama = $jual->nama;
    $alamat = $jual->alamat;
} else {
    $nama = $konsumen->nama;
    $alamat = $konsumen->alamat;
    $phone = $konsumen->phone;
}
$almt_arr = explode('<br>', wordwrap(str_replace(array("\r", "\n"), " ", $alamat), 29, '<br>', true));
$almt1 = array_key_exists(0, $almt_arr) ? $almt_arr[0] : "";
$almt2 = array_key_exists(1, $almt_arr) ? $almt_arr[1] : "";
$almt3 = array_key_exists(2, $almt_arr) ? $almt_arr[2] : "";
$p = new BasePrint();
$format =
    chr(27) . chr(64) . //init
    chr(27) . chr(67) . chr(33) . // page lenght 33 lines
    chr(27) . chr(80) . //10cpi
    chr(27) . chr(33) . chr(16) . //double strike more bold
    $nama_faktur . "\r\n" .
    chr(27) . chr(33) . chr(32) . //double width
    app()->params['corp']['nama'] . "\r\n" .
    chr(27) . chr(33) . chr(0) . // reset font style
    app()->params['corp']['address'][0] . "\r\n" .
    app()->params['corp']['address'][1] . "\r\n" .
    $p->addLeftRight("No. Faktur", $jual->doc_ref, 34) . "     " . $p->addLeftRightWithMax("Nama", $nama, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("Tgl. Faktur", sql2date($jual->tgl), 34) . "     " . $p->addLeftRightWithMax("Alamat", $almt1, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("No. SO", $jual->so, 34) . "     " . $p->addLeftRightWithMax("", $almt2, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("", "", 34) . "     " . $p->addLeftRightWithMax("Telp", $phone, 10, 29, " ") . "\r\n" .
    chr(27) . chr(77) . //12cpi
    "================================================================================================\r\n" .
    "NO.KODE          NAMA BARANG                    SAT      JML       HARGA    POTONGAN     NOMINAL\r\n" .
    "================================================================================================\r\n";
$no = 1;
foreach ($jual->detilPenjualans as $detil) {
    $format .= $p->addDetailsItemSales($no, $detil->barang->barcode,
            $detil->barang->barang_name, $detil->sat, $detil->jml, $detil->price, $detil->totalpot, $detil->nominal) . "\r\n";
    $no++;
}
for ($no; $no < 14; $no++) {
    $format .= "                                                                                                \r\n";
}
$user = Users::model()->findByPk($jual->id_user);
$format .=
    "================================================================================================\r\n" .
    $p->addLeftRightWithMax("Salesman :", "Subtotal", 69, 14, " ") . " " . str_pad(number_format($jual->sub_total, 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("Keterangan :", "Ongkos Kirim", 69, 14, " ") . " " . str_pad(number_format($jual->ongkos_angkut, 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax(app()->params['corp']['faktur_footer'][0], "Total", 69, 14, " ") . " " . str_pad(number_format($jual->total, 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax(app()->params['corp']['faktur_footer'][1], "", 69, 14, " ") . " " . str_pad("", 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("Kasir : ".$user->username, "", 69, 14, " ") . " " . "" . "\r\n" .
//    "                                                                                                \r\n" .
    chr(13) . chr(10) . chr(12) . chr(27) . chr(64); // cr lf ff init
echo base64_encode($format);
//echo "<pre>" . $format . "</pre>";