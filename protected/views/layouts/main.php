<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/default.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <style type="text/css">
        /*chrome grid issue */
        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }
        #form-LapPenjualan, #form-Calculator td {
            padding: 2px;
        }

        .border_thin {
            border: 1px solid;
        }

        /** {*/
            /*font-size: 12px;*/
            /*font-family: Candara;*/
        /*}*/

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            /*border: solid;*/
            /*border-color: #C1C1C1;*/
            /*border-width: thin;*/
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all-debug-w-comments.js"></script>
<script>
    // window.onerror = function (msg, url, lineNumber) {
    //     alert(msg + '\nurl: '+ url + '\nline: ' + lineNumber);
    //     return true; //true means don't propogate the error
    // }
    $('*').dblclick(function (event) {
        event.preventDefault();
        alert("DILARANG MENGGUNAKAN DOUBLE KLIK!!!");
        return false;
    });
    Ext.namespace('jun');
    var SYSTEM_TITLE = '<?=app()->params['corp']['nama']; ?>';
    var SYSTEM_SUBTITLE = '<?= app()->params['corp']['address'][0]; ?>';
    var SYSTEM_KAS_KECIL = '<?=Kas::get_kas_kecil_id(); ?>';
    var SYSTEM_PEMBELIAN_1 = <?=SecurityRoles::have_role('108'); ?>;
    var SYSTEM_PEMBELIAN_3 = <?=SecurityRoles::have_role('110'); ?>;
    var SYSTEM_PEMBELIAN_5 = <?=SecurityRoles::have_role('119'); ?>;
    var SYSTEM_PENJUALAN_1 = <?=SecurityRoles::have_role('100'); ?>;
    var SYSTEM_PENJUALAN_3 = <?=SecurityRoles::have_role('102'); ?>;
    var SYSTEM_PENJUALAN_5 = <?=SecurityRoles::have_role('118'); ?>;
    var DEFKASPELPIU = '<?=SysPrefs::get_val('default kas pelunasan piutang')?>';
    var DEFKASPENJ = '<?=SysPrefs::get_val('default kas penjualan')?>';
    var TGL_NOW = Date.parseDate('<?=date("Y-m-d H:i:s");?>', 'Y-m-d H:i:s');
</script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript">
    if(!notReady()){
        var fs = require('fs');
        var data = fs.readFileSync('./config.json'),myObj;
        try {
            myObj = JSON.parse(data);
            PRINTER_RECEIPT = myObj.PRINTER_RECEIPT;
            PRINTER_CARD = myObj.PRINTER_CARD ;
            COM_POSIFLEX = myObj.COM_POSIFLEX ;
        }
        catch (err) {
            console.log('There has been an error parsing your JSON.')
            console.log(err);
        }
    }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<?
$dir = array('/js/view/');
foreach ($dir as $path) {
    $templatePath = dirname(Yii::app()->basePath) . $path;
    $files = scandir($templatePath);
    foreach ($files as $file) {
        if (is_file($templatePath . '/' . $file)) {
            ?>
            <script type="text/javascript" src="<?php echo(bu() . $path . $file); ?>"></script>
        <?
        }
    }
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
</body>
</html>