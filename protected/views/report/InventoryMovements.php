<h1>Mutasi Stok</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->pageTitle = 'Mutasi Stok';
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Barcode',
            'name' => 'barcode'
        ),
        array(
            'header' => 'Nama Barang',
            'name' => 'barang_name'
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'before',
            'value' => function ($data) {
                return format_number_report($data['before'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Masuk',
            'name' => 'masuk',
            'value' => function ($data) {
                return format_number_report($data['masuk'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Keluar',
            'name' => 'keluar',
            'value' => function ($data) {
                return format_number_report($data['keluar'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo Akhir',
            'name' => 'after',
            'value' => function ($data) {
                return format_number_report($data['after'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));