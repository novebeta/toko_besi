<?php
Yii::import('application.components.U');
Yii::import('application.components.Reference');
class GlTransController extends GxController
{
    public function actionCreate()
    {
        $model = new GlTrans;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['GlTrans'][$k] = $v;
            }
            $model->attributes = $_POST['GlTrans'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->counter;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionCreateJurnalUmum()
    {
        if (!app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            Yii::import('application.components.Gl');
            $gl = new Gl();
            $msg = 'Jurnal umum berhasil disimpan.';
            $memo_ = $_POST['memo_'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference(JURNAL_UMUM);
                $jurnal_umum_id = U::get_max_type_no(JURNAL_UMUM);
                $jurnal_umum_id++;
                foreach ($detils as $detil) {
                    $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                    $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                        $detil['account'], $memo_, $memo_, $amount);
                }
                $gl->validate();
                $ref->save(JURNAL_UMUM, $jurnal_umum_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'GlTrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['GlTrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['GlTrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->counter;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->counter));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'GlTrans')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $this->renderJsonArr(GlTrans::jurnal_umum_details($_POST['type_no']));
        }
    }
    public function actionSetSaldoAwal()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            //require_once(Yii::app()->basePath . '/vendors/frontaccounting/ui.inc');
            $status = false;
            $msg = 'Saldo Awal berhasil disimpan.';
            $date = $_POST['trans_date'];
            $user = Yii::app()->user->getId();
            $id = U::get_max_type_no(SALDO_AWAL);
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $gl = new Gl();
                $gl->add_gl(SALDO_AWAL, $id, $date, "-", $_POST['account_code'],
                    '-', "-", get_number($_POST['amount']));
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $id,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionListJurnalUmum()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = JurnalUmum::model()->findAll($criteria);
        $total = JurnalUmum::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}