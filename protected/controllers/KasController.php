<?php
Yii::import('application.components.Reference');
class KasController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            Yii::import('application.components.Gl');
            $gl = new Gl();
            $is_in = $_POST['arus'] == 0;
            $model = new Kas;
            $msg = 'Kas berhasil disimpan.';
            Yii::import('application.components.U');
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference($is_in ? KAS_MASUK : KAS_KELUAR);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Kas'][$k] = $v;
                }
                if (!$is_in) {
                    $_POST['Kas']['total'] = -$_POST['Kas']['total'];
                }
                $_POST['Kas']['doc_ref'] = $docref;
                $model->attributes = $_POST['Kas'];
//                $msg = "Data gagal disimpan";
                if (!$model->save())
                    throw new Exception("Gagal menyimpan kas. " . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $kas_detil = new KasDtl;
                    $_POST['KasDtl']['no_faktur'] = $detil['no_faktur'];
                    $_POST['KasDtl']['nilai_faktur'] = $is_in ? get_number($detil['nilai_faktur']) : -get_number($detil['nilai_faktur']);
                    $_POST['KasDtl']['total'] = $is_in ? get_number($detil['total']) : -get_number($detil['total']);
                    $_POST['KasDtl']['account_code'] = $detil['account_code'];
                    $_POST['KasDtl']['kas_id'] = $model->kas_id;
                    $kas_detil->attributes = $_POST['KasDtl'];
                    if (!$kas_detil->save())
                        throw new Exception("Gagal menyimpan detil kas. " . CHtml::errorSummary($kas_detil));
                    $gl->add_gl($is_in ? KAS_MASUK : KAS_KELUAR, $model->kas_id, $model->tgl, $model->doc_ref,
                        $kas_detil->account_code, '', '', -$kas_detil->total);
                }
                $ref->save($is_in ? KAS_MASUK : KAS_KELUAR, $model->kas_id, $docref);
                $gl->add_gl($is_in ? KAS_MASUK : KAS_KELUAR, $model->kas_id, $model->tgl, $model->doc_ref,
                    $model->idBank->account_code, '', '', $model->total);
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Kas');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Kas'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Kas'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Kas')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $arus = "";
        $param = array();
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition("doc_ref like :doc_ref");
            $param[':doc_ref'] = "%" . $_POST['doc_ref'] . "%";
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition("note like :note");
            $param[':note'] = "%" . $_POST['note'] . "%";
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition("tgl = :tgl");
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['total'])) {
            $criteria->addCondition("total like :total");
            $param[':total'] = "%" . $_POST['total'] . "%";
        }
        if (isset ($_POST['arus'])) {
            $arus = ($_POST['arus'] == 'masuk' || $_POST['arus'] == 'masukkecil') ? "" : "-";
            $criteria->addCondition($arus == '' ? "arus = 0" : "arus = 1");
            if ($_POST['arus'] == 'masukkecil' || $_POST['arus'] == 'keluarkecil') {
                $criteria->addCondition("id_bank = :id_bank");
                $param['id_bank'] = Kas::get_kas_kecil_id();
            }
        }
        $criteria->params = $param;
        $criteria->order = "doc_ref DESC";
        $criteria->select = "kas_id, doc_ref, tgl, $arus(total) total, note, cara_bayar,
         who, id_bank, tipe_kas, who_id,arus";
        if ((isset($_POST['limit']) && isset($_POST['start']) || (isset($_POST['mode']) && $_POST['mode'] == 'grid'))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Kas::model()->findAll($criteria);
        $total = Kas::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}