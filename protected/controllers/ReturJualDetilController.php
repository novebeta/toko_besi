<?php
class ReturJualDetilController extends GxController {
	public function actionCreate() {
		$model = new ReturJualDetil ();
		if (! Yii::app ()->request->isAjaxRequest)
			return;
		if (isset ( $_POST ) && ! empty ( $_POST )) {
			foreach ( $_POST as $k => $v ) {
				if (is_angka ( $v ))
					$v = get_number ( $v );
				$_POST ['ReturJualDetil'] [$k] = $v;
			}
			$model->attributes = $_POST ['ReturJualDetil'];
			$msg = "Data gagal disimpan";
			
			if ($model->save ()) {
				$status = true;
				$msg = "Data berhasil di simpan dengan id " . $model->retur_jual_detil_id;
			} else {
				$status = false;
			}
			
			echo CJSON::encode ( array (
					'success' => $status,
					'msg' => $msg 
			) );
			Yii::app ()->end ();
		}
	}
	public function actionUpdate($id) {
		$model = $this->loadModel ( $id, 'ReturJualDetil' );
		
		if (isset ( $_POST ) && ! empty ( $_POST )) {
			foreach ( $_POST as $k => $v ) {
				if (is_angka ( $v ))
					$v = get_number ( $v );
				$_POST ['ReturJualDetil'] [$k] = $v;
			}
			$msg = "Data gagal disimpan";
			$model->attributes = $_POST ['ReturJualDetil'];
			
			if ($model->save ()) {
				
				$status = true;
				$msg = "Data berhasil di simpan dengan id " . $model->retur_jual_detil_id;
			} else {
				$status = false;
			}
			
			if (Yii::app ()->request->isAjaxRequest) {
				echo CJSON::encode ( array (
						'success' => $status,
						'msg' => $msg 
				) );
				Yii::app ()->end ();
			} else {
				$this->redirect ( array (
						'view',
						'id' => $model->retur_jual_detil_id 
				) );
			}
		}
	}
	public function actionDelete($id) {
		if (Yii::app ()->request->isPostRequest) {
			$msg = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel ( $id, 'ReturJualDetil' )->delete ();
			} catch ( Exception $ex ) {
				$status = false;
				$msg = $ex->getMessage();
			}
			echo CJSON::encode ( array (
					'success' => $status,
					'msg' => $msg 
			) );
			Yii::app ()->end ();
		} else
			throw new CHttpException ( 400, Yii::t ( 'app', 'Invalid request. Please do not repeat this request again.' ) );
	}
	public function actionIndex() {
        Yii::import('application.components.U');
        $id = $_POST['penjualan_id'];
        $retur = U::get_retur_jual_detil($id);
        $total = count($retur);
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($retur) . '}';
        Yii::app()->end($jsonresult);
	}
}