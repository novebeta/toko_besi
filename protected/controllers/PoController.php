<?php
Yii::import('application.components.Reference');
class PoController extends GxController
{
    /**
     *TODO : Set barang yang dipesan
     */
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $po_id = $_POST['id'];
            $msg = 'Purchase order berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $po = $is_new ? new Po : $this->loadModel($po_id,
                    "Po");
                $docref = $po->no_po;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(PO);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Po'][$k] = $v;
                }
                $_POST['Po']['no_po'] = $docref;
                $po->attributes = $_POST['Po'];
                if (!$po->save())
                    throw new Exception("Gagal menyimpan purchase order. " . CHtml::errorSummary($po));
                PoDetil::model()->deleteAll('po_id  = :po_id',
                    array(':po_id' => $po_id));
                foreach ($detils as $detil) {
                    $po_detil = new PoDetil;
                    $_POST['PoDetil']['barang_id'] = $detil['barang_id'];
                    $_POST['PoDetil']['sat'] = $detil['sat'];
                    $_POST['PoDetil']['jml'] = get_number($detil['jml']);
                    $_POST['PoDetil']['pcs'] = 0;
                    $_POST['PoDetil']['po_id'] = $po->po_id;
                    $po_detil->attributes = $_POST['PoDetil'];
                    if (!$po_detil->save())
                        throw new Exception("Gagal menyimpan purchase order detil. " . CHtml::errorSummary($po_detil));
                }
                if ($is_new) {
                    $ref->save(PO, $po->po_id, $docref);
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Po');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Po'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Po'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->po_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->po_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Po')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['done'])) {
            $criteria->addCondition('done = :done');
            $param[':done'] = $_POST['done'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->order = "tgl DESC";
        $criteria->params = $param;
        $model = Po::model()->findAll($criteria);
        $total = Po::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}