<?php

class BankTransController extends GxController
{
    public function actionCreate()
    {
        $model = new BankTrans;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BankTrans'][$k] = $v;
            }
            $model->attributes = $_POST['BankTrans'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bank_trans_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionGetBalance()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            //$id = $_POST['id'];
            Yii::import('application.components.U');
            $amt = U::get_balance_before_for_bank_account(get_date_tomorrow(), $_POST['id']);
            echo CJSON::encode(array(
                'success' => true,
                'id' => $amt,
            ));
            Yii::app()->end();
        }
    }

    public function actionCreateTransfer()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Transfer antar kas berhasil diproses.';
            $id = -1;
            $bank_asal = $_POST['bank_act_asal'];
            $bank_tujuan = $_POST['bank_act_tujuan'];
            $trans_date = $_POST['trans_date'];
            $memo = $_POST['memo'];
            $amount = get_number($_POST['amount']);
            Yii::import('application.components.U');
            Yii::import('application.components.Reference');
            Yii::import('application.components.Gl');
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference(BANKTRANSFER);
                $bank_account_asal = U::get_act_code_from_bank_act($bank_asal);
                $bank_account_tujuan = U::get_act_code_from_bank_act($bank_tujuan);
                $trans_no = U::get_next_trans_no_bank_trans(BANKTRANSFER);
                $user = Yii::app()->user->getId();
                //debet kode bank tujuan - kredit kode bank asal
                $amount = round($amount,2);
                $gl = new Gl();
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_tujuan,
                    '',$memo,$amount);
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                    '',$memo, -$amount);
                $gl->validate();
                $ref->save(BANKTRANSFER, $trans_no, $docref);
                $id = $docref;
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $id,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'BankTrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BankTrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['BankTrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bank_trans_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bank_trans_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'BankTrans')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = BankTrans::model()->findAll($criteria);
        $total = BankTrans::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}