<?php
Yii::import('application.components.Reference');
class PenjualanController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
//            $is_new = $_POST['mode'] == 0;
            $is_new = true;
            $TYPE = $_POST['arus'] == 1 ? PENJUALAN : PENJUALAN_KREDIT;
//            $is_final = $_POST['mode'] == 2;
            $penjualan_id = $_POST['id'];
            $msg = 'Penjualan berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::import('application.components.U');
                $penjualan = $is_new ? new Penjualan : $this->loadModel($penjualan_id,
                    "Penjualan");
                $docref = $penjualan->doc_ref;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($TYPE);
//                    $docref = get_date_today('yy') . $docref;
                }
                $_POST['Penjualan']['kirim'] = 0;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Penjualan'][$k] = $v;
                }
                $_POST['Penjualan']['doc_ref'] = $docref;
//                $_POST['Penjualan']['arus'] = 1;
                $_POST['Penjualan']['id_user'] = Yii::app()->user->getId();
//                if ($is_final) $_POST['Penjualan']['final'] = 1;
                $_POST['Penjualan']['lunas'] = $_POST['Penjualan']['sisa_tagihan'] == 0 ? 1 : 0;
                $_POST['Penjualan']['status_kirim'] = $_POST['Penjualan']['kirim'];
                $penjualan->attributes = $_POST['Penjualan'];
                if (!$penjualan->save()) {
                    throw new Exception("Gagal menyimpan penjualan. " . CHtml::errorSummary($penjualan));
                }
//                DetilPenjualan::model()->deleteAll('penjualan_id  = :penjualan_id',
//                    array(':penjualan_id' => $penjualan_id));
                $ppn = 0;
                $hpp = 0;
                foreach ($detils as $detil) {
                    $penjualan_detil = new DetilPenjualan;
                    $_POST['DetilPenjualan']['barang_id'] = $detil['barang_id'];
                    $_POST['DetilPenjualan']['sat'] = "";//$detil['sat'];
                    $_POST['DetilPenjualan']['jml'] = get_number($detil['jml']);
                    $_POST['DetilPenjualan']['price'] = get_number($detil['price']);
                    $_POST['DetilPenjualan']['disc1'] = get_number($detil['disc1']);
                    $_POST['DetilPenjualan']['disc2'] = 0;//get_number($detil['disc2']);
                    $_POST['DetilPenjualan']['pot'] = get_number($detil['pot']);
                    $_POST['DetilPenjualan']['totalpot'] = get_number($detil['totalpot']);
                    $_POST['DetilPenjualan']['bruto'] = get_number($detil['bruto']);
                    $_POST['DetilPenjualan']['nominal'] = get_number($detil['nominal']);
//                    $_POST['DetilPenjualan']['pcs'] = get_number($detil['pcs']);
//                    $_POST['DetilPenjualan']['hpp'] = get_number($detil['hpp']);
                    $_POST['DetilPenjualan']['penjualan_id'] = $penjualan->penjualan_id;
                    $penjualan_detil->attributes = $_POST['DetilPenjualan'];
                    if (!$penjualan_detil->save()) {
                        throw new Exception("Gagal menyimpan penjualan detil. " . CHtml::errorSummary($penjualan_detil));
                    }
//                    if ($is_final) {
                    if ($penjualan_detil->barang->ppn) {
                        $ppn += round(($penjualan_detil->nominal) * (1 / 11), 2);
                    }
                    $hpp_detil = $penjualan_detil->barang->get_hpp($penjualan_detil->jml);
                    $hpp += $hpp_detil;
                    $penjualan_detil->hpp = $hpp_detil;
                    if (!$penjualan_detil->save()) {
                        throw new Exception("Gagal menyimpan hpp penjualan detil. " . CHtml::errorSummary($penjualan_detil));
                    }
                    U::add_stock_moves($TYPE, $penjualan->penjualan_id, $penjualan->tgl,
                        $penjualan_detil->barang_id, -$penjualan_detil->jml, $penjualan->doc_ref,
                        $penjualan_detil->price);
//                    }
                }
                $hpp = round($hpp, 2);
                if ($is_new) {
                    $ref->save($TYPE, $penjualan->penjualan_id, $docref);
                }
//                $is_agen = $penjualan->konsumen->agen == 1;
//                if ($is_final) {
                Yii::import('application.components.Gl');
                $gl = new Gl();
                // Kas Debet
                if ($TYPE == PENJUALAN) {
                    if ($penjualan->total != 0) {
                        $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                            $penjualan->idBank->account_code, '', '', $penjualan->total);
                    }
                } else {
                    if ($penjualan->uang_muka != 0) {
                        $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                            $penjualan->idBank->account_code, '', '', $penjualan->uang_muka);
                    }
                    // piutang dagang debet
                    if ($penjualan->sisa_tagihan != 0) {
                        $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                            PIUTANGDAGANG, '', '', $penjualan->sisa_tagihan);
                    }
                }
                if ($penjualan->disc != 0) {
                    //disc debet
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        POTONGPENJUALANSALES, '', '', $penjualan->disc);
                }
                if ($penjualan->ongkos_angkut != 0) {
                    //ongkos angkut kredit
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        ONGKIRJUAL, '', '', -$penjualan->ongkos_angkut);
                }
                if ($ppn != 0) {
                    //ppn kredit
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        PPN, '', '', -$ppn);
                }
                //penjualan kredit
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    PENJUALANSALES, '', '', -($penjualan->bruto - $ppn));
                // hpp debet
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    HPP, '', '', $hpp);
                // persediaan kredit
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    PERSEDIANBARANGDAGANG, '', '', -$hpp);
                $gl->validate();
//                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionCreateRetur()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
//            $is_new = $_POST['mode'] == 0;
            $is_new = true;
            $TYPE = $_POST['arus'] == -1 ? RETURJUAL : RETUR_JUAL_KREDIT;
//            $is_final = $_POST['mode'] == 2;
            $penjualan_id = $_POST['id'];
            $msg = 'Penjualan berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::import('application.components.U');
                $penjualan = $is_new ? new Penjualan : $this->loadModel($penjualan_id,
                    "Penjualan");
                $docref = $penjualan->doc_ref;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($TYPE);
//                    $docref = get_date_today('yy') . $docref;
                }
                $_POST['Penjualan']['kirim'] = 0;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Penjualan'][$k] = $v;
                }
                $_POST['Penjualan']['doc_ref'] = $docref;
//                $_POST['Penjualan']['arus'] = 1;
                $_POST['Penjualan']['id_user'] = Yii::app()->user->getId();
//                if ($is_final) $_POST['Penjualan']['final'] = 1;
                $_POST['Penjualan']['lunas'] = $_POST['Penjualan']['sisa_tagihan'] == 0 ? 1 : 0;
                $_POST['Penjualan']['status_kirim'] = $_POST['Penjualan']['kirim'];
                $penjualan->attributes = $_POST['Penjualan'];
                if (!$penjualan->save()) {
                    throw new Exception("Gagal menyimpan penjualan. " . CHtml::errorSummary($penjualan));
                }
//                DetilPenjualan::model()->deleteAll('penjualan_id  = :penjualan_id',
//                    array(':penjualan_id' => $penjualan_id));
                $ppn = 0;
                $hpp = 0;
                foreach ($detils as $detil) {
                    $penjualan_detil = new DetilPenjualan;
                    $_POST['DetilPenjualan']['barang_id'] = $detil['barang_id'];
                    $_POST['DetilPenjualan']['sat'] = "";//$detil['sat'];
                    $_POST['DetilPenjualan']['jml'] = get_number($detil['jml']);
                    $_POST['DetilPenjualan']['price'] = get_number($detil['price']);
                    $_POST['DetilPenjualan']['disc1'] = get_number($detil['disc1']);
                    $_POST['DetilPenjualan']['disc2'] = 0;//get_number($detil['disc2']);
                    $_POST['DetilPenjualan']['pot'] = get_number($detil['pot']);
                    $_POST['DetilPenjualan']['totalpot'] = get_number($detil['totalpot']);
                    $_POST['DetilPenjualan']['bruto'] = get_number($detil['bruto']);
                    $_POST['DetilPenjualan']['nominal'] = get_number($detil['nominal']);
//                    $_POST['DetilPenjualan']['pcs'] = get_number($detil['pcs']);
//                    $_POST['DetilPenjualan']['hpp'] = get_number($detil['hpp']);
                    $_POST['DetilPenjualan']['penjualan_id'] = $penjualan->penjualan_id;
                    $penjualan_detil->attributes = $_POST['DetilPenjualan'];
                    if (!$penjualan_detil->save()) {
                        throw new Exception("Gagal menyimpan penjualan detil. " . CHtml::errorSummary($penjualan_detil));
                    }
//                    if ($is_final) {
                    if ($penjualan_detil->barang->ppn) {
                        $ppn += ($penjualan_detil->nominal) * (1 / 11);
                    }
                    $hpp_detil = $penjualan_detil->barang->get_hpp($penjualan_detil->jml);
                    $hpp += $hpp_detil;
                    $penjualan_detil->hpp = $hpp_detil;
                    if (!$penjualan_detil->save()) {
                        throw new Exception("Gagal menyimpan hpp penjualan detil. " . CHtml::errorSummary($penjualan_detil));
                    }
                    U::add_stock_moves($TYPE, $penjualan->penjualan_id, $penjualan->tgl,
                        $penjualan_detil->barang_id, -$penjualan_detil->jml, $penjualan->doc_ref,
                        $penjualan_detil->price);
//                    }
                }
                $hpp = round($hpp, 2);
                if ($is_new) {
                    $ref->save($TYPE, $penjualan->penjualan_id, $docref);
                }
//                $is_agen = $penjualan->konsumen->agen == 1;
//                if ($is_final) {
                Yii::import('application.components.Gl');
                $gl = new Gl();
                // Kas Debet
                if ($penjualan->uang_muka != 0) {
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        KASSEMENTARA, '', '', $penjualan->uang_muka);
                }
                // piutang dagang debet
                if ($penjualan->sisa_tagihan != 0) {
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        PIUTANGDAGANG, '', '', $penjualan->sisa_tagihan);
                }
                if ($ppn != 0) {
                    //ppn kredit
                    $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                        PPN, '', '', -$ppn);
                }
//                    if ($penjualan->disc != 0) {
//                        //disc debet
//                        U::add_gl(PENJUALAN, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
//                           $is_agen ? POTONGPENJUALANAGEN : POTONGPENJUALANSALES, '', '', $penjualan->disc);
//                    }
                //penjualan kredit
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    PENJUALANSALES, '', '', -($penjualan->total - $ppn));
                // hpp debet
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    HPP, '', '', $hpp);
                // persediaan kredit
                $gl->add_gl($TYPE, $penjualan->penjualan_id, $penjualan->tgl, $penjualan->doc_ref,
                    PERSEDIANBARANGDAGANG, '', '', -$hpp);
                $gl->validate();
//                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Penjualan');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Penjualan'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Penjualan'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->penjualan_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->penjualan_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Penjualan')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app',
                    'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['status_kirim']) && $_POST['status_kirim'] != 3) {
            $criteria->addCondition("status_kirim = :status_kirim");
            $param[':status_kirim'] = $_POST['status_kirim'];
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        switch ($_POST['arus']) {
            case 1:
                $model = PenjualanTunai::model()->findAll($criteria);
                $total = PenjualanTunai::model()->count($criteria);
                break;
            case 2:
                $model = PenjualanKredit::model()->findAll($criteria);
                $total = PenjualanKredit::model()->count($criteria);
                break;
            case -1:
                $model = PenjualanTunaiRetur::model()->findAll($criteria);
                $total = PenjualanTunaiRetur::model()->count($criteria);
                break;
            case -2:
                $model = PenjualanKreditRetur::model()->findAll($criteria);
                $total = PenjualanKreditRetur::model()->count($criteria);
                break;
        }
        $this->renderJson($model, $total);
    }
    public function actionPrint()
    {
        Yii::import('application.components.BasePrint');
//        $this->layout = "Print";
//        $_REQUEST['penjualan_id'] = '754b0b52-fa8f-11e4-94bd-201a06331945';
        /** @var $jual Penjualan */
        $penjualan = Penjualan::model()->findByPk($_REQUEST['penjualan_id']);
        $param = array(
            'jual' => $penjualan
        );
//        if (!Yii::app()->request->isAjaxRequest) {
//            $this->redirect(url('/'));
//            if ($penjualan->type_ == 0) {
//                $param['nama_faktur'] ='FAKTUR PENJUALAN TUNAI';
//                $this->render('tunai', $param);
//            } else {
//                $param['nama_faktur'] ='FAKTUR PENJUALAN KREDIT';
//                $this->render('kredit', $param);
//            }
//            Yii::app()->end();
//        }
        $encode = "";
        if ($penjualan->arus == 1) {
            $param['nama_faktur'] = 'FAKTUR PENJUALAN TUNAI';
            $encode = $this->render('tunai', $param, true);
        } elseif ($penjualan->arus == 2) {
            $param['nama_faktur'] = 'FAKTUR PENJUALAN KREDIT';
            $encode = $this->render('kredit', $param, true);
        }
//        echo CJSON::encode(array(
//            'success' => true,
//            'msg' => $encode
//        ));
        echo $encode;
        Yii::app()->end();
    }
    public function actionDo()
    {
        Yii::import('application.components.BasePrint');
//        $this->layout = "Print";
//        $_REQUEST['penjualan_id'] = '754b0b52-fa8f-11e4-94bd-201a06331945';
        /** @var $jual Penjualan */
        $penjualan = Penjualan::model()->findByPk($_REQUEST['penjualan_id']);
        $param = array(
            'jual' => $penjualan
        );
        if (strlen(trim($penjualan->doc_ref_do)) == 0) {
            $penjualan->doc_ref_do = str_replace('P', 'DO', $penjualan->doc_ref);
            $penjualan->status_kirim = SALES_SUDAH_KIRIM;
            if (!$penjualan->save()) {
                throw new Exception("Gagal menyimpan penjualan. " . CHtml::errorSummary($penjualan));
            }
        }
        if ($penjualan->arus == 1) {
            $param['nama_faktur'] = 'DELIVERY ORDER';
        } elseif ($penjualan->arus == 2) {
            $param['nama_faktur'] = 'TITIPAN BARANG';
        }
        $encode = $this->render('do', $param, true);
        echo $encode;
        Yii::app()->end();
    }
}