<?php
class GudangController extends GxController {
    public function actionCreate() {
        $model = new Gudang;


        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Gudang'][$k] = $v;
            }
            $model->attributes = $_POST['Gudang'];
            $msg = "Data gagal disimpan";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->gudang_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Gudang');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Gudang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Gudang'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->gudang_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->gudang_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Gudang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
            if (!Yii::app()->request->isAjaxRequest)
                    $this->redirect(array('admin'));
        }
        else
                throw new CHttpException(400,
            Yii::t('app',
                    'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex() {
        $criteria = new CDbCriteria();
        $model = Gudang::model()->findAll($criteria);
        $total = Gudang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}