<?php
Yii::import('application.components.U');
class BankController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            $msg = "Akun bank berhasil dibuat";
            $model = new Bank;
            if (U::account_in_gl_trans($id)) {
                $status = false;
                $msg = "Akun bank gagal dibuat, karena account sudah dipakai transaksi.";
            } elseif (U::account_used_bank($id)) {
                $status = false;
                $msg = "Akun bank gagal dibuat, karena account dipakai akun bank lain.";
            }
            if (!$status) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $model->attributes = $_POST['Bank'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->id_bank;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Bank');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Bank'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->id_bank;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->id_bank));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Bank')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['account_code'])) {
            $criteria->addCondition("account_code like :account_code");
            $param[':account_code'] = "%" . $_POST['account_code'] . "%";
        }
        if (isset($_POST['nama_akun'])) {
            $criteria->addCondition("nama_akun like :nama_akun");
            $param[':nama_akun'] = "%" . $_POST['nama_akun'] . "%";
        }
        if (isset($_POST['no_rekening'])) {
            $criteria->addCondition("no_rekening like :no_rekening");
            $param[':no_rekening'] = "%" . $_POST['no_rekening'] . "%";
        }
        if (isset($_POST['nama_bank'])) {
            $criteria->addCondition("nama_bank like :nama_bank");
            $param[':nama_bank'] = "%" . $_POST['nama_bank'] . "%";
        }
        if (isset($_POST['alamat_bank'])) {
            $criteria->addCondition("alamat_bank like :alamat_bank");
            $param[':alamat_bank'] = "%" . $_POST['alamat_bank'] . "%";
        }
        if (isset($_POST['no_tlp'])) {
            $criteria->addCondition("no_tlp like :no_tlp");
            $param[':no_tlp'] = "%" . $_POST['no_tlp'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Bank::model()->findAll($criteria);
        $total = Bank::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}