<?php
class KonsumenController extends GxController
{
    public function actionCreate()
    {
        $model = new Orang;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Konsumen'][$k] = $v;
            }
            $_POST['Konsumen']['type_'] = KONSUMEN;
            $model->attributes = $_POST['Konsumen'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->orang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Orang');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Konsumen'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Konsumen'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->orang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->order = "nama";
        if (isset($_POST['konsumen_code'])) {
            $criteria->addCondition("konsumen_code like :konsumen_code");
            $param[':konsumen_code'] = $_POST['konsumen_code'] . "%";
        }
        if (isset($_POST['konsumen_name'])) {
            $criteria->addCondition("konsumen_name like :konsumen_name");
            $param[':konsumen_name'] = "%" . $_POST['konsumen_name'] . "%";
        }
        if (isset($_POST['phone'])) {
            $criteria->addCondition("phone like :phone");
            $param[':phone'] = "%" . $_POST['phone'] . "%";
        }
        if (isset($_POST['hp'])) {
            $criteria->addCondition("hp like :hp");
            $param[':hp'] = "%" . $_POST['hp'] . "%";
        }
        if (isset($_POST['hp2'])) {
            $criteria->addCondition("hp2 like :hp2");
            $param[':hp2'] = "%" . $_POST['hp2'] . "%";
        }
        if (isset($_POST['tempo'])) {
            $criteria->addCondition("tempo like :tempo");
            $param[':tempo'] = "%" . $_POST['tempo'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        if (isset($_POST['mode']) && $_POST['mode'] == 'active') {
            $criteria->addColumnCondition(array('status' => 1));
        } else if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
//        $criteria->limit = 100;
        $model = Konsumen::model()->findAll($criteria);
        $total = Konsumen::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}