<?php
Yii::import('application.components.Reference');
class ReturBotolController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $retur_botol_id = $_POST['id'];
            $msg = 'Retur botol berhasil disimpan.';
            Yii::import('application.components.U');
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $retur_botol = $is_new ? new ReturBotol : $this->loadModel($retur_botol_id,
                    "ReturBotol");
                $docref = $retur_botol->doc_ref;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(RETURBOTOL);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['ReturBotol'][$k] = $v;
                }
                $_POST['ReturBotol']['doc_ref'] = $docref;
                $retur_botol->attributes = $_POST['ReturBotol'];
                if (!$retur_botol->save())
                    throw new Exception("Gagal menyimpan retur botol. ".CHtml::errorSummary($retur_botol));
                ReturBotolDetil::model()->deleteAll('retur_botol_id  = :retur_botol_id',
                    array(':retur_botol_id' => $retur_botol_id));
                $selish_rp = 0;
                foreach ($detils as $detil) {
                    $ReturBotol_detil = new ReturBotolDetil;
                    $_POST['ReturBotolDetil']['jenis_botol_id'] = $detil['jenis_botol_id'];
                    $_POST['ReturBotolDetil']['jml_faktur'] = get_number($detil['jml_faktur']);
                    $_POST['ReturBotolDetil']['jml_data_gudang'] = get_number($detil['jml_data_gudang']);
                    $_POST['ReturBotolDetil']['jml_selisih'] = get_number($detil['jml_selisih']);
                    $_POST['ReturBotolDetil']['price'] = get_number($detil['price']);
                    $_POST['ReturBotolDetil']['retur_botol_id'] = $retur_botol->retur_botol_id;
                    $ReturBotol_detil->attributes = $_POST['ReturBotolDetil'];
                    if (!$ReturBotol_detil->save())
                        throw new Exception("Gagal menyimpan detil retur botol. ".CHtml::errorSummary($ReturBotol_detil));
                    if ($ReturBotol_detil->jml_selisih != 0) {
                        $selish_rp += $ReturBotol_detil->jml_selisih * $ReturBotol_detil->price;
                    }
                }
                if ($is_new) {
                    $ref->save(RETURBOTOL, $retur_botol->retur_botol_id, $docref);
                }
                if ($selish_rp != 0) {
                    Yii::import('application.components.Gl');
                    $gl = new Gl();
                    $gl->add_gl(RETURBOTOL, $retur_botol->retur_botol_id, $retur_botol->tgl, $retur_botol->doc_ref,
                        PIUTANGBOTOL, '', '', -$selish_rp);
                    $gl->add_gl(RETURBOTOL, $retur_botol->retur_botol_id, $retur_botol->tgl, $retur_botol->doc_ref,
                        PENDAPATANLAIN, '', '', $selish_rp);
                    $gl->validate();
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ReturBotol');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ReturBotol'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ReturBotol'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->retur_botol_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->retur_botol_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ReturBotol')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $tgl = $_POST['tgl'];
        $sales = $_POST['sales'];
        $criteria = new CDbCriteria();
        $criteria->addCondition("salesman_id = :salesman_id");
        $criteria->addCondition("tgl = :tgl");
        $criteria->params = array(':salesman_id' => $sales, ':tgl' => $tgl);
        $model = ReturBotol::model()->findAll($criteria);
        $total = ReturBotol::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}