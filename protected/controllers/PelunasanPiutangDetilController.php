<?php

class PelunasanPiutangDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new PelunasanPiutangDetil;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanPiutangDetil'][$k] = $v;
            }
            $model->attributes = $_POST['PelunasanPiutangDetil'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_piutang_detil_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PelunasanPiutangDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanPiutangDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PelunasanPiutangDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_piutang_detil_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pelunasan_piutang_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PelunasanPiutangDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['pelunasan_piutang_id'])){
            $criteria->addCondition('pelunasan_piutang_id = :pelunasan_piutang_id');
            $param[':pelunasan_piutang_id'] = $_POST['pelunasan_piutang_id'];
        }
        $criteria->params = $param;
        $model = PelunasanPiutangDetilView::model()->findAll($criteria);
        $total = PelunasanPiutangDetilView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionFaktur()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['konsumen_id'])){
            $criteria->addCondition('konsumen_id = :konsumen_id');
            $param[':konsumen_id'] = $_POST['konsumen_id'];
        }
        $criteria->params = $param;
        $model = DaftarFakturPiutang::model()->findAll($criteria);
        $total = DaftarFakturPiutang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPiutangSales()
    {
        Yii::import('application.components.U');
        $sales = $_POST['sales'];
        $tgl = $_POST['tgl'];
        $piutang = U::get_piutang_per_sales($sales,$tgl);
        $total = count($piutang);
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($piutang) . '}';
        Yii::app()->end($jsonresult);
    }
}