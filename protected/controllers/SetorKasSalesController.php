<?php
Yii::import('application.components.Reference');
class SetorKasSalesController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $is_new = true;//$_POST['mode'] == 0;
//            $setor_kas_id = $_POST['id'];
            $msg = 'Setoran kas per salesman berhasil disimpan.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $setor_kas = new SetorKasSales;
                $docref = $setor_kas->doc_ref;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(SETORKASSALES);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SetorKasSales'][$k] = $v;
                }
                $_POST['SetorKasSales']['doc_ref'] = $docref;
                $_POST['SetorKasSales']['salesman_id'] = $_POST['sales'];
                Yii::import('application.components.U');
                $setor = U::get_setoran_kas_per_sales($_POST['tgl'], $_POST['sales']);
                $_POST['SetorKasSales']['faktur'] = $setor['faktur'];
                $_POST['SetorKasSales']['pelunasan_piutang'] = $setor['piutang'];
                $_POST['SetorKasSales']['retur_botol'] = $setor['retur_botol'];
                $_POST['SetorKasSales']['biaya_sales'] = $setor['biaya_sales'];
                $_POST['SetorKasSales']['retur_penjualan_tunai'] = $setor['retur_jual'];
                $_POST['SetorKasSales']['kas_disetor'] = $setor['total'];
                $setor_kas->attributes = $_POST['SetorKasSales'];
                if (!$setor_kas->save())
                    throw new Exception("Gagal menyimpan jurnal. ".CHtml::errorSummary($setor_kas));
                if ($is_new) {
                    $ref->save(SETORKASSALES, $setor_kas->setor_kas_sales_id, $docref);
                }
                Yii::import('application.components.Gl');
                $gl = new Gl();
                $gl->add_gl(SETORKASSALES, $setor_kas->setor_kas_sales_id, $setor_kas->tgl, $setor_kas->doc_ref,
                    KASBESAR, '', '', $setor_kas->kas_disetor);
                $gl->add_gl(SETORKASSALES, $setor_kas->setor_kas_sales_id, $setor_kas->tgl, $setor_kas->doc_ref,
                    KASSEMENTARA, '', '', -$setor_kas->kas_disetor);
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SetorKasSales');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SetorKasSales'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SetorKasSales'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->setor_kas_sales_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->setor_kas_sales_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SetorKasSales')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $tgl = $_POST['tgl'];
        $sales = $_POST['sales'];
        $criteria = new CDbCriteria();
        $criteria->addCondition("salesman_id = :salesman_id");
        $criteria->addCondition("tgl = :tgl");
        $criteria->params = array(':salesman_id' => $sales, ':tgl' => $tgl);
        $model = SetorKasSales::model()->findAll($criteria);
        $total = SetorKasSales::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}