<?php
class ChartMasterController extends GxController
{
    public function actionCreate()
    {
        $model = new ChartMaster;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ChartMaster'][$k] = $v;
            }
            $model->attributes = $_POST['ChartMaster'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->account_code;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ChartMaster');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ChartMaster'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ChartMaster'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->account_code;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->account_code));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ChartMaster')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        } else
        $param = array();
        if (isset ($_POST['mode']) && $_POST['mode'] == 'detil') {
            $criteria->addCondition("header = 0");
        } else if (isset ($_POST['mode']) && $_POST['mode'] == 'header') {
            $criteria->addCondition("header = 1");
        } else if (isset ($_POST['mode']) && $_POST['mode'] == 'nonkasbank') {
            $criteria->condition = "(kategori NOT IN (:kas,:bank) OR kategori IS NULL) AND header = 0";
//            $criteria->addNotInCondition('kategori',array(GRUPKAS,GRUPBANK));
//            $criteria->addCondition("header = 0");
            $param[':kas'] = GRUPKAS;
            $param[':bank'] = GRUPBANK;
        }
        $criteria->params = $param;
        $model = ChartMaster::model()->findAll($criteria);
        $total = ChartMaster::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}