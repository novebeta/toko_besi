<?php

class KasDtlController extends GxController
{
    public function actionCreate()
    {
        $model = new KasDtl;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KasDtl'][$k] = $v;
            }
            $model->attributes = $_POST['KasDtl'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_dtl_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'KasDtl');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KasDtl'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['KasDtl'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_dtl_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_dtl_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'KasDtl')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $arus = $_POST['mode'] == 'in' ? "" : "-";
        $criteria = new CDbCriteria();
        $criteria->select = "kas_dtl_id, no_faktur, $arus(nilai_faktur) nilai_faktur, $arus(total) total,
        kas_id, account_code";
        $criteria->addCondition("kas_id = :kas_id");
        $param[':kas_id'] = $_POST['kas_id'];
        $criteria->params = $param;
        $model = KasDtl::model()->findAll($criteria);
        $total = KasDtl::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}