<?php
class BarangController extends GxController
{
    public function actionView($id)
    {
        $this->render('view',
            array(
                'model' => $this->loadModel($id, 'Barang'),
            ));
    }
    public function actionCreate()
    {
        $model = new Barang;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $_POST['Barang']['status'] = 0;
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Barang'][$k] = $v;
            }
            $model->attributes = $_POST['Barang'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->barang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Barang');
        if (isset($_POST) && !empty($_POST)) {
//            $model->bonus = 0;
            $model->ppn = 0;
            $_POST['Barang']['status'] = 0;
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Barang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Barang'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->barang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->order = "barang_name";
        if (isset($_POST['barcode'])) {
            $criteria->addCondition("barcode like :barcode");
            $param[':barcode'] = "%" . $_POST['barcode'] . "%";
        }
        if (isset($_POST['barang_name'])) {
            $criteria->addCondition("barang_name like :barang_name");
            $param[':barang_name'] = "%" . $_POST['barang_name'] . "%";
        }
        if (isset($_POST['price_pcs'])) {
            $criteria->addCondition("price_pcs like :price_pcs");
            $param[':price_pcs'] = "%" . $_POST['price_pcs'] . "%";
        }
        if (isset($_POST['price_lsn'])) {
            $criteria->addCondition("price_lsn like :price_lsn");
            $param[':price_lsn'] = "%" . $_POST['price_lsn'] . "%";
        }
        if (isset($_POST['price_pak'])) {
            $criteria->addCondition("price_pak like :price_pak");
            $param[':price_pak'] = "%" . $_POST['price_pak'] . "%";
        }
        if (isset($_POST['stok_minimal'])) {
            $criteria->addCondition("stok_minimal like :stok_minimal");
            $param[':stok_minimal'] = "%" . $_POST['stok_minimal'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) || isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Barang::model()->findAll($criteria);
        $total = Barang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionCmp()
    {
//        Yii::import('application.components.U');
//        $barang = U::get_barang_cmp();
//        $total = count($barang);
//        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($barang) . '}';
//        Yii::app()->end($jsonresult);
        $criteria = new CDbCriteria();
        $model = BarangCmp::model()->findAll($criteria);
        $total = BarangCmp::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}