<?php
class SupplierController extends GxController {
  
    public function actionCreate() {
        $model = new Orang;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Supplier'][$k] = $v;
            }
            $_POST['Supplier']['type_'] = SUPPLIER;
            $model->attributes = $_POST['Supplier'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->orang_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Orang');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Supplier'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Supplier'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->orang_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->orang_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['supplier_code'])) {
            $criteria->addCondition("supplier_code like :supplier_code");
            $param[':supplier_code'] = "%" . $_POST['supplier_code'] . "%";
        }
        if (isset($_POST['supplier_name'])) {
            $criteria->addCondition("supplier_name like :supplier_name");
            $param[':supplier_name'] = "%" . $_POST['supplier_name'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;

        if (isset($_POST['mode']) && $_POST['mode'] == 'active') {
            $criteria->addColumnCondition(array('status' => 1));
        } else if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) || isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Supplier::model()->findAll($criteria);
        $total = Supplier::model()->count($criteria);

        $this->renderJson($model, $total);
    }
}