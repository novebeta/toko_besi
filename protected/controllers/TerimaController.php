<?php
Yii::import('application.components.Reference');
class TerimaController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
//            $is_new = $_POST['mode'] == 0;
//            $po_id = $_POST['id'];
            $msg = 'Penerimaan barang berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $terima = new Terima;
                $ref = new Reference();
                $docref = $ref->get_next_reference(TERIMABARANG);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Terima'][$k] = $v;
                }
                $_POST['Terima']['no_lpb'] = $docref;
                $terima->attributes = $_POST['Terima'];
                if (!$terima->save())
                    throw new Exception("Gagal menyimpan penerimaan barang. " . CHtml::errorSummary($terima));
                foreach ($detils as $detil) {
                    $terima_detil = new TerimaDetil;
                    $_POST['TerimaDetil']['barang_id'] = $detil['barang_id'];
                    $_POST['TerimaDetil']['sat'] = $detil['sat'];
                    $_POST['TerimaDetil']['jml'] = get_number($detil['jml']);
                    $_POST['TerimaDetil']['pcs'] = get_number($detil['pcs']);
                    $_POST['TerimaDetil']['terima_id'] = $terima->terima_id;
                    $terima_detil->attributes = $_POST['TerimaDetil'];
                    if ($_POST['TerimaDetil']['jml'] == 0) {
                        throw new Exception("Gagal menyimpan detil penerimaan barang. Jumlah tidak boleh nol.");
                    }
                    if ($_POST['TerimaDetil']['pcs'] == 0) {
                        throw new Exception("Gagal menyimpan detil penerimaan barang. Jumlah tidak boleh nol.");
                    }
                    if (!$terima_detil->save())
                        throw new Exception("Gagal menyimpan detil penerimaan barang. " . CHtml::errorSummary($terima_detil));
                }
                $po = $terima->po;
                $po->done = 1;
                if (!$po->save())
                    throw new Exception("Gagal menyimpan purchase order. " . CHtml::errorSummary($po));
                $ref->save(TERIMABARANG, $terima->terima_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Terima');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Terima'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Terima'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->terima_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->terima_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Terima')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionGetDataByPO()
    {
        $arr = Terima::getDataByPO('*', $_POST['po_id']);
        $this->renderJson($arr['model'], $arr['total']);
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['done'])) {
            $criteria->addCondition('done = :done');
            $param[':done'] = $_POST['done'];
            $criteria->order = "tgl DESC";
        }
        if (isset($_POST['terima_id'])) {
            $criteria->addCondition('terima_id = :terima_id');
            $param[':terima_id'] = $_POST['terima_id'];
        }
        if (isset($_POST['tgl_terima'])) {
            $criteria->addCondition('tgl_terima = :tgl_terima');
            $param[':tgl_terima'] = $_POST['tgl_terima'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = TerimaView::model()->findAll($criteria);
        $total = TerimaView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}