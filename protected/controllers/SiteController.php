<?php

class SiteController extends CController
{
    public $layout = 'plain';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenU::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array('class' => 'CViewAction',),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->layout = 'main';
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) echo $error['message'];
            else $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
                mail(Yii::app()->params['adminEmail'], $subject, $model->body,
                    $headers);
                Yii::app()->user->setFlash('contact',
                    'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }
    /**
     * Displays the login page
     */
    //  public function actionLogin()
//    {
//        $model = new LoginForm;
//
//        // if it is ajax validation request
//        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
//        {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//
//        // collect user input data
//        if (isset($_POST['LoginForm']))
//        {
//            $model->attributes = $_POST['LoginForm'];
//            // validate user input and redirect to the previous page if valid
//            if ($model->validate() && $model->login())
//                $this->redirect(Yii::app()->user->returnUrl);
//        }
//        // display the login form
//        $this->render('login', array('model' => $model));
//    }
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionGenerateGL()
    {
        return;
        Yii::app()->end();
        Yii::import('application.components.Gl');
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {

            /* @var $pembelian Pembelian[] */
            $pembelian = Pembelian::model()->findAll("tgl >= :from and tipe = -1",
                array(":from" => '2014-09-01'));
            foreach ($pembelian as $beli) {
                $beli->disc = 0;
                $ppn = $persedian = $nominal = 0;
                $gl = new Gl();
                foreach ($beli->pembelianDetils as $beli_detil) {
                    if ($beli_detil->barang->ppn) {
                        $ppn += ($beli_detil->nominal) * (10 / 110);
                        $persedian += ($beli_detil->nominal) * (100 / 110);
                    } else {
                        $persedian += $beli_detil->nominal;
                    }
                    $nominal += $beli_detil->nominal;
                }
                $beli->sub_total = $nominal;
                $beli->total = $nominal;
                $beli->sisa_tagihan = $nominal;
                if (!$beli->save())
                    throw new Exception("Gagal menyimpan faktur pembelian. " . CHtml::errorSummary($beli));
                $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                    PERSEDIANBARANGDAGANG, '', '', $persedian);
                if ($ppn != 0) {
                    //ppn Debet
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        PPN, '', '', $ppn);
                }
                // biaya bongkar pembelian Debet
                if ($beli->biaya_bongkar != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        BONGKAR, '', '', $beli->biaya_bongkar);
                }
                // biaya angkut pembelian Debet
                if ($beli->biaya_angkut != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        ANGKUT, '', '', $beli->biaya_angkut);
                }
                // utang dagang Kredit
                if ($beli->sisa_tagihan != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        UTANGDAGANG, '', '', -$beli->sisa_tagihan);
                }
                // Kas Kredit
                if ($beli->uang_muka != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        $beli->idBank->account_code, '', '', -$beli->uang_muka);
                }
                // potongan pembelian Kredit
                if ($beli->disc != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        POTONGAN_PEMBELIAN, '', '', -$beli->disc);
                }
                // allowance dagang Kredit
                if ($beli->allowance != 0) {
                    $gl->add_gl(RETURPEMBELIAN, $beli->pembelian_id, $beli->tgl, $beli->no_faktur_beli,
                        ALLOWANCE, '', '', -$beli->allowance);
                }
                $gl->validate();
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
    }

    public function actionGenerateStockMovesDOS()
    {
        Yii::import('application.components.U');
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /* @var $dos Dos[] */
            $dos = Dos::model()->findAll("tgl >= :from",
                array(':from' => '2014-09-01'));
            /* @var $dosdetils DosDetil[] */
//            $dosdetils = DosDetil::model()->findAll($criteria);
            foreach ($dos as $dosh) {
                foreach ($dosh->dosDetils as $dosdetil) {
                    U::add_stock_moves(DOS, $dosh->dos_id, $dosh->tgl,
                        $dosdetil->barang_id, -$dosdetil->pcs, $dosh->doc_ref,
                        0);
                    U::add_stock_moves(DOS, $dosh->dos_id, $dosh->tgl,
                        $dosdetil->barang_id, $dosdetil->pcs, $dosh->doc_ref,
                        0);
                }
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionLogin()
    {
        Yii::import('application.components.UserIdentity');
        if (!Yii::app()->request->isAjaxRequest) {
            $this->layout = 'login';
            $this->render('login');
        } else {
            $model = new LoginForm;
            $loginUsername = isset($_POST["loginUsername"]) ? $_POST["loginUsername"]
                : "";
            $loginPassword = isset($_POST["loginPassword"]) ? $_POST["loginPassword"]
                : "";
            if ($loginUsername != "") {
                //$model->attributes = $_POST['LoginForm'];
                $model->username = $loginUsername;
                $model->password = $loginPassword;
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    echo "{success: true}";
                else
                    echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
            } else {
                echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
            }
        }
        //$loginUsername = "f";
        //if ($loginUsername == "f")
//        {
//            echo "{success: true}";
//        } else
//        {
//            echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
//        }
    }

    public function actionGenerate()
    {
        $templatePath = './css/silk_v013/icons';
        $files = scandir($templatePath);
        $txt = "";
        foreach ($files as $file) {
            if (is_file($templatePath . '/' . $file)) {
                $basename = explode(".", $file);
                $name = $basename[0];
                $txt .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
            }
        }
        $myFile = "silk013.css";
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $txt);
        fclose($fh);
    }

    public function actionTree()
    {
        Yii::import('application.components.MenuTree');
        $user = Users::model()->findByPk(user()->getId());
        $menu = new MenuTree($user->role_id);
        $data = $menu->get_menu();
//        $data = "[{text: 'Penjualan',
//                  expanded: false,
//                  children:[{
//                  text: 'Faktur Penjualan',
//                  id: 'jun.NotaGrid',
//                  leaf: true
//                  },
//                  {
//                  text: 'Sales dan Pelanggan',
//                  expanded: false,
//                  children:[{
//                    text: 'Managemen Pelanggan',
//                    id: 'jun.CustomersGrid',
//                    leaf: true
//                    }]
//                  }]
//                  },
//                  {text: 'Pembelian',
//                  expanded: false,
//                  children:[{
//                    text: 'Pembelian Kredit',
//                    id: 'jun.NotaGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Pembelian Tunai',
//                    id: 'jun.NotaGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Supplier',
//                    expanded: false,
//                    children:[{
//                        text: 'Managemen Supplier',
//                        id: 'jun.SuppliersGrid',
//                        leaf: true
//                        }]
//                    }]
//                  },
//                  {text: 'Item',
//                  expanded: false,
//                  children:[{
//                    text: 'Item',
//                    id: 'jun.StockMasterGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Satuan Item',
//                    id: 'jun.ItemUnitsGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Kategori Stok',
//                    id: 'jun.StockCategoryGrid',
//                    leaf: true
//                    }]
//                  },
//                  {text: 'Kas/Bank',
//                    expanded: false,
//                    children:[{
//                        text: 'Kas Keluar',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                        text: 'Kas Masuk',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                        text: 'Transfer Antar Bank',
//                        id: 'jun.TranferBankWin',
//                        leaf: true
//                        },{
//                        text: 'Penyesuain Bank',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                         text: 'Bank Statement',
//                            id: 'jun.BankTransGrid',
//                           leaf: true
//                        },
//                        {
//                        text: 'Managemen',
//                        expanded: false,
//                        children:[{
//                            text: 'Akun Bank',
//                            id: 'jun.BankAccountsGrid',
//                            leaf: true
//                            }]
//                        }]},
//                  {
//                    text:'Akuntansi',
//                    expanded: false,
//                    children:[{
//                    text: 'Input Jurnal',
//                    id: 'jun.TblUserGrid',
//                    leaf: true
//                    },{
//                    text: 'GL Inquiry',
//                    id: 'jun.GlTransGrid',
//                    leaf: true
//                    },{
//                        text: 'Managemen',
//                        expanded: false,
//                        children:[{
//                            text: 'Quick Entries',
//                            id: 'jun.NotaGrid',
//                            leaf: true
//                            },{
//                            text: 'Akun Rekening',
//                            id: 'jun.ChartMasterGrid',
//                            leaf: true
//                            },{
//                            text: 'Grup Akun Rekening',
//                            id: 'jun.ChartTypesGrid',
//                            leaf: true
//                            },{
//                            text: 'Kelas Akun Rekening',
//                            id: 'jun.ChartClassGrid',
//                            leaf: true
//                            }]
//                        }
//                    ]},
//                    {
//                                text: 'Setting',
//                                expanded: false,
//                                children:[{
//                                    text: 'Forms Setup',
//                                    id: 'jun.SysTypesGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Profile',
//                                    id: 'jun.KlasfikasiDetailManfaatGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Ubah Password',
//                                    id: 'jun.KlasfikasiDetailManfaatGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Users',
//                                    id: 'jun.UsersGrid',
//                                    leaf: true
//                                }]
//
//                            }]";
        Yii::app()->end($data);
    }

    public function actionClosing()
    {
//        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
//            $year = $_POST['year'];
//            $tgl = $year . '-12-31';
            $tgl = $_POST['tgl'];
            $year = $tgl;
            $transaction = Yii::app()->db->beginTransaction();
            $tgl_closing = sql2date($tgl,'dd MMM yyyy');
            echo "===== MULAI CLOSING $tgl_closing =====<br/>";
            try {
                echo date("d/m/y H:i:s"), ": ", "Ambil data transaksi jurnal<br/>";
                $gltrans = Yii::app()->db->createCommand("
                SELECT ngl.account_code,COALESCE(SUM(ngl.amount),0) amount
                FROM psn_gl_trans ngl WHERE ngl.tran_date <= :tgl
                GROUP BY ngl.account_code")
                    ->queryAll(true, array(':tgl' => $tgl));
                echo date("d/m/y H:i:s"), ": ", "Ambil data transaksi kas/bank<br/>";
                $banktrans = Yii::app()->db->createCommand("
                SELECT nbt.id_bank,COALESCE(SUM(nbt.amount),0) amount
                FROM psn_bank_trans nbt WHERE nbt.trans_date <= :tgl
                GROUP BY nbt.id_bank")
                    ->queryAll(true, array(':tgl' => $tgl));
                echo date("d/m/y H:i:s"), ": ", "Ambil data mutasi barang<br/>";
                $stockmove = Yii::app()->db->createCommand("
                SELECT nsm.barang_id,SUM(nsm.qty) qty
                FROM psn_stock_moves nsm WHERE nsm.tran_date <= :tgl
                GROUP BY nsm.barang_id")
                    ->queryAll(true, array(':tgl' => $tgl));
                echo date("d/m/y H:i:s"), ": ", "Delete transaksi bank, gl, stok, penjualan<br/>";
                Yii::app()->db->createCommand("
                DELETE FROM psn_bank_trans WHERE trans_date <= :tgl;
                DELETE FROM psn_comments WHERE date_ <= :tgl;
                DELETE FROM psn_gl_trans WHERE tran_date <= :tgl;
                DELETE FROM psn_kas WHERE tgl <= :tgl;
                DELETE FROM psn_cogs WHERE tgl <= :tgl;
                DELETE FROM psn_stock_moves WHERE tran_date <= :tgl;
                DELETE FROM psn_penjualan WHERE abs(arus) = 1 AND tgl <= :tgl;
                DELETE pp1 FROM psn_penjualan pp1 JOIN(
                SELECT pp.penjualan_id,pp.total - (IF(SUM(pppd.kas_diterima) IS NULL,
                 0, SUM(pppd.kas_diterima)) + pp.uang_muka) sisa
                FROM psn_penjualan pp
	            LEFT OUTER JOIN psn_pelunasan_piutang_detil pppd ON ( pp.penjualan_id = pppd.penjualan_id  )
                WHERE pp.lunas AND pp.tgl <= :tgl
                GROUP BY pp.penjualan_id, pp.doc_ref, pp.tgl, pp.total
                HAVING sisa = 0) q ON q.penjualan_id = pp1.penjualan_id;
                DELETE ppp1 FROM psn_pelunasan_piutang ppp1 JOIN
                (SELECT ppp.pelunasan_piutang_id FROM psn_pelunasan_piutang ppp LEFT JOIN
                psn_pelunasan_piutang_detil pppd ON ppp.pelunasan_piutang_id = pppd.pelunasan_piutang_id
                WHERE ppp.tgl <= :tgl
                GROUP BY ppp.pelunasan_piutang_id
                HAVING COUNT(pppd.pelunasan_piutang_id) = 0) q
                ON q.pelunasan_piutang_id = ppp1.pelunasan_piutang_id;")
                    ->execute(array(':tgl' => $tgl));
                echo date("d/m/y H:i:s"), ": ", "Delete transaksi pembelian<br/>";
                Yii::app()->db->createCommand("
                DELETE pp.*,ppud.*,pt.*,ppo.*
                FROM psn_pembelian pp
                LEFT JOIN psn_pelunasan_utang_detil AS ppud ON  pp.pembelian_id = ppud.pembelian_id
                LEFT JOIN psn_terima AS pt ON  pt.terima_id = pp.terima_id
                LEFT JOIN psn_po AS ppo ON pt.po_id = ppo.po_id
                WHERE pp.lunas AND pp.total > 0 AND pp.tgl  <= :tgl;

                DELETE pp.*,ppud.*
                FROM psn_pembelian pp
                LEFT JOIN psn_pelunasan_utang_detil AS ppud ON  pp.pembelian_id = ppud.pembelian_id
                WHERE pp.lunas AND pp.total < 0 AND pp.tgl  <= :tgl;

                DELETE ppu1 FROM psn_pelunasan_utang ppu1 JOIN
                (SELECT ppu.pelunasan_utang_id FROM psn_pelunasan_utang ppu LEFT JOIN
                psn_pelunasan_utang_detil ppud ON ppu.pelunasan_utang_id = ppud.pelunasan_utang_id
                WHERE ppu.tgl  <= :tgl
                GROUP BY ppu.pelunasan_utang_id
                HAVING COUNT(ppud.pelunasan_utang_id) = 0) q
                ON q.pelunasan_utang_id = ppu1.pelunasan_utang_id;")
                    ->execute(array(':tgl' => $tgl));

                // CREATE TEMPORARY TABLE IF NOT EXISTS table2 AS (SELECT * FROM table1)

                $total_akun_laba_rugi = 0;
                $total_laba_berjalan = 0;
                echo date("d/m/y H:i:s"), ": ", "Input data saldo awal jurnal<br/>";
                foreach ($gltrans as $row) {
                    if ($row['amount'] == 0 || preg_match('/^(10[4-9])\w+/', $row['account_code'])) {
                        if($row['amount'] != 0){
                            $total_akun_laba_rugi += -$row['amount'];
                        }
                        continue;
                    }
                    $memo = "Closing $year";
                    if($row['account_code'] == COA_LABA_RUGI){
                        $total_laba_berjalan = $row['amount'];
                        $memo = "Closing $year, dari akun laba rugi";
                        $row['account_code']  = COA_LABA_DITAHAN;
                    }
                    $gl = new GlTrans;
                    $gl->type = SALDO_AWAL;
                    $gl->type_no = $year;
                    $gl->tran_date = $tgl;
                    $gl->memo_ = $memo;
                    $gl->amount = $row['amount'];
                    $gl->id_user = Yii::app()->user->getId();
                    $gl->account_code = $row['account_code'];
                    echo date("d/m/y H:i:s"), ": ", $row['account_code']."<br/>";
                    $gl->tdate = new CDbExpression('NOW()');
                    if (!$gl->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Journal')) . CHtml::errorSummary($gl));
                }
                $selisih_laba_berjalan = abs($total_akun_laba_rugi) - abs($total_laba_berjalan);
                echo date("d/m/y H:i:s"), ": ", "Input data saldo awal laba ditahan, selish ".number_format($selisih_laba_berjalan,2)."<br/>";
                if($selisih_laba_berjalan != 0){
                    $gl = new GlTrans;
                    $gl->type = SALDO_AWAL;
                    $gl->type_no = $year;
                    $gl->tran_date = $tgl;
                    $gl->memo_ = "Closing $year, selisih laba rugi";
                    $gl->amount = $selisih_laba_berjalan;
                    $gl->id_user = Yii::app()->user->getId();
                    $gl->account_code = COA_LABA_DITAHAN;
                    $gl->tdate = new CDbExpression('NOW()');
                    if (!$gl->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Journal')) . CHtml::errorSummary($gl));
                    $gl = new GlTrans;
                    $gl->type = SALDO_AWAL;
                    $gl->type_no = $year;
                    $gl->tran_date = $tgl;
                    $gl->memo_ = "Closing $year, selisih laba rugi";
                    $gl->amount = -$selisih_laba_berjalan;
                    $gl->id_user = Yii::app()->user->getId();
                    $gl->account_code = PENDAPATAN_LAIN_LAIN;
                    $gl->tdate = new CDbExpression('NOW()');
                    if (!$gl->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Journal')) . CHtml::errorSummary($gl));
                }
                echo date("d/m/y H:i:s"), ": ", "Input data saldo awal bank<br/>";
                foreach ($banktrans as $row) {
                    if ($row['amount'] == 0) {
                        continue;
                    }
                    $bt = new BankTrans;
                    $bt->ref = "-";
                    $bt->type = SALDO_AWAL;
                    $bt->trans_no = $year;
                    $bt->trans_date = $tgl;
                    $bt->amount = $row['amount'];
                    $bt->id_user = Yii::app()->user->getId();
                    $bt->tdate = new CDbExpression('NOW()');
                    $bt->id_bank = $row['id_bank'];
                    if (!$bt->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank Trans')) . CHtml::errorSummary($bt));
                }
                echo date("d/m/y H:i:s"), ": ", "Input data saldo awal barang<br/>";
                foreach ($stockmove as $row) {
                    if ($row['qty'] == 0) {
                        continue;
                    }
                    /* @var $barang Barang */
                    $barang = Barang::model()->findByPk($row['barang_id']);
//                    $barang = new Barang();
                    $stm = new StockMoves;
                    $stm->reference = "-";
                    $stm->type = SALDO_AWAL;
                    $stm->trans_no = $year;
                    $stm->tran_date = $tgl;
                    $stm->price = $barang->price_buy;
                    $stm->qty = $row['qty'];
                    $stm->barang_id = $row['barang_id'];
                    if (!$stm->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank Trans')) . CHtml::errorSummary($stm));
                }
                $closing = new Closing();
                $closing->tgl = $tgl;
                if (!$closing->save())
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Closing')) . CHtml::errorSummary($closing));
                $transaction->commit();
                $msg = 'Closing successfuly...';
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $msg = $ex->getMessage();
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionBackupAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = DIR_BACKUP;
        $files = scandir($dir_backup);
        foreach ($files as $file) {
            if (is_file("$dir_backup\\$file")) {
                unlink("$dir_backup\\$file");
            }
        }
        $filename = $db . date("Y-m-d-H-i-s") . '.pos';
        $backup_file = $dir_backup . $filename;
        $mysqldump = MYSQLDUMP;
        $command = "$mysqldump --opt -h $host -u $user --password=$pass -P $port " .
            "$db > $backup_file";
        system($command);
        $size = filesize($backup_file);
        if ($size > 0) {
            $zip = new ZipArchive();
            if ($zip->open("$backup_file.zip", ZIPARCHIVE::CREATE)) {
                $zip->addFile($backup_file, $filename);
                $zip->close();
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mimeType = finfo_file($finfo, "$backup_file.zip");
                $size = filesize("$backup_file.zip");
                $name = basename("$backup_file.zip");
                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                    // cache settings for IE6 on HTTPS
                    header('Cache-Control: max-age=120');
                    header('Pragma: public');
                } else {
                    header('Cache-Control: private, max-age=120, must-revalidate');
                    header("Pragma: no-cache");
                }
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // long ago
                header("Content-Type: $mimeType");
                header('Content-Disposition: attachment; filename="' . $name . '";');
                header("Accept-Ranges: bytes");
                header('Content-Length: ' . filesize("$backup_file.zip"));
                print readfile("$backup_file.zip");
                exit;
            }
//            $bu = $dir_backup . basename("$backup_file.zip");
//            header("Location: $bu");
        }
    }

    public function actionRestoreAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        if (isset($_FILES["filename"])) { // it is recommended to check file type and size here
            if ($_FILES["filename"]["error"] > 0) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $_FILES["file"]["error"]
                ));
            } else {
                $backup_file = $dir_backup . $_FILES["filename"]["name"];
                move_uploaded_file($_FILES["filename"]["tmp_name"], $backup_file);
                $mysql = dirname(Yii::app()->request->scriptFile) . "\\mysql";
                $gzip = dirname(Yii::app()->request->scriptFile) . "\\gzip";
                $command = "$gzip -d $backup_file";
                system($command);
                $backup_file = substr($backup_file, 0, -3);
                if (!file_exists($backup_file)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => "Failed restore file " . $_FILES["file"]["name"]
                    ));
                } else {
                    Yii::app()->db->createCommand("DROP DATABASE $db")
                        ->execute();
                    Yii::app()->db->createCommand("CREATE DATABASE IF NOT EXISTS $db")
                        ->execute();
                    $command = "$mysql -h $host -u $user --password=$pass -P $port " .
                        "$db < $backup_file";
                    system($command);
                    echo CJSON::encode(array(
                        'success' => true,
                        'msg' => "Succefully restore file " . $_FILES["file"]["name"]
                    ));
                }
                Yii::app()->end();
            }
        }
    }
}
