<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
class PelunasanPiutangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            Yii::import('application.components.Gl');
            $gl = new Gl();
            $is_new = $_POST['mode'] == 0;
            $pelunasan_id = $_POST['id'];
            $msg = 'Pelunasan piutang berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $pelunasan = $is_new ? new PelunasanPiutang : $this->loadModel($pelunasan_id,
                    "PelunasanPiutang");
                $docref = $pelunasan->doc_ref;
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(PELUNASANPIUTANG);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['PelunasanPiutang'][$k] = $v;
                }
                $_POST['PelunasanPiutang']['doc_ref'] = $docref;
                $pelunasan->attributes = $_POST['PelunasanPiutang'];
                if (!$pelunasan->save())
                    throw new Exception("Gagal menyimpan pelunasan piutang. ".CHtml::errorSummary($pelunasan));
                PelunasanPiutangDetil::model()->deleteAll('pelunasan_piutang_id  = :pelunasan_piutang_id',
                    array(':pelunasan_piutang_id' => $pelunasan_id));
                foreach ($detils as $detil) {
                    $pelunasan_piutang_detil = new PelunasanPiutangDetil;
                    $_POST['PelunasanPiutangDetil']['penjualan_id'] = $detil['penjualan_id'];
                    $_POST['PelunasanPiutangDetil']['kas_diterima'] = get_number($detil['kas_diterima']);
                    $_POST['PelunasanPiutangDetil']['sisa'] = get_number($detil['sisa']);
                    $_POST['PelunasanPiutangDetil']['pelunasan_piutang_id'] = $pelunasan->pelunasan_piutang_id;
                    $pelunasan_piutang_detil->attributes = $_POST['PelunasanPiutangDetil'];
                    if (!$pelunasan_piutang_detil->save())
                        throw new Exception("Gagal menyimpan detil pelunasan piutang. ".CHtml::errorSummary($pelunasan_piutang_detil));
                    if ($_POST['PelunasanPiutangDetil']['kas_diterima'] == get_number($detil['sisa'])) {
                        $jual = $this->loadModel($detil['penjualan_id'], "Penjualan");
                        $jual->lunas = 1;
                        if (!$jual->save())
                            throw new Exception("Gagal mengubah status final. ".CHtml::errorSummary($jual));
                    }
                }
                if ($is_new) {
                    $ref->save(PELUNASANPIUTANG, $pelunasan->pelunasan_piutang_id, $docref);
                }
                $gl->add_gl(PELUNASANPIUTANG, $pelunasan->pelunasan_piutang_id, $pelunasan->tgl, $pelunasan->doc_ref,
                    PIUTANGDAGANG, '', '', -$pelunasan->total);
                $gl->add_gl(PELUNASANPIUTANG, $pelunasan->pelunasan_piutang_id, $pelunasan->tgl, $pelunasan->doc_ref,
                    $pelunasan->idBank->account_code, '', '', $pelunasan->total);
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PelunasanPiutang');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanPiutang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PelunasanPiutang'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_piutang_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pelunasan_piutang_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PelunasanPiutang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['konsumen_id'])){
            $criteria->addCondition("konsumen_id = :konsumen_id");
            $param[':konsumen_id'] = $_POST['konsumen_id'];
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = PelunasanPiutangView::model()->findAll($criteria);
        $total = PelunasanPiutangView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}