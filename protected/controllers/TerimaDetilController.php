<?php
class TerimaDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new TerimaDetil;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['TerimaDetil'][$k] = $v;
            }
            $model->attributes = $_POST['TerimaDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->terima_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'TerimaDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['TerimaDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['TerimaDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->terima_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->terima_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'TerimaDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        $terima = Terima::model()->find('po_id = :po_id',array(':po_id'=>$_POST['po_id']));
        if (isset($_POST['po_id'])) {
            $criteria = new CDbCriteria();
            $criteria->select = "barang_id,sat,jml,pcs";
            $criteria->addCondition('po_id = :po_id');
            $criteria->params = array(':po_id' => $_POST['po_id']);
            $model = PoDetil::model()->findAll($criteria);
            $total = PoDetil::model()->count($criteria);
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
        if (isset($_POST['terima_id'])) {
//            $terima_id = $terima->terima_id;
            $model = TerimaDetil::model()->findAll('terima_id = :terima_id', array(':terima_id' => $_POST['terima_id']));
            $total = TerimaDetil::model()->count('terima_id = :terima_id', array(':terima_id' => $_POST['terima_id']));
            $this->renderJson($model, $total);
        }
        Yii::app()->end();
    }
}