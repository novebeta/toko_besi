<?php
class PelunasanUtangDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new PelunasanUtangDetil;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanUtangDetil'][$k] = $v;
            }
            $model->attributes = $_POST['PelunasanUtangDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_utang_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PelunasanUtangDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanUtangDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PelunasanUtangDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_utang_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pelunasan_utang_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PelunasanUtangDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionFaktur(){
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['supplier_id'])){
            $criteria->addCondition('supplier_id = :supplier_id');
            $param[':supplier_id'] = $_POST['supplier_id'];
        }
        $criteria->params = $param;
        $model = DaftarFakturUtang::model()->findAll($criteria);
        $total = DaftarFakturUtang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndex()
    {
//        $model = PelunasanUtangDetil::get_detil($_POST['pelunasan_utang_id']);
//        $this->renderJsonArr($model);
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['pelunasan_utang_id'])){
            $criteria->addCondition('pelunasan_utang_id = :pelunasan_utang_id');
            $param[':pelunasan_utang_id'] = $_POST['pelunasan_utang_id'];
        }
        $criteria->params = $param;
        $model = PelunasanUtangDetilView::model()->findAll($criteria);
        $total = PelunasanUtangDetilView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}