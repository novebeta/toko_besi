<?php
class TypeKonsumenController extends GxController {
    public function actionCreate() {
        $model = new TypeKonsumen;


        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['TypeKonsumen'][$k] = $v;
            }
            $model->attributes = $_POST['TypeKonsumen'];
            $msg = "Data gagal disimpan";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->type_konsumen_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'TypeKonsumen');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['TypeKonsumen'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['TypeKonsumen'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->type_konsumen_id;
            } else {
                $msg .= " ".CHtml::errorSummary($model);
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->type_konsumen_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['type_konsumen_code'])) {
            $criteria->addCondition("type_konsumen_code like :type_konsumen_code");
            $param[':type_konsumen_code'] = "%" . $_POST['type_konsumen_code'] . "%";
        }
        if (isset($_POST['type_konsumen_name'])) {
            $criteria->addCondition("type_konsumen_name like :type_konsumen_name");
            $param[':type_konsumen_name'] = "%" . $_POST['type_konsumen_name'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        if (isset($_POST['mode']) && $_POST['mode'] == 'active') {
            $criteria->addColumnCondition(array('status' => 1));
        } else if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) || isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = TypeKonsumen::model()->findAll($criteria);
        $total = TypeKonsumen::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}