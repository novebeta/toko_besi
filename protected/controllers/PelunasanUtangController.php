<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
class PelunasanUtangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            Yii::import('application.components.Gl');
            $gl = new Gl();
            $msg = 'Pelunasan utang berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $pelunasan = new PelunasanUtang;
                $docref = $pelunasan->doc_ref;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PELUNASANUTANG);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['PelunasanUtang'][$k] = $v;
                }
                $_POST['PelunasanUtang']['doc_ref'] = $docref;
                $pelunasan->attributes = $_POST['PelunasanUtang'];
                if (!$pelunasan->save())
                    throw new Exception("Gagal menyimpan pelunasan utang. " . CHtml::errorSummary($pelunasan));
                PelunasanUtangDetil::model()->deleteAll('pelunasan_utang_id  = :pelunasan_utang_id',
                    array(':pelunasan_utang_id' => $pelunasan->pelunasan_utang_id));
                foreach ($detils as $detil) {
                    $pelunasan_utang_detil = new PelunasanUtangDetil;
                    $_POST['PelunasanUtangDetil']['pembelian_id'] = $detil['pembelian_id'];
                    $_POST['PelunasanUtangDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
                    $_POST['PelunasanUtangDetil']['sisa'] = get_number($detil['sisa']);
                    $_POST['PelunasanUtangDetil']['pelunasan_utang_id'] = $pelunasan->pelunasan_utang_id;
                    $pelunasan_utang_detil->attributes = $_POST['PelunasanUtangDetil'];
                    if (!$pelunasan_utang_detil->save())
                        throw new Exception("Gagal menyimpan detil pelunasan utang. " . CHtml::errorSummary($pelunasan_utang_detil));
                    if ($_POST['PelunasanUtangDetil']['kas_dibayar'] == get_number($detil['sisa'])) {
                        /** @var $trans Pembelian*/
                        $trans = $this->loadModel($pelunasan_utang_detil->pembelian_id, "Pembelian");
                        $trans->lunas = 1;
                        if (!$trans->save())
                            throw new Exception("Gagal mengubah status final. " . CHtml::errorSummary($trans));
                    }
                }
                $ref->save(PELUNASANUTANG, $pelunasan->pelunasan_utang_id, $docref);
                //Utang Debet
                $gl->add_gl(PELUNASANUTANG, $pelunasan->pelunasan_utang_id, $pelunasan->tgl, $pelunasan->doc_ref,
                    UTANGDAGANG, '', '', $pelunasan->total);
                //Kas Kredit
                $gl->add_gl(PELUNASANUTANG, $pelunasan->pelunasan_utang_id, $pelunasan->tgl, $pelunasan->doc_ref,
                    $pelunasan->idBank->account_code, '', '', -$pelunasan->total);
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['supplier_id'])){
            $criteria->addCondition("supplier_id = :supplier_id");
            $param[':supplier_id'] = $_POST['supplier_id'];
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = PelunasanUtangView::model()->findAll($criteria);
        $total = PelunasanUtangView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}