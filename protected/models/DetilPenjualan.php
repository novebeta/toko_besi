<?php

Yii::import('application.models._base.BaseDetilPenjualan');

class DetilPenjualan extends BaseDetilPenjualan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
	{
		if ($this->detil_penjualan == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->detil_penjualan = $uuid;
		}
		return parent::beforeValidate();
	}
}