<?php

Yii::import('application.models._base.BaseKas');
class Kas extends BaseKas
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->kas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function get_kas_kecil_id()
    {
        $res = Bank::model()->find("nama_akun like '%Kas Kecil%'");
        if ($res != null) {
            return $res->id_bank;
        } else {
            return -1;
        }
    }
}