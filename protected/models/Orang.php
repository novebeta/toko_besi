<?php

Yii::import('application.models._base.BaseOrang');

class Orang extends BaseOrang
{

	public function beforeValidate(){
		if ($this->orang_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->orang_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}