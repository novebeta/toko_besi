<?php
Yii::import('application.models._base.BaseReturJual');
class ReturJual extends BaseReturJual
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->retur_jual_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->retur_jual_id = $uuid;
        }
        return parent::beforeValidate();
    }
}