<?php

Yii::import('application.models._base.BasePelunasanUtangDetil');
class PelunasanUtangDetil extends BasePelunasanUtangDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->pelunasan_utang_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pelunasan_utang_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_detil($pelunasan_utang_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT COALESCE(pp.tgl,pnd.tgl) tgl,COALESCE(pp.no_faktur_beli,pnd.doc_ref) no_faktur,
        COALESCE(pp.sub_total,pnd.total) nilai,ppud.sisa, ppud.kas_dibayar
        FROM
        psn_pelunasan_utang_detil AS ppud
        LEFT JOIN psn_pembelian AS pp ON ppud. = pp.pembelian_id
        WHERE ppud.pelunasan_utang_id = :pelunasan_utang_id");
        return $comm->queryAll(true, array(':pelunasan_utang_id' => $pelunasan_utang_id));
    }
}