<?php
Yii::import('application.models._base.BaseSecurityRoles');
class SecurityRoles extends BaseSecurityRoles
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function have_role($id)
    {
        $user = Yii::app()->user->getId();
        $comm = Yii::app()->db->createCommand("SELECT psr.id
        FROM psn_security_roles psr
	    INNER JOIN psn_users pu ON psr.id = pu.role_id
        WHERE psr.sections LIKE :id AND pu.id_user = :id_user");
        $res = $comm->queryAll(true, array(':id' => '%' . $id . '%', ':id_user' => $user));
        return count($res) > 0 ? "true" : "false";
    }
}