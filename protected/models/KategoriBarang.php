<?php

Yii::import('application.models._base.BaseKategoriBarang');

class KategoriBarang extends BaseKategoriBarang
{

	public function beforeValidate(){
		if ($this->kategori_barang_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->kategori_barang_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}