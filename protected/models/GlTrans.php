<?php

Yii::import('application.models._base.BaseGlTrans');

class GlTrans extends BaseGlTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
    {
        if ($this->counter == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->counter = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function jurnal_umum_index($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ngt.type_no,ngt.tran_date,ngt.amount tot_debit,ngt.amount tot_kredit,
        pr.reference
        FROM psn_gl_trans AS ngt
        INNER JOIN psn_refs as pr ON ngt.type = pr.type AND ngt.type_no = pr.type_no
        WHERE ngt.type = :type AND ngt.amount > 0 AND ngt.tran_date = :tgl");
        return $comm->queryAll(true, array(':type' => JURNAL_UMUM, ':tgl' => $tgl));
    }
    public static function jurnal_umum_details($type_no)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ngt.type_no,ngt.tran_date,ngt.amount,
        if(ngt.amount >= 0,ngt.amount,0) debit,
        if(ngt.amount < 0,-ngt.amount,0) kredit,
        ngt.counter,ngt.memo_,ngt.account_code account
        FROM psn_gl_trans AS ngt
        WHERE ngt.type = :type AND ngt.type_no = :type_no");
        return $comm->queryAll(true, array(':type' => JURNAL_UMUM, ':type_no' => $type_no));
    }
}