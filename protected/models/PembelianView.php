<?php

Yii::import('application.models._base.BasePembelianView');

class PembelianView extends BasePembelianView
{
	public function primaryKey()
	{
		return 'pembelian_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}