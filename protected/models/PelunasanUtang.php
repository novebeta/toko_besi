<?php

Yii::import('application.models._base.BasePelunasanUtang');

class PelunasanUtang extends BasePelunasanUtang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->pelunasan_utang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pelunasan_utang_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}