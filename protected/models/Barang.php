<?php

Yii::import('application.models._base.BaseBarang');
class Barang extends BaseBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate(){
        if ($this->barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function get_hpp($qty_pcs)
    {
        return $this->price_buy * $qty_pcs;
    }
}