<?php

Yii::import('application.models._base.BaseBank');

class Bank extends BaseBank
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function saldo_before($tgl){
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(pbt.amount),0)
            FROM psn_bank_trans AS pbt
            WHERE pbt.id_bank = :bank_id AND pbt.trans_date < :tgl");
        return $comm->queryScalar(array(':bank_id'=>$this->id_bank,':tgl'=>$tgl));
    }

    public function beforeValidate(){
        if ($this->id_bank == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id_bank = $uuid;
        }
        return parent::beforeValidate();
    }
}