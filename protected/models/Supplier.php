<?php
Yii::import('application.models._base.BaseSupplier');
class Supplier extends BaseSupplier
{
    public function primaryKey()
    {
        return 'orang_id';
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function total_hutang_before($from)
    {
        $param = array(':from' => $from);
        $param[':supplier_id'] = $this->orang_id;
        $res = app()->db->createCommand("SELECT SUM(c.total) FROM (
(SELECT SUM(nti.total) total
	FROM psn_pembelian AS nti WHERE nti.supplier_id = :supplier_id AND nti.tgl < :from)
UNION
(SELECT SUM(-nti.total) FROM psn_pembelian AS nti
        WHERE nti.supplier_id = :supplier_id AND nti.tipe = -1 AND nti.tgl >= :from)
UNION
(SELECT -SUM(npud.kas_dibayar) total
FROM psn_pembelian AS nti
  INNER JOIN psn_pelunasan_utang_detil AS npud
			ON nti.pembelian_id = npud.pembelian_id
  INNER JOIN psn_pelunasan_utang AS npu
			ON npud.pelunasan_utang_id = npu.pelunasan_utang_id WHERE nti.supplier_id = :supplier_id
			 AND npu.tgl < :from)) c");
        return $res->queryScalar($param);
    }
    public function get_pelunasan($from, $to)
    {
        $param = array(':from' => $from, ':to' => $to);
        $param[':supplier_id'] = $this->orang_id;
        $res = app()->db->createCommand("SELECT nti.tgl,nti.no_faktur_beli,'DEBT' note,nti.no_faktur_supp no_faktur,
        nti.total hutang,0 payment,nti.total FROM psn_pembelian AS nti WHERE
        nti.supplier_id = :supplier_id AND nti.tgl >= :from AND nti.tgl <= :to
        UNION
        (SELECT nti.tgl,nti.no_faktur_beli,'PAYEMNT' note,nti.no_faktur_supp no_faktur,
        0 hutang,-nti.total payment,nti.total FROM psn_pembelian AS nti
        WHERE nti.supplier_id = :supplier_id AND nti.tipe = -1 AND nti.tgl >= :from AND nti.tgl <= :to)
        UNION
        (SELECT npu.tgl,npu.doc_ref,'PAYEMNT' note,nti.no_faktur_supp no_faktur,0 hutang,npud.kas_dibayar payment,
        -npud.kas_dibayar total FROM psn_pembelian AS nti
        INNER JOIN psn_pelunasan_utang_detil AS npud
			ON nti.pembelian_id = npud.pembelian_id
        INNER JOIN psn_pelunasan_utang AS npu
			ON npud.pelunasan_utang_id = npu.pelunasan_utang_id
        WHERE nti.supplier_id = :supplier_id AND npu.tgl >= :from AND npu.tgl <= :to)
        ORDER BY tgl");
        return $res->queryAll(true, $param);
    }
}