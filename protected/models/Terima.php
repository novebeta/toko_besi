<?php

Yii::import('application.models._base.BaseTerima');
class Terima extends BaseTerima
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getDataByPO($select, $id)
    {
        $criteria = new CDbCriteria();
        $criteria->select = $select;
        $criteria->addCondition('po_id = :po_id');
        $criteria->params = array('po_id' => $id);
        $model = Terima::model()->findAll($criteria);
        $total = Terima::model()->count($criteria);
        return array('model' => $model, 'total' => $total);
    }
    public function beforeValidate()
    {
        if ($this->terima_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function LapTerimaBarang($from, $to, $supplier_id){
        $jenis = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($supplier_id != null) {
            $jenis = "WHERE ptv.supplier_id = :supplier_id";
            $param[':supplier_id'] = $supplier_id;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT ptv.no_lpb,DATE_FORMAT(ptv.tgl_terima,'%d %b %Y') AS tgl_terima,
            ptv.no_po,ps.nama,pb.barcode,pb.barang_name,ptd.jml,
            ptd.sat,ptd.pcs,pb.satuan
            FROM psn_terima_view AS ptv
            INNER JOIN psn_terima_detil AS ptd ON ptd.terima_id = ptv.terima_id
            INNER JOIN psn_supplier AS ps ON ptv.supplier_id = ps.orang_id
            INNER JOIN psn_barang AS pb ON ptd.barang_id = pb.barang_id
            $jenis
            ORDER BY no_lpb");
        return $comm->queryAll(true, $param);
    }
}