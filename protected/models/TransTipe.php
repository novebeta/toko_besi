<?php

Yii::import('application.models._base.BaseTransTipe');

class TransTipe extends BaseTransTipe
{

	public function beforeValidate(){
		if ($this->tipe_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->tipe_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}