<?php

Yii::import('application.models._base.BaseSysPrefs');

class SysPrefs extends BaseSysPrefs
{

	public function beforeValidate(){
		if ($this->sys_pref_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->sys_pref_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public static function get_val($index)
	{
		/** @var $pref SysPrefs */
		$pref = SysPrefs::model()->find('name_ = :name_',
			array(':name_' => $index));
		if($pref == null){
			return null;
		}
		return $pref->value;
	}
	public static function save_value($name_, $value_, $store)
	{
		$comm = Yii::app()->db->createCommand(
			"REPLACE INTO nscc_sys_prefs (name_, value_, store, up)
                VALUES (:name_, :value_, :store, 0)"
		);
		return $comm->execute(array(':name_' => $name_, ':value_' => $value_, ':store' => $store));
	}
}