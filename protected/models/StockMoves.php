<?php

Yii::import('application.models._base.BaseStockMoves');

class StockMoves extends BaseStockMoves
{
    public function beforeValidate()
    {
        if ($this->trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->trans_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function count_barang($id){
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(psm.qty),0) FROM
            psn_stock_moves AS psm WHERE psm.barang_id = :barang_id");
        return $comm->queryScalar(array(':barang_id'=>$id));
    }
    public static function get_saldo_item_before($barang_id,$tgl){
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM psn_stock_moves AS nsm
        INNER JOIN psn_barang nb ON nsm.barang_id = nb.barang_id
        WHERE  nb.barang_id = :barang_id AND nsm.tran_date < :tgl");
        return $comm->queryScalar(array(':barang_id'=>$barang_id,':tgl'=>$tgl));
    }
    public static function get_saldo_item($barang_id,$gudang_id = GUDANG_PST){
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM psn_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.gudang_id = :gudang_id");
        return $comm->queryScalar(array(':barang_id'=>$barang_id, ':gudang_id' => $gudang_id));
    }
}