<?php

Yii::import('application.models._base.BasePembelian');

class Pembelian extends BasePembelian
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function getByPO($id){
        $comm = Yii::app()->db->createCommand("SELECT pp.pembelian_id,
            pp.no_faktur_beli,pp.tgl,pp.tempo,IFNULL(pp.sub_total,0) sub_total,
            IFNULL(pp.total,0) total,IFNULL(pp.uang_muka,0) uang_muka,
            pp.no_bg_cek,IFNULL(pp.sisa_tagihan,0) sisa_tagihan,
            pp.lunas,pp.final,pp.id_user,IFNULL(pp.disc,0) disc,
            pp.no_faktur_supp,pp.tgl_tempo,pt.terima_id ref_id,
            pp.id_bank,IFNULL(pp.biaya_bongkar,0) biaya_bongkar,
            IFNULL(pp.biaya_angkut,0) biaya_angkut,
            IFNULL(pp.allowance,0) allowance, pt.no_lpb,po.no_po,po.supplier_id
            FROM {{pembelian}} pp RIGHT JOIN {{terima}} pt ON pp.total >= 0 AND pp.ref_id = pt.terima_id
            RIGHT JOIN {{po}} po ON pt.po_id = po.po_id
            WHERE po.po_id = :po_id");
        return $comm->queryRow(true,array(':po_id'=>$id));
    }
    public function beforeValidate()
    {
        if ($this->pembelian_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pembelian_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}