<?php

Yii::import('application.models._base.BasePelunasanPiutangDetil');

class PelunasanPiutangDetil extends BasePelunasanPiutangDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
	{
		if ($this->pelunasan_piutang_detil_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->pelunasan_piutang_detil_id = $uuid;
		}
		return parent::beforeValidate();
	}
}