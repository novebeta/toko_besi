<?php

Yii::import('application.models._base.BasePelunasanPiutang');

class PelunasanPiutang extends BasePelunasanPiutang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
    {
        if ($this->pelunasan_piutang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pelunasan_piutang_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}