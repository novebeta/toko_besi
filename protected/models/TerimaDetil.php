<?php

Yii::import('application.models._base.BaseTerimaDetil');

class TerimaDetil extends BaseTerimaDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
	{
		if ($this->terima_detil_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->terima_detil_id = $uuid;
		}
		return parent::beforeValidate();
	}
}