<?php

Yii::import('application.models._base.BaseKasDtl');

class KasDtl extends BaseKasDtl
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
	{
		if ($this->kas_dtl_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->kas_dtl_id = $uuid;
		}
		return parent::beforeValidate();
	}
}