<?php
Yii::import('application.models._base.BaseReturJualDetil');
class ReturJualDetil extends BaseReturJualDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->retur_jual_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->retur_jual_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}