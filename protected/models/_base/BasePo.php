<?php

/**
 * This is the model base class for the table "{{po}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Po".
 *
 * Columns in table "{{po}}" available as properties of the model,
 * followed by relations of table "{{po}}" available as properties of the model.
 *
 * @property string $po_id
 * @property string $no_po
 * @property string $tgl
 * @property string $tgl_butuh
 * @property string $id_user
 * @property integer $done
 * @property string $tdate
 * @property string $supplier_id
 *
 * @property Orang $supplier
 * @property PoDetil[] $poDetils
 * @property Terima[] $terimas
 */
abstract class BasePo extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{po}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('po_id, tgl, tgl_butuh, id_user, tdate, supplier_id', 'required'),
			array('done', 'numerical', 'integerOnly'=>true),
			array('po_id, id_user, supplier_id', 'length', 'max'=>36),
			array('no_po', 'length', 'max'=>15),
			array('no_po, done', 'default', 'setOnEmpty' => true, 'value' => null),
			array('po_id, no_po, tgl, tgl_butuh, id_user, done, tdate, supplier_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'supplier' => array(self::BELONGS_TO, 'Orang', 'supplier_id'),
			'poDetils' => array(self::HAS_MANY, 'PoDetil', 'po_id'),
			'terimas' => array(self::HAS_MANY, 'Terima', 'po_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'po_id' => Yii::t('app', 'Po'),
			'no_po' => Yii::t('app', 'No Po'),
			'tgl' => Yii::t('app', 'Tgl'),
			'tgl_butuh' => Yii::t('app', 'Tgl Butuh'),
			'id_user' => Yii::t('app', 'Id User'),
			'done' => Yii::t('app', 'Done'),
			'tdate' => Yii::t('app', 'Tdate'),
			'supplier_id' => Yii::t('app', 'Supplier'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('po_id', $this->po_id, true);
		$criteria->compare('no_po', $this->no_po, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('tgl_butuh', $this->tgl_butuh, true);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('done', $this->done);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('supplier_id', $this->supplier_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}