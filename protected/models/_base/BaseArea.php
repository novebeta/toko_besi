<?php

/**
 * This is the model base class for the table "{{area}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Area".
 *
 * Columns in table "{{area}}" available as properties of the model,
 * followed by relations of table "{{area}}" available as properties of the model.
 *
 * @property integer $area_id
 * @property string $area_code
 * @property string $area_name
 * @property integer $status
 *
 * @property Konsumen[] $konsumens
 */
abstract class BaseArea extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{area}}';
	}

	public static function representingColumn() {
		return 'area_code';
	}

	public function rules() {
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('area_code', 'length', 'max'=>20),
			array('area_name', 'length', 'max'=>100),
			array('area_code, area_name, status', 'default', 'setOnEmpty' => true, 'value' => null),
			array('area_id, area_code, area_name, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'konsumens' => array(self::HAS_MANY, 'Konsumen', 'area_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'area_id' => Yii::t('app', 'Area'),
			'area_code' => Yii::t('app', 'Area Code'),
			'area_name' => Yii::t('app', 'Area Name'),
			'status' => Yii::t('app', 'Status'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('area_id', $this->area_id);
		$criteria->compare('area_code', $this->area_code, true);
		$criteria->compare('area_name', $this->area_name, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}