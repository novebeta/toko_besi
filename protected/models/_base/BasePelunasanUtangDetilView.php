<?php

/**
 * This is the model base class for the table "{{pelunasan_utang_detil_view}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PelunasanUtangDetilView".
 *
 * Columns in table "{{pelunasan_utang_detil_view}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $pelunasan_utang_detil_id
 * @property double $kas_dibayar
 * @property string $pelunasan_utang_id
 * @property double $sisa
 * @property string $pembelian_id
 * @property string $no_faktur_beli
 * @property string $tgl
 * @property double $total
 *
 */
abstract class BasePelunasanUtangDetilView extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pelunasan_utang_detil_view}}';
	}

	public static function representingColumn() {
		return 'pelunasan_utang_detil_id';
	}

	public function rules() {
		return array(
			array('pelunasan_utang_detil_id, pelunasan_utang_id, pembelian_id, tgl', 'required'),
			array('kas_dibayar, sisa, total', 'numerical'),
			array('pelunasan_utang_detil_id, pelunasan_utang_id, pembelian_id', 'length', 'max'=>36),
			array('no_faktur_beli', 'length', 'max'=>15),
			array('kas_dibayar, sisa, no_faktur_beli, total', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pelunasan_utang_detil_id, kas_dibayar, pelunasan_utang_id, sisa, pembelian_id, no_faktur_beli, tgl, total', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pelunasan_utang_detil_id' => Yii::t('app', 'Pelunasan Utang Detil'),
			'kas_dibayar' => Yii::t('app', 'Kas Dibayar'),
			'pelunasan_utang_id' => Yii::t('app', 'Pelunasan Utang'),
			'sisa' => Yii::t('app', 'Sisa'),
			'pembelian_id' => Yii::t('app', 'Pembelian'),
			'no_faktur_beli' => Yii::t('app', 'No Faktur Beli'),
			'tgl' => Yii::t('app', 'Tgl'),
			'total' => Yii::t('app', 'Total'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pelunasan_utang_detil_id', $this->pelunasan_utang_detil_id, true);
		$criteria->compare('kas_dibayar', $this->kas_dibayar);
		$criteria->compare('pelunasan_utang_id', $this->pelunasan_utang_id, true);
		$criteria->compare('sisa', $this->sisa);
		$criteria->compare('pembelian_id', $this->pembelian_id, true);
		$criteria->compare('no_faktur_beli', $this->no_faktur_beli, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('total', $this->total);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}