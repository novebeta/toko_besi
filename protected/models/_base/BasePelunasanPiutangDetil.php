<?php

/**
 * This is the model base class for the table "{{pelunasan_piutang_detil}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PelunasanPiutangDetil".
 *
 * Columns in table "{{pelunasan_piutang_detil}}" available as properties of the model,
 * followed by relations of table "{{pelunasan_piutang_detil}}" available as properties of the model.
 *
 * @property string $pelunasan_piutang_detil_id
 * @property double $sisa
 * @property double $kas_diterima
 * @property string $pelunasan_piutang_id
 * @property string $penjualan_id
 *
 * @property Penjualan $penjualan
 * @property PelunasanPiutang $pelunasanPiutang
 */
abstract class BasePelunasanPiutangDetil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pelunasan_piutang_detil}}';
	}

	public static function representingColumn() {
		return 'pelunasan_piutang_detil_id';
	}

	public function rules() {
		return array(
			array('pelunasan_piutang_detil_id, pelunasan_piutang_id, penjualan_id', 'required'),
			array('sisa, kas_diterima', 'numerical'),
			array('pelunasan_piutang_detil_id, pelunasan_piutang_id, penjualan_id', 'length', 'max'=>36),
			array('sisa, kas_diterima', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pelunasan_piutang_detil_id, sisa, kas_diterima, pelunasan_piutang_id, penjualan_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'penjualan' => array(self::BELONGS_TO, 'Penjualan', 'penjualan_id'),
			'pelunasanPiutang' => array(self::BELONGS_TO, 'PelunasanPiutang', 'pelunasan_piutang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pelunasan_piutang_detil_id' => Yii::t('app', 'Pelunasan Piutang Detil'),
			'sisa' => Yii::t('app', 'Sisa'),
			'kas_diterima' => Yii::t('app', 'Kas Diterima'),
			'pelunasan_piutang_id' => Yii::t('app', 'Pelunasan Piutang'),
			'penjualan_id' => Yii::t('app', 'Penjualan'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pelunasan_piutang_detil_id', $this->pelunasan_piutang_detil_id, true);
		$criteria->compare('sisa', $this->sisa);
		$criteria->compare('kas_diterima', $this->kas_diterima);
		$criteria->compare('pelunasan_piutang_id', $this->pelunasan_piutang_id);
		$criteria->compare('penjualan_id', $this->penjualan_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}