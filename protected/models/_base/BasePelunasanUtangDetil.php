<?php

/**
 * This is the model base class for the table "{{pelunasan_utang_detil}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PelunasanUtangDetil".
 *
 * Columns in table "{{pelunasan_utang_detil}}" available as properties of the model,
 * followed by relations of table "{{pelunasan_utang_detil}}" available as properties of the model.
 *
 * @property string $pelunasan_utang_detil_id
 * @property double $kas_dibayar
 * @property string $pelunasan_utang_id
 * @property double $sisa
 * @property string $pembelian_id
 *
 * @property Pembelian $pembelian
 * @property PelunasanUtang $pelunasanUtang
 */
abstract class BasePelunasanUtangDetil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pelunasan_utang_detil}}';
	}

	public static function representingColumn() {
		return 'pelunasan_utang_detil_id';
	}

	public function rules() {
		return array(
			array('pelunasan_utang_detil_id, pelunasan_utang_id, pembelian_id', 'required'),
			array('kas_dibayar, sisa', 'numerical'),
			array('pelunasan_utang_detil_id, pelunasan_utang_id, pembelian_id', 'length', 'max'=>36),
			array('kas_dibayar, sisa', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pelunasan_utang_detil_id, kas_dibayar, pelunasan_utang_id, sisa, pembelian_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'pembelian' => array(self::BELONGS_TO, 'Pembelian', 'pembelian_id'),
			'pelunasanUtang' => array(self::BELONGS_TO, 'PelunasanUtang', 'pelunasan_utang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pelunasan_utang_detil_id' => Yii::t('app', 'Pelunasan Utang Detil'),
			'kas_dibayar' => Yii::t('app', 'Kas Dibayar'),
			'pelunasan_utang_id' => Yii::t('app', 'Pelunasan Utang'),
			'sisa' => Yii::t('app', 'Sisa'),
			'pembelian_id' => Yii::t('app', 'Pembelian'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pelunasan_utang_detil_id', $this->pelunasan_utang_detil_id, true);
		$criteria->compare('kas_dibayar', $this->kas_dibayar);
		$criteria->compare('pelunasan_utang_id', $this->pelunasan_utang_id);
		$criteria->compare('sisa', $this->sisa);
		$criteria->compare('pembelian_id', $this->pembelian_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}