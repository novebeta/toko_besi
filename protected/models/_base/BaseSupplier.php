<?php

/**
 * This is the model base class for the table "{{supplier}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Supplier".
 *
 * Columns in table "{{supplier}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $orang_id
 * @property string $kode
 * @property string $nama
 * @property string $alamat
 * @property string $phone
 * @property string $bagian
 * @property integer $type_
 *
 */
abstract class BaseSupplier extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{supplier}}';
	}

	public static function representingColumn() {
		return 'orang_id';
	}

	public function rules() {
		return array(
			array('orang_id, nama', 'required'),
			array('type_', 'numerical', 'integerOnly'=>true),
			array('orang_id', 'length', 'max'=>36),
			array('kode', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>100),
			array('phone', 'length', 'max'=>25),
			array('bagian', 'length', 'max'=>50),
			array('alamat', 'safe'),
			array('kode, alamat, phone, bagian, type_', 'default', 'setOnEmpty' => true, 'value' => null),
			array('orang_id, kode, nama, alamat, phone, bagian, type_', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'orang_id' => Yii::t('app', 'Orang'),
			'kode' => Yii::t('app', 'Kode'),
			'nama' => Yii::t('app', 'Nama'),
			'alamat' => Yii::t('app', 'Alamat'),
			'phone' => Yii::t('app', 'Phone'),
			'bagian' => Yii::t('app', 'Bagian'),
			'type_' => Yii::t('app', 'Type'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('orang_id', $this->orang_id, true);
		$criteria->compare('kode', $this->kode, true);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('alamat', $this->alamat, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('bagian', $this->bagian, true);
		$criteria->compare('type_', $this->type_);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}