<?php

Yii::import('application.models._base.BaseKaryawan');

class Karyawan extends BaseKaryawan
{

	public function primaryKey()
	{
		return 'orang_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}