<?php

Yii::import('application.models._base.BaseBankTrans');
class BankTrans extends BaseBankTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->bank_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
}