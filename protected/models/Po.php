<?php

Yii::import('application.models._base.BasePo');
class Po extends BasePo
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->po_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}