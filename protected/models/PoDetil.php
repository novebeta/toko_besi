<?php

Yii::import('application.models._base.BasePoDetil');

class PoDetil extends BasePoDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
	{
		if ($this->po_detil_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->po_detil_id = $uuid;
		}
		return parent::beforeValidate();
	}
}