<?php

Yii::import('application.models._base.BasePembelianDetil');

class PembelianDetil extends BasePembelianDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function getByPembelianId($id){
        $criteria = new CDbCriteria();
        $criteria->addCondition('pembelian_id = :pembelian_id');
        $criteria->params = array(':pembelian_id'=>$id);
        $model = PembelianDetil::model()->findAll($criteria);
        $total = PembelianDetil::model()->count($criteria);
        return array('model'=>$model,'total'=>$total);
    }
    public static function getByTerimaId($id){
        $command = Yii::app()->db->createCommand("
        SELECT ptd.barang_id,ptd.sat,ptd.jml,0 price,0 disc1,0 disc2,0 pot,0 nominal,ptd.pcs,
            0 totalpot,0 bruto
        FROM psn_terima_detil AS ptd
            INNER JOIN psn_terima AS pt ON ptd.terima_id = pt.terima_id
            INNER JOIN psn_po AS po ON pt.po_id = po.po_id
        WHERE pt.terima_id = :id");
        return $command->queryAll(true,array(':id'=>$id));
    }
    public function beforeValidate()
    {
        if ($this->pembelian_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pembelian_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}