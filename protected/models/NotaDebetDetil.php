<?php
Yii::import('application.models._base.BaseNotaDebetDetil');
class NotaDebetDetil extends BaseNotaDebetDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->nota_debet_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->nota_debet_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}