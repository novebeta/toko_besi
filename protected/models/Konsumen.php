<?php

Yii::import('application.models._base.BaseKonsumen');

class Konsumen extends BaseKonsumen
{

	public function primaryKey()
	{
		return 'orang_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function total_piutang_before($from)
	{
		$param = array(':from' => $from);
		$param[':customer_id'] = $this->orang_id;
		$res = app()->db->createCommand("SELECT SUM(c.total) FROM (
(SELECT SUM(nti.total) total
	FROM psn_penjualan_kredit AS nti WHERE nti.konsumen_id = :customer_id AND nti.tgl < :from)
UNION
(SELECT SUM(-nti.total) FROM psn_penjualan_kredit AS nti
        WHERE nti.konsumen_id = :customer_id AND nti.arus = -2 AND nti.tgl >= :from)
UNION
(SELECT -SUM(npud.kas_diterima) AS total
FROM psn_penjualan_kredit AS nti
  INNER JOIN psn_pelunasan_piutang_detil AS npud
			ON nti.penjualan_id = npud.penjualan_id
  INNER JOIN psn_pelunasan_piutang AS npu
			ON npud.pelunasan_piutang_id = npu.pelunasan_piutang_id WHERE nti.konsumen_id = :customer_id
			 AND npu.tgl < :from)) c");
		return $res->queryScalar($param);
	}

	public function get_pelunasan($from,$to){
//		$where = "WHERE ";
		$param = array(':from' => $from,':to'=>$to);
		$param[':customer_id'] = $this->orang_id;

		$res = app()->db->createCommand("SELECT nti.tgl,nti.doc_ref,'DEBT' note,nti.doc_ref no_faktur,
        nti.total hutang,0 payment,nti.total FROM psn_penjualan_kredit AS nti WHERE
        nti.konsumen_id = :customer_id AND nti.tgl >= :from AND nti.tgl <= :to
        UNION
        (SELECT nti.tgl,nti.doc_ref,'PAYEMNT' note,nti.doc_ref no_faktur,
        0 hutang,-nti.total payment,nti.total FROM psn_penjualan_kredit AS nti
        WHERE nti.konsumen_id = :customer_id AND nti.tgl >= :from AND nti.arus = -2 AND nti.tgl <= :to)
        UNION
        (SELECT npu.tgl,npu.doc_ref,'PAYEMNT' note,nti.doc_ref no_faktur,0 hutang,npud.kas_diterima payment,
        -npud.kas_diterima total FROM psn_penjualan_kredit AS nti
        INNER JOIN psn_pelunasan_piutang_detil AS npud
			ON nti.penjualan_id = npud.penjualan_id
        INNER JOIN psn_pelunasan_piutang AS npu
			ON npud.pelunasan_piutang_id = npu.pelunasan_piutang_id
        WHERE nti.konsumen_id = :customer_id AND npu.tgl >= :from AND npu.tgl <= :to)
        ORDER BY tgl");
		return $res->queryAll(true,$param);
	}
}