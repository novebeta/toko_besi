<?php

Yii::import('application.models._base.BaseDos');

class Dos extends BaseDos
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate()
    {
        $this->tdate = new CDbExpression('NOW()');
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}