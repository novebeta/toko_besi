<?php

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_TIME,'INDONESIA');
// change the following paths if necessary
//$yii = dirname(__FILE__) . '/../../yii/framework/YiiBase.php';
//$yii=dirname(__FILE__).'/../yii/framework/Yiilite.php';
$config = dirname(__FILE__) . '/protected/config/ext.php';
//$yiiG = dirname(__FILE__) . '/protected/globals.php';

// remove the following lines when in production mode
define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
define('YII_TRACE_LEVEL', 3);

require_once(dirname(__FILE__) . '/../yii/framework/YiiBase.php');
require_once(dirname(__FILE__) . '/protected/globals.php');
require_once(dirname(__FILE__) . '/protected/security.php');
//require_once($define);
class Yii extends YiiBase {

    /**
     * @static
     * @return CWebApplication
     */
    public static function app() {
        return parent::app();
    }

}
//<script src="js/ext340/ext-all-debug-w-comments.js">
$app = Yii::createWebApplication($config);
// Yii::import('ext.yiiexcel.YiiExcel', true);
// Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);
// PHPExcel_Shared_ZipStreamWrapper::register();

// if (ini_get('mbstring.func_overload') & 2) {
//     throw new Exception('Multibyte function overloading in PHP must be disabled for string functions (2).');
// }
// PHPExcel_Shared_String::buildCharacterSets();
$app->run();
?>
